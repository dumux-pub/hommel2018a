// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the one-phase model:
 * water is flowing from bottom to top through and around a low permeable lens.
 */
#ifndef DUMUX_1PTEST_PROBLEM_HH
#define DUMUX_1PTEST_PROBLEM_HH

#include <dumux/porousmediumflow/1p/implicit/model.hh>
#include <dumux/porousmediumflow/implicit/problem.hh>
#include <dumux/material/components/brine.hh>
#include <dumux/material/fluidsystems/liquidphase.hh>

#include <appl/co2/icp/gorgasspatialparams1p.hh>

namespace Dumux
{
template <class TypeTag>
class OnePTestProblem;

namespace Properties
{
NEW_TYPE_TAG(OnePTestProblem, INHERITS_FROM(OneP, GorgasSpatialParams));
NEW_TYPE_TAG(OnePTestBoxProblem, INHERITS_FROM(BoxModel, OnePTestProblem));
NEW_TYPE_TAG(OnePTestCCProblem, INHERITS_FROM(CCModel, OnePTestProblem));

SET_PROP(OnePTestProblem, FluidSystem)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef FluidSystems::LiquidPhase<Scalar, Brine<Scalar> > type;
};

// Set the grid type
SET_TYPE_PROP(OnePTestProblem, Grid, Dune::UGGrid<3>);

// Set the problem property
SET_TYPE_PROP(OnePTestProblem, Problem, OnePTestProblem<TypeTag> );

// Set the spatial parameters
SET_TYPE_PROP(OnePTestProblem, SpatialParams, GorgasSpatialParams<TypeTag> );

// Linear solver settings
SET_TYPE_PROP(OnePTestProblem, LinearSolver, ILU0BiCGSTABBackend<TypeTag> );

// Enable gravity
SET_BOOL_PROP(OnePTestProblem, ProblemEnableGravity, true);
}

/*!
 * \ingroup OnePModel
 * \ingroup ImplicitTestProblems
 * \brief  Test problem for the one-phase model:
 * water is flowing from bottom to top through and around a low permeable lens.
 *
 * The domain is box shaped. All sides are closed (Neumann 0 boundary)
 * except the top and bottom boundaries (Dirichlet), where water is
 * flowing from bottom to top.
 *
 * In the middle of the domain, a lens with low permeability (\f$K=10e-12\f$)
 * compared to the surrounding material (\f$ K=10e-10\f$) is defined.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_box1p -parameterFile test_box1p.input</tt> or
 * <tt>./test_cc1p -parameterFile test_cc1p.input</tt>
 *
 * The same parameter file can be also used for 3d simulation but you need to change line
 * <tt>typedef Dune::YaspGrid<2> type;</tt> to
 * <tt>typedef Dune::YaspGrid<3> type;</tt> in the problem file
 * and use <tt>test_1p_3d.dgf</tt> in the parameter file.
 */
template <class TypeTag>
class OnePTestProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {
        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };
    enum {
        // indices of the primary variables
        conti0EqIdx = Indices::conti0EqIdx,
        pressureIdx = Indices::pressureIdx
    };

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::Intersection Intersection;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;

public:
    OnePTestProblem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView)
    {
        name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag,
                                             std::string,
                                             Problem,
                                             Name);
        initPressure_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Initial, initPressure);
        injectHeight_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, Height);
        numInjections_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Injection, numInjections);
        injectionParameters_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Injection, InjectionParamFile);

        std::ifstream injectionData;
                std::string row;
                injectionData.open( injectionParameters_); // open the Injection data file
                if (not injectionData.is_open())
                {
                     std::cerr << "\n\t -> Could not open file '"
                              << injectionParameters_
                              << "'. <-\n\n\n\n";
                    exit(1) ;
                }
                Scalar tempTime = 0;
                int tempType = 0;
                Scalar tempQ = 0;
                Scalar tempFactor = 0;

                // print file to make sure it is the right file
                std::cout << "Read file: " << injectionParameters_ << " ..." << std::endl << std::endl;
                while(!injectionData.eof())
                {
                    getline(injectionData, row);
                    std::cout << row << std::endl;
                }
                injectionData.close();

                //read data from file
                injectionData.open(injectionParameters_);

                while(!injectionData.eof())
                {
                    getline(injectionData, row);

                    if(row == "EndTimes")
                        {
                        getline(injectionData, row);
                        while(row != "#")
                            {
                            if (row != "#")
                                {
                                std::istringstream ist(row);
                                ist >> tempTime;
                                epiEnd_.push_back(tempTime);
    //                          std::cout << "size of EpiEnd: "<<epiEnd_.size() << std::endl;
                                }
                            getline(injectionData, row);
                            }
                        }
                    if(row == "InjectionQ")
                        {
                        getline(injectionData, row);
                        while(row != "#")
                            {
                            if (row != "#")
                                {
                                std::istringstream ist(row);
                                ist >> tempQ;
                                injQ_.push_back(tempQ);
                                }
                            getline(injectionData, row);
                            }
                        }
                }

                injectionData.close();

        this->timeManager().startNextEpisode(EpisodeEnd(0));
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string& name() const
    {
        return name_;
    }

    /*!
     * \brief Return the temperature within the domain in [K].
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + 25; } // 25C

    /*!
     * \brief Return the sources within the domain.
     *
     * \param values Stores the source values, acts as return value
     * \param globalPos The global position
     */
    void sourceAtPos(PrimaryVariables &values,
                const GlobalPosition &globalPos) const
    {
        values = 0;
    }
    // \}
    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param values The boundary types for the conservation equations
     * \param globalPos The position of the center of the finite volume
     */
    void boundaryTypesAtPos(BoundaryTypes &values,
            const GlobalPosition &globalPos) const
    {
        Scalar zmax = this->bBoxMax()[dim - 1];
        Scalar rmax = this->bBoxMax()[0];
        Scalar rMin = 0.2286;

        values.setAllNeumann();

        if(sqrt(globalPos[0]*globalPos[0]+globalPos[1]*globalPos[1]) > rmax - eps_)
        {
                values.setAllDirichlet();
        }
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param globalPos The center of the finite volume which ought to be set.
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichletAtPos(PrimaryVariables &values,
                        const GlobalPosition &globalPos) const
    {
        Scalar zmax = this->bBoxMax()[dim-1];
        values[pressureIdx] = initPressure_ + (0.5*zmax - globalPos[dim-1])
                *FluidSystem::density(temperature(), initPressure_)*9.81; //p_atm + rho*g*h
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * For this method, the \a priVars parameter stores the mass flux
     * in normal direction of each component. Negative values mean
     * influx.
     */
    using ParentType::neumann;
    void neumann(PrimaryVariables &priVars,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const Intersection &intersection,
                 const int scvIdx,
                 const int boundaryFaceIdx) const
    {
        Scalar rMin = this->bBoxMin()[0]/0.8798;
        const GlobalPosition &globalPos =fvGeometry.boundaryFace[boundaryFaceIdx].ipGlobal;
        Scalar zmax = this->bBoxMax()[dim-1];
        int episodeIdx = this->timeManager().episodeIndex()-1;
        Scalar waterFlux = injQ_[episodeIdx];//[m³/s]
        waterFlux /= 0.2286 * 2* 3.14 * injectHeight_;  //[m/s]

        if(sqrt(globalPos[0]*globalPos[0]+globalPos[1]*globalPos[1])<= rMin + eps_
                && globalPos[dim-1] <= (zmax+injectHeight_)/2 + eps_
                && globalPos[dim-1] >= (zmax-injectHeight_)/2 - eps_)
        {
            //in mol
//            priVars[conti0EqIdx] = -waterFlux*FluidSystem::density(temperature(), initPressure_)
//            /FluidSystem::molarMass(conti0EqIdx);
            // in kg
            priVars[conti0EqIdx] = -waterFlux*FluidSystem::density(temperature(), initPressure_);
        }
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    void initial(PrimaryVariables &priVars,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const int scvIdx) const
    {
        Scalar zmax = this->bBoxMax()[dim-1];
        const GlobalPosition &globalPos = element.geometry().corner(scvIdx);

        priVars[pressureIdx] = initPressure_ + (0.5*zmax - globalPos[dim-1])
                *FluidSystem::density(temperature(), initPressure_)*9.81; //p_atm + rho*g*h
    }

    // \}


    /*!
    * \brief Manage the episodes.
    * The duration of the next episode set using the private function Scalar episodeEnd(episodeIdx).
    * Also, the time-step size is set to 1 as a new episode usually means a changed injection type
    * which im many cases causes difficulties for the Newton convergence.
    */
    void episodeEnd()
    {
         int episodeIdx = this->timeManager().episodeIndex();
         Scalar tEpisode= EpisodeEnd(episodeIdx)-EpisodeEnd(episodeIdx-1);

         this->timeManager().startNextEpisode(tEpisode);
         this->timeManager().setTimeStepSize(50);
    }

private:

    Scalar EpisodeEnd (int episodeIdx)
    {
        if(episodeIdx < epiEnd_.size())
            return 60 * 60 * epiEnd_[episodeIdx]; //epiEnd from parameter file in hours!!!!
        else                            //default value
            return 10e10;
    }

    static constexpr Scalar eps_ = 1e-6;

    std::string name_;
    Scalar initPressure_;
    Scalar injectHeight_;
    Scalar numInjections_;
    std::string injectionParameters_;

    std::vector<Scalar> epiEnd_;
    std::vector<Scalar> injQ_;
};
} //end namespace

#endif
