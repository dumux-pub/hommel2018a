Summary
=======

Hommel2018a contains a fully working version of the DuMuX code used
for the recent reevaluation of the field-scale modeling of
microbially induced calcite precipitation in 2018 (2018 simulations in the manuscript)
for the application at the Gorgas power plant as published in Computational Geoscience.

The code for the old simulations refered to in the manuscript as "2014 simulations"
is available in the dumux-pub repository Shigorina2014a, see https://git.iws.uni-stuttgart.de/dumux-pub/Shigorina2014a.


The required DUNE modules are listed at the end of this README.

The Results folder contains the data used to generate the plots (in .vtu and .mat format)

Bibtex entry:
@Article{Hommel2018,
  author =    {Cunningham, Alfred B.  and Class, Holger and Ebigbo, Anozie  and Gerlach, Robin and Phillips, Adrienne J. and Hommel, Johannes},
  title =     {Field-scale modeling of microbially induced calcite precipitation},
  journal =   {Computational Geoscience},
  volume = {tbd},
  number = {tbd},
  year =      {2018},
  pages =     {tbd},
  doi =       {tbd}
}



Installation
============

The easiest way to install this module is to create a new folder and to execute the file
[installHommel2015a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Hommel2015a/raw/master/installHommel2015a.sh)
in this folder.

```bash
mkdir -p Hommel2018a && cd Hommel2018a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Hommel2018a/raw/master/installHommel2018a.sh
sh ./installHommel2018a.sh
```
For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook.


Applications
============


The content of this DUNE module was extracted from the module dumux-devel.
In particular, the following subfolders of dumux-devel have been extracted:

  appl/co2/icp/micp/,

as well as for the 1p simulation for determining the Dirichlet bc pressure:
  appl/co2/icp/1pGorgasForPressure

Additionally, all headers in dumux-devel that are required to build the
executables from the sources

  appl/co2/icp/micp/micp.cc,
  appl/co2/icp/1pGorgasForPressure/test1p.cc,

have been extracted. You can build the module just like any other DUNE
module. For building and running the executables, please go to the folders
containing the sources listed above.

Used Versions and Software
==========================

When this module was created, the original module dumux-devel was using
the following list of DUNE modules and third-party libraries.
BEWARE: This does not necessarily mean that the applications in this
extracted module actually depend on all these components.

