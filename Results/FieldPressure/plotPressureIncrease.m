clear;
load('pBigReal.mat');
% load('extendedGorgasPressures.mat');
load('FieldPressure.mat');
load('Gorgas1pBCPressure.mat');
load('extendedGorgasPressuresHommel.mat');
load('extendedGorgasPressuresShigorina.mat');


Fig = figure;
figure(Fig);

h1_plot = plot( TimeH, MaxPressurePa-3.34e6, 'b*', timeBigReal, pMeanBigReal - pMeanBigReal(1), 'r-', time, pMeanBC-pMeanBC(1), 'g-', timeShigorina, pMeanShigorina-pMeanShigorina(1), 'r--', timeHommel, pMeanHommel-pMeanHommel(1), 'r:');
h1_leg = legend('Maximum borehole pressure', 'Simulated 2014, r_m_a_x=8m', 'Dynamic Pressure Boundary Condition (DPBC) @ 50m', 'Sim., with DPBC & 2014 parameters, r_m_a_x=50m', 'Sim., with DPBC & Hommel 2015 parameters, r_m_a_x=50m');
h1_xlab = xlabel('Time [h]');
h1_ylab = ylabel('Pressure - Hydrostatic Pressure [Pa]');
% h1_plot = plot( TimeH, MaxPressurePa - MaxPressurePa(1), 'b*', timeBigReal, pMeanBigReal - pMeanBigReal(1), 'r-', timeExtended, pMeanExtended-pMeanExtended(1), 'm-');
% h1_leg = legend('Maximum downhole pressure', 'Simulated injection pressure', 'Simulated injection pressure with dynamic BC' );
% h1_xlab = xlabel('Time [h]');
% h1_ylab = ylabel('Pressure - Initial Pressure [Pa]');




set(gca,'FontSize',20)
set(h1_leg,'FontSize',16)
set(h1_xlab,'FontSize',20)
set(h1_ylab,'FontSize',20)
set(h1_plot,'Markersize',10);

% axis([0 900 0 4.8]);
set(Fig,'position',[0 0, 1200 688])
set(Fig,'PaperPositionMode','Auto')
saveas(Fig,'./new_graph','epsc')