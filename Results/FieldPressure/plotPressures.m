clear;
load('pBigReal.mat');
% load('extendedGorgasPressures.mat');
load('FieldPressure.mat');
load('Gorgas1pBCPressure.mat');
load('extendedGorgasPressuresHommel.mat');
load('extendedGorgasPressuresShigorina.mat');


Fig = figure;
figure(Fig);

% % h1_plot = plot( TimeH, MaxPressurePa, 'b*', timeBigReal, pMeanBigReal, 'r-', time, pMeanBC+(3.34-1.79)*2*1e6, 'g-', timeShigorina, pMeanShigorina+(3.34-1.79)*1e6, 'r--', timeHommel, pMeanHommel+(3.34-1.79)*1e6, 'r:');
% h1_plot = plot( TimeH, MaxPressurePa, 'b*', timeBigReal, pMeanBigReal, 'r-', time, pMeanBC+(3.34-1.79)*1e6, 'g-', timeShigorina, pMeanShigorina, 'r--', timeHommel, pMeanHommel, 'r:');
% h1_leg = legend('Maximum borehole pressure', 'Simulated 2014, r_m_a_x=8m', 'Dynamic Pressure Boundary Condition (DPBC) @ 50m', 'Sim., with DPBC & 2014 parameters, r_m_a_x=50m', 'Sim., with DPBC & Hommel 2015 parameters, r_m_a_x=50m');
% h1_xlab = xlabel('Time [h]');
% h1_ylab = ylabel('Injection Pressure [Pa]');
% % axis([0.23 2.4 0 1*10^-12]);

% h1_plot = plot( timeBigReal, pMeanBigReal, 'r-', time, pMeanBC+(3.34-1.79)*2*1e6, 'g-', timeShigorina, pMeanShigorina+(3.34-1.79)*1e6, 'r--', timeHommel, pMeanHommel+(3.34-1.79)*1e6, 'r:');
h1_plot = plot(time, pMeanBC+(3.34-1.79)*1e6, 'b-', timeShigorina, pMeanShigorina, 'r-', timeHommel, pMeanHommel, 'g:');
h1_leg = legend('Dynamic Pressure Boundary Condition (DPBC) @ 50m', 'Injection pressure: 8mx50m with 2014 parameters', 'Injection pressure: 8mx50m with Hommel 2015 parameters');
h1_xlab = xlabel('Time [h]');
h1_ylab = ylabel('Injection Pressure [Pa]');
axis([0 75 3000000 6500000]);

% % h1_plot = plot( timeBigReal, pMeanBigReal, 'r-', time, pMeanBC+(3.34-1.79)*2*1e6, 'g-', timeShigorina, pMeanShigorina+(3.34-1.79)*1e6, 'r--', timeHommel, pMeanHommel+(3.34-1.79)*1e6, 'r:');
% h1_plot = plot(timeBigReal, pMeanBigReal, 'r-', time, pMeanBC+(3.34-1.79)*1e6, 'g-', timeShigorina, pMeanShigorina, 'r--', timeHommel, pMeanHommel, 'r:');
% h1_leg = legend('Simulated 2014, constant boundary pressure, r_m_a_x=8m', 'Dynamic Pressure Boundary Condition (DPBC) @ 50m', 'Sim., with DPBC & 2014 parameters, r_m_a_x=50m', 'Sim., with DPBC & Hommel 2015 parameters, r_m_a_x=50m');
% h1_xlab = xlabel('Time [h]');
% h1_ylab = ylabel('Injection Pressure [Pa]');
% axis([0.23 2.4 0 1*10^-12]);
%%%bar
% h1_plot = plot( TimeH, MaxPressurePa/10^5, 'b*', timeBigReal, pMeanBigReal/10^5, 'r-', timeBigReal, pMeanBigReal/10^5+55, 'r--');
% h1_leg = legend('Maximum borehole pressure', 'Simulated injection pressure', 'Simulated injection pressure + 55 bar');
% h1_xlab = xlabel('Time [h]');
% h1_ylab = ylabel('Pressure [bar]');


set(gca,'FontSize',20)
set(h1_leg,'FontSize',16)
set(h1_xlab,'FontSize',20)
set(h1_ylab,'FontSize',20)
set(h1_plot,'Linewidth',2);
set(h1_plot,'Markersize',10);

% axis([0 900 0 4.8]);
set(Fig,'position',[0 0, 1200 688])
set(Fig,'PaperPositionMode','Auto')
saveas(Fig,'./new_graph','epsc')