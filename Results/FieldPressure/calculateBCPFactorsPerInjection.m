load('Gorgas1pBCPressure.mat');
load('RealInjMidTimes.mat');

% interpolated pressure
interpolatedPFactor = interp1(time,pfactor,InjMidTimes); %interp1(x,v,xq) returns interpolated values of a 1-D function at specific query points using linear interpolation. Vector x contains the sample points, and v contains the corresponding values, v(x). Vector xq contains the coordinates of the query points.

plot(InjMidTimes,interpolatedPFactor, 'b-')