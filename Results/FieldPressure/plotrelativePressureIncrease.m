clear;
load('pBigReal.mat');
% load('extendedGorgasPressures.mat');
load('FieldPressure.mat');
load('Gorgas1pBCPressure.mat');
load('extendedGorgasPressuresHommel.mat');
load('extendedGorgasPressuresShigorina.mat');

% pIncreaseBigReal= pMeanBigReal/pMeanBigReal(1);
% pIncreaseExtended= pMeanExtended/pMeanExtended(1);
% 
% 
% % interpolated pressure
% interpolatedPFactor = interp1(time,pfactor,timeBigReal); %interp1(x,v,xq) returns interpolated values of a 1-D function at specific query points using linear interpolation. Vector x contains the sample points, and v contains the corresponding values, v(x). Vector xq contains the coordinates of the query points.
% pBCEffet = pIncreaseBigReal.* interpolatedPFactor;
% pBCEffet05 = pIncreaseBigReal.* (interpolatedPFactor+5)/6;

Fig = figure;
figure(Fig);
% 
% h1_plot = plot( TimeH, MaxPressurePa/MaxPressurePa(1), 'b*', timeBigReal, pIncreaseBigReal, 'r-', time1p8by50, pMean1p8by50/pMean1p8by50(1), 'm-');
% h1_leg = legend('Downhole pressure', 'Simulated injection pressure MICP' , 'Simulated injection pressure 1Phase' );
% h1_xlab = xlabel('Time [h]');
% h1_ylab = ylabel('Pressure increase [Pa/Pa_i_n_i_t]');

% h1_plot = plot( TimeH, MaxPressurePa/3.34e6, 'b*', timeBigReal, pIncreaseBigReal, 'r-', timeExtended, pIncreaseExtended, 'm-');
h1_plot = plot( TimeH, MaxPressurePa/3.34e6, 'b*', timeBigReal, pMeanBigReal/pMeanBigReal(1), 'r-', time, pMeanBC/pMeanBC(1), 'g-', timeShigorina, pMeanShigorina/pMeanShigorina(1), 'r--', timeHommel, pMeanHommel/pMeanHommel(1), 'r:');
h1_leg = legend('Maximum borehole pressure', 'Simulated 2014, r_m_a_x=8m', 'Dynamic Pressure Boundary Condition (DPBC) @ 50m', 'Sim., with DPBC & 2014 parameters, r_m_a_x=50m', 'Sim., with DPBC & Hommel 2015 parameters, r_m_a_x=50m');
h1_xlab = xlabel('Time [h]');
h1_ylab = ylabel('Pressure / Hydrostatic Pressure [Pa/Pa]');

% h1_plot = plot( TimeH, MaxPressurePa/MaxPressurePa(1), 'b*', timeBigReal, pIncreaseBigReal, 'r-', timeExtended, pIncreaseExtended, 'm-');
% h1_leg = legend('Maximum downhole pressure', 'Simulated injection pressure', 'Simulated injection pressure with dynamic BC' );
% h1_xlab = xlabel('Time [h]');
% h1_ylab = ylabel('Pressure / Initial Pressure [Pa/Pa_i_n_i_t]');

% h1_plot = plot( TimeH, MaxPressurePa/MaxPressurePa(1), 'b*', timeBigReal, pBCEffet, 'r-', timeBigReal, pBCEffet05, 'm-');
% h1_leg = legend('Downhole pressure', 'Simulated injection pressure MICP x 1pBCpressureIncrease' );
% h1_xlab = xlabel('Time [h]');
% h1_ylab = ylabel('Pressure / Initial Pressure [Pa/Pa_i_n_i_t]');

% axis([0.23 2.4 0 1*10^-12]);
%%%bar
% h1_plot = plot( TimeH, MaxPressurePa/10^5, 'b*', timeBigReal, pMeanBigReal/10^5, 'r-', timeBigReal, pMeanBigReal/10^5+55, 'r--');
% h1_leg = legend('Maximum borehole pressure', 'Simulated injection pressure', 'Simulated injection pressure + 55 bar');
% h1_xlab = xlabel('Time [h]');
% h1_ylab = ylabel('Pressure [bar]');


set(gca,'FontSize',20)
set(h1_leg,'FontSize',16)
set(h1_xlab,'FontSize',20)
set(h1_ylab,'FontSize',20)
set(h1_plot,'Markersize',10);

% axis([0 900 0 4.8]);
set(Fig,'position',[0 0, 1200 688])
set(Fig,'PaperPositionMode','Auto')
saveas(Fig,'./new_graph','epsc')