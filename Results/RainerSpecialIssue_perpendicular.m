% clear;
% 
% y = 0;
% x = 0.2286;
% eps = 1e-6;
% j =1;
% load('extendedHommel.mat');
% 
% % get coordinate vectors
% j = 1;
% for i=1:3:length(ref_Coordinates(1,:))
%     XCoor(j) = ref_Coordinates(1,i);
%     YCoor(j) = ref_Coordinates(1,i+1);
%     ZCoor(j) = ref_Coordinates(1,i+2);
%     j = j+1;
% end
% j = 1;
% 
% %plot over line
% timeFilePara = length(ref_Time);
% 
% 
% % y = 0;
% % z = 0.2286;
% % eps = 1e-6;
% j =1;
% for i=1:length(ZCoor)
% 
%        if (YCoor(i) > y - eps && YCoor(i) < y + eps) % take all coordinates with YCoor=y
%        if (XCoor(i) > x - eps && XCoor(i) < x + eps) % take all coordinates with ZCoor=z
%         nodeNo(j) = i;
%         j = j + 1;
%        end
%        end
%     
% end
% 
% for i=1:length(nodeNo)
% 
%     phiCHommel(i) = ref_Porosity_Calcite(timeFilePara, nodeNo(i));
%     phiFHommel(i) = ref_Porosity_Biofilm(timeFilePara, nodeNo(i));
% 
%     poroHommel(i) = ref_porosity(timeFilePara, nodeNo(i));
%     permHommel(i) = ref_Permeability(timeFilePara, nodeNo(i));
%     %sg1(i) = ref_Sg(timeFilePara, nodeNo(i));
%     
%     zHommel(i) = ZCoor(nodeNo(i));
%     error1(i) = 0;
% end
% 
% load('extendedShigorina.mat');
% 
% % get coordinate vectors
% j = 1;
% for i=1:3:length(ref_Coordinates(1,:))
%     XCoor(j) = ref_Coordinates(1,i);
%     YCoor(j) = ref_Coordinates(1,i+1);
%     ZCoor(j) = ref_Coordinates(1,i+2);
%     j = j+1;
% end
% j = 1;
% 
% %plot over line
% timeFilePara = length(ref_Time);
% 
% 
% % y = 0;
% % z = 0.2286;
% % eps = 1e-6;
% j =1;
% for i=1:length(ZCoor)
% 
%        if (YCoor(i) > y - eps && YCoor(i) < y + eps) % take all coordinates with YCoor=y
%        if (XCoor(i) > x - eps && XCoor(i) < x + eps) % take all coordinates with ZCoor=z
%         nodeNo(j) = i;
%         j = j + 1;
%        end
%        end
%     
% end
% 
% for i=1:length(nodeNo)
% 
%     phiCShigorina(i) = ref_Porosity_Calcite(timeFilePara, nodeNo(i));
%     phiFShigorina(i) = ref_Porosity_Biofilm(timeFilePara, nodeNo(i));
% 
%     poroShigorina(i) = ref_porosity(timeFilePara, nodeNo(i));
%     permShigorina(i) = ref_Permeability(timeFilePara, nodeNo(i));
%     %sg1(i) = ref_Sg(timeFilePara, nodeNo(i));
%     
%     zShigorina(i) = ZCoor(nodeNo(i));
%     error1(i) = 0;
% end
% 
% 
% 
% 
% % 
% % load('big_simple.mat');
% % 
% % % get coordinate vectors
% % j = 1;
% % for i=1:3:length(ref_Coordinates(1,:))
% %     XCoor(j) = ref_Coordinates(1,i);
% %     YCoor(j) = ref_Coordinates(1,i+1);
% %     ZCoor(j) = ref_Coordinates(1,i+2);
% %     j = j+1;
% % end
% % j = 1;
% % 
% % %plot over line
% % timeFilePara = length(ref_Time);
% % 
% % 
% % % y = 0;
% % % x = 0.5596;
% % % eps = 1e-6;
% % j =1;
% % for i=1:length(ZCoor)
% % 
% %        if (YCoor(i) > y - eps && YCoor(i) < y + eps) % take all coordinates with YCoor=y
% %        if (XCoor(i) > x - eps && XCoor(i) < x + eps) % take all coordinates with ZCoor=z
% %         nodeNo(j) = i;
% %         j = j + 1;
% %        end
% %        end
% %     
% % end
% % 
% % for i=1:length(nodeNo)
% % 
% %     phiCfine_simple(i) = ref_Porosity_Calcite(timeFilePara, nodeNo(i));
% %     phiFfine_simple(i) = ref_Porosity_Biofilm(timeFilePara, nodeNo(i));
% %     Cafine_fancyimple(i) = ref_X_lCa(timeFilePara, nodeNo(i));
% %     %omega1(i) = ref_omega(timeFilePara, nodeNo(i));
% %     pHfine_fancy(i) = ref_pH(timeFilePara, nodeNo(i));
% %     initporo = 0.18;
% %     %poro1(i) = initporo - phiC1(i) - phiF1(i);
% %     initperm = 3*10^-14;
% %     poro1_simple(i) = ref_porosity(timeFilePara, nodeNo(i));
% %     perm1_simple(i) = ref_Permeability(timeFilePara, nodeNo(i));
% %     %sg1(i) = ref_Sg(timeFilePara, nodeNo(i));
% %     
% %     zfine_simple(i) = ZCoor(nodeNo(i));
% %     error1(i) = 0;
% % end
% % 
% % load('big_fancy.mat');
% % 
% % % get coordinate vectors
% % j = 1;
% % for i=1:3:length(ref_Coordinates(1,:))
% %     XCoor(j) = ref_Coordinates(1,i);
% %     YCoor(j) = ref_Coordinates(1,i+1);
% %     ZCoor(j) = ref_Coordinates(1,i+2);
% %     j = j+1;
% % end
% % j = 1;
% % 
% % %plot over line
% % timeFilePara = length(ref_Time);
% % 
% % 
% % % y = 0;
% % % x = 0.2286;
% % % eps = 1e-6;
% % j =1;
% % for i=1:length(ZCoor)
% % 
% %        if (YCoor(i) > y - eps && YCoor(i) < y + eps) % take all coordinates with YCoor=y
% %        if (XCoor(i) > x - eps && XCoor(i) < x + eps) % take all coordinates with ZCoor=z
% %         nodeNo(j) = i;
% %         j = j + 1;
% %        end
% %        end
% %     
% % end
% % 
% % for i=1:length(nodeNo)
% % 
% %     phiCfine_fancy(i) = ref_Porosity_Calcite(timeFilePara, nodeNo(i));
% %     phiFfine_fancy(i) = ref_Porosity_Biofilm(timeFilePara, nodeNo(i));
% %     Cafine_fancy(i) = ref_X_lCa(timeFilePara, nodeNo(i));
% %     %omega1(i) = ref_omega(timeFilePara, nodeNo(i));
% %     pHfine_fancy(i) = ref_pH(timeFilePara, nodeNo(i));
% %     initporo = 0.18;
% %     %poro1(i) = initporo - phiC1(i) - phiF1(i);
% %     initperm = 3*10^-14;
% %     poro1_fancy(i) = ref_porosity(timeFilePara, nodeNo(i));
% %     perm1_fancy(i) = ref_Permeability(timeFilePara, nodeNo(i));
% %     %sg1(i) = ref_Sg(timeFilePara, nodeNo(i));
% %     
% %     zfine_fancy(i) = ZCoor(nodeNo(i));
% %     error1(i) = 0;
% % end
% 
% load('big_real.mat');
% 
% % get coordinate vectors
% j = 1;
% for i=1:3:length(ref_Coordinates(1,:))
%     XCoor(j) = ref_Coordinates(1,i);
%     YCoor(j) = ref_Coordinates(1,i+1);
%     ZCoor(j) = ref_Coordinates(1,i+2);
%     j = j+1;
% end
% j = 1;
% 
% %plot over line
% timeFilePara = length(ref_Time);
% 
% 
% % y = 0;
% % z = 0.2286;
% % eps = 1e-6;
% j =1;
% for i=1:length(ZCoor)
% 
%        if (YCoor(i) > y - eps && YCoor(i) < y + eps) % take all coordinates with YCoor=y
%        if (XCoor(i) > x - eps && XCoor(i) < x + eps) % take all coordinates with ZCoor=z
%         nodeNo(j) = i;
%         j = j + 1;
%        end
%        end
%     
% end
% 
% for i=1:length(nodeNo)
% 
%     phiCfine_real(i) = ref_Porosity_Calcite(timeFilePara, nodeNo(i));
%     phiFfine_real(i) = ref_Porosity_Biofilm(timeFilePara, nodeNo(i));
%     poro1_real(i) = ref_porosity(timeFilePara, nodeNo(i));
%     perm1_real(i) = ref_Permeability(timeFilePara, nodeNo(i));
%    
%     
%     zfine_real(i) = ZCoor(nodeNo(i));
%     error1(i) = 0;
% end
% 
% 
% 
% load('small_real.mat');
% 
% % get coordinate vectors
% j = 1;
% for i=1:3:length(ref_Coordinates(1,:))
%     XCoor(j) = ref_Coordinates(1,i);
%     YCoor(j) = ref_Coordinates(1,i+1);
%     ZCoor(j) = ref_Coordinates(1,i+2);
%     j = j+1;
% end
% j = 1;
% 
% %plot over line
% timeFilePara = length(ref_Time);
% 
% 
% % y = 0;
% % z = 0.2286;
% % eps = 1e-6;
% j =1;
% for i=1:length(ZCoor)
% 
%        if (YCoor(i) > y - eps && YCoor(i) < y + eps) % take all coordinates with YCoor=y
%        if (XCoor(i) > x - eps && XCoor(i) < x + eps) % take all coordinates with ZCoor=z
%         nodeNo(j) = i;
%         j = j + 1;
%        end
%        end
%     
% end
% 
% for i=1:length(nodeNo)
% 
%     phiCsmall_real(i) = ref_Porosity_Calcite(timeFilePara, nodeNo(i));
%     phiFsmall_real(i) = ref_Porosity_Biofilm(timeFilePara, nodeNo(i));
%     porosmall_real(i) = ref_porosity(timeFilePara, nodeNo(i));
%     permsmall_real(i) = ref_Permeability(timeFilePara, nodeNo(i));
%         
%     zsmall_real(i) = ZCoor(nodeNo(i));
%     error1(i) = 0;
% end

load('PerpendicularData.mat');


Fig = figure;
figure(Fig);

% % % For CMWR poster
% fracture at 341 m
% h1_plot = plot(zsmall_real-342.2, phiCsmall_real, 'b-', zfine_real-345, phiCfine_real, 'r--', zShigorina-345, phiCShigorina, 'k:o', zHommel-345, phiCHommel, 'g-+');
% fracture at 340.8 m
h1_plot = plot( zfine_real-344.8, phiCfine_real, 'r-',zsmall_real-342, phiCsmall_real, 'b--', zShigorina-344.8, phiCShigorina, 'k:o', zHommel-344.8, phiCHommel, 'g-+');
h1_leg = legend('2014: 8mx8m','2014: 2.4mx2.4m', '2018: 8mx50m, 2014 param.', '2018: 8mx50m, Hommel 2015 param.');
h1_xlab = xlabel('Depth [m]');
h1_ylab = ylabel('Calcite Volume Fraction [-]');
% axis([-343 -335 0 0.02]);
axis([-342 -337 0 0.02]);

set(gca,'FontSize',16)
set(h1_leg,'FontSize',16)
set(h1_xlab,'FontSize',16)
set(h1_ylab,'FontSize',16)
set(h1_plot,'LineWidth',2);
set(h1_plot,'MarkerSize',10);

% axis([0 900 0 4.8]);
set(Fig,'position',[0 0, 600 400])
set(Fig,'PaperPositionMode','Auto')
saveas(Fig,'./new_graph','epsc')

% % %for Rainer Special Issue
% fracture at 341 m
% h1_plot = plot(zsmall_real-342.2, phiCsmall_real, 'b-', zfine_real-345, phiCfine_real, 'r--', zShigorina-345, phiCShigorina, 'k:o', zHommel-345, phiCHommel, 'g-+');
% fracture at 340.8 m
% h1_plot = plot(zsmall_real-342, phiCsmall_real, 'b-', zfine_real-344.8, phiCfine_real, 'r--', zShigorina-344.8, phiCShigorina, 'k:o', zHommel-344.8, phiCHommel, 'g-+');
% h1_leg = legend('2014: 2.4mx2.4m, real injection','2014: 8mx8m, real injection', '2018: 8mx50m, real inj., 2014 parameters', '2018: 8mx50m, real inj., Hommel 2015 params.');
% h1_xlab = xlabel('Depth [m]');
% h1_ylab = ylabel('Calcite Volume Fraction [-]');
% % axis([-343 -335 0 0.02]);
% axis([-342 -337 0 0.02]);
% 
% set(gca,'FontSize',16)
% set(h1_leg,'FontSize',16)
% set(h1_xlab,'FontSize',16)
% set(h1_ylab,'FontSize',16)
% set(h1_plot,'LineWidth',2);
% set(h1_plot,'MarkerSize',10);
% 
% % axis([0 900 0 4.8]);
% set(Fig,'position',[0 0, 800 400])
% set(Fig,'PaperPositionMode','Auto')
% saveas(Fig,'./new_graph','epsc')

% 
% Fig = figure;
% figure(Fig);
% 
% h1_plot = plot(zfine_real, perm1_real, 'b-');
% h1_leg = legend('2014: 8mx8m, real injection', '2018: 8mx50m, real inj., 2014 parameters', '2018: 8mx50m, real inj., Hommel 2015 parameters');
% h1_xlab = xlabel('Depth [m]');
% h1_ylab = ylabel('Permeability [m^2]');
% axis([0.23 8 0 8*10^-14]);
% 
% 
% set(gca,'FontSize',16)
% set(h1_leg,'FontSize',16)
% set(h1_xlab,'FontSize',16)
% set(h1_ylab,'FontSize',16)
% set(h1_plot,'LineWidth',2);
% set(h1_plot,'MarkerSize',10);
% 
% % axis([0 900 0 4.8]);
% set(Fig,'position',[0 0, 500 500])
% set(Fig,'PaperPositionMode','Auto')
% saveas(Fig,'./new_graph','epsc')

