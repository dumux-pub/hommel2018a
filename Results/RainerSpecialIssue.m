clear;
%edited copy of "promo_big.m"
% 
% load('real_small_rightQ.mat');
% 
% % get coordinate vectors
% j = 1;
% for i=1:3:length(ref_Coordinates(1,:))
%     XCoor(j) = ref_Coordinates(1,i);
%     YCoor(j) = ref_Coordinates(1,i+1);
%     ZCoor(j) = ref_Coordinates(1,i+2);
%     j = j+1;
% end
% j = 1;
% 
% %plot over line
% timeFilePara = length(ref_Time);
% 
% 
% y = 0;
% z = 1.225;
% % z = 1.195;
% eps = 1e-6;
% j =1;
% for i=1:length(XCoor)
% 
%        if (YCoor(i) > y - eps && YCoor(i) < y + eps) % take all coordinates with YCoor=y
%        if (ZCoor(i) > z - eps && ZCoor(i) < z + eps) % take all coordinates with ZCoor=z
%         nodeNor(j) = i;
%         j = j + 1;
%        end
%        end
%     
% end
% 
% for i=1:length(nodeNor)
% 
%     phiC_real(i) = ref_Porosity_Calcite(timeFilePara, nodeNor(i));
%     phiF_real(i) = ref_Porosity_Biofilm(timeFilePara, nodeNor(i));
%     Ca_real(i) = ref_X_lCa(timeFilePara, nodeNor(i));
%     %omega1(i) = ref_omega(timeFilePara, nodeNor(i));
%     pH_real(i) = ref_pH(timeFilePara, nodeNor(i));
%     poro_real(i) = ref_porosity(timeFilePara, nodeNor(i));
%     perm_real(i) = ref_Permeability(timeFilePara, nodeNor(i));
%     %sg1(i) = ref_Sg(timeFilePara, nodeNor(i));
%     
%     x_real(i) = XCoor(nodeNor(i));
% end
% 
% load('real_small_rightQ.mat');
% 
% % get coordinate vectors
% j = 1;
% for i=1:3:length(ref_Coordinates(1,:))
%     XCoor(j) = ref_Coordinates(1,i);
%     YCoor(j) = ref_Coordinates(1,i+1);
%     ZCoor(j) = ref_Coordinates(1,i+2);
%     j = j+1;
% end
% j = 1;
% 
% %plot over line
% timeFilePara = length(ref_Time);
% 
% 
% y = 0;
% % z = 1.225;
% z = 1.205;
% eps = 1e-6;
% j =1;
% for i=1:length(XCoor)
% 
%        if (YCoor(i) > y - eps && YCoor(i) < y + eps) % take all coordinates with YCoor=y
%        if (ZCoor(i) > z - eps && ZCoor(i) < z + eps) % take all coordinates with ZCoor=z
%         nodeNorn(j) = i;
%         j = j + 1;
%        end
%        end
%     
% end
% 
% for i=1:length(nodeNorn)
% 
%     phiC_realnew(i) = ref_Porosity_Calcite(timeFilePara, nodeNorn(i));
%     phiF_realnew(i) = ref_Porosity_Biofilm(timeFilePara, nodeNorn(i));
%     Ca_realnew(i) = ref_X_lCa(timeFilePara, nodeNorn(i));
%     %omega1(i) = ref_omega(timeFilePara, nodeNorn(i));
%     pH_realnew(i) = ref_pH(timeFilePara, nodeNorn(i));
%     poro_realnew(i) = ref_porosity(timeFilePara, nodeNorn(i));
%     perm_realnew(i) = ref_Permeability(timeFilePara, nodeNorn(i));
%     %sg1(i) = ref_Sg(timeFilePara, nodeNorn(i));
%     
%     x_realnew(i) = XCoor(nodeNorn(i));
% end

% load('ideal_small_wrongQ.mat');
% load('ideal_rightQ_11_4.mat');
% 
% % get coordinate vectors
% j = 1;
% for i=1:3:length(ref_Coordinates(1,:))
%     XCoor(j) = ref_Coordinates(1,i);
%     YCoor(j) = ref_Coordinates(1,i+1);
%     ZCoor(j) = ref_Coordinates(1,i+2);
%     j = j+1;
% end
% j = 1;
% 
% %plot over line
% timeFilePara = length(ref_Time);
% 
% 
% y = 0;
% z = 1.205;
% eps = 1e-6;
% j =1;
% for i=1:length(XCoor)
% 
%        if (YCoor(i) > y - eps && YCoor(i) < y + eps) % take all coordinates with YCoor=y
%        if (ZCoor(i) > z - eps && ZCoor(i) < z + eps) % take all coordinates with ZCoor=z
%         nodeNoi(j) = i
%         j = j + 1;
%        end
%        end
%     
% end
% 
% for i=1:length(nodeNoi)
% 
%     phiC_ideal(i) = ref_Porosity_Calcite(timeFilePara, nodeNoi(i));
%     phiF_ideal(i) = ref_Porosity_Biofilm(timeFilePara, nodeNoi(i));
%     Ca_ideal(i) = ref_X_lCa(timeFilePara, nodeNoi(i));
%     %omega1(i) = ref_omega(timeFilePara, nodeNoi(i));
%     pH_ideal(i) = ref_pH(timeFilePara, nodeNoi(i));
%     poro_ideal(i) = ref_porosity(timeFilePara, nodeNoi(i));
%     perm_ideal(i) = ref_Permeability(timeFilePara, nodeNoi(i));
%     %sg1(i) = ref_Sg(timeFilePara, nodeNoi(i));
%     
%     x_ideal(i) = XCoor(nodeNoi(i));
% end
% for i=1:length(nodeNor)
% 
%     phiC_ideal(i) = ref_Porosity_Calcite(timeFilePara, nodeNor(i));
%     phiF_ideal(i) = ref_Porosity_Biofilm(timeFilePara, nodeNor(i));
%     Ca_ideal(i) = ref_X_lCa(timeFilePara, nodeNor(i));
%     %omega1(i) = ref_omega(timeFilePara, nodeNor(i));
%     pH_ideal(i) = ref_pH(timeFilePara, nodeNor(i));
%     poro_ideal(i) = ref_porosity(timeFilePara, nodeNor(i));
%     perm_ideal(i) = ref_Permeability(timeFilePara, nodeNor(i));
%     %sg1(i) = ref_Sg(timeFilePara, nodeNor(i));
%     
%     x_ideal(i) = x_real(i);
% end
% 
% 
% 
% load('simple_rightQ_11_4.mat');
% 
% % get coordinate vectors
% j = 1;
% for i=1:3:length(ref_Coordinates(1,:))
%     XCoor(j) = ref_Coordinates(1,i);
%     YCoor(j) = ref_Coordinates(1,i+1);
%     ZCoor(j) = ref_Coordinates(1,i+2);
%     j = j+1;
% end
% j = 1;
% 
% %plot over line
% timeFilePara = length(ref_Time);
% 
% 
% y = 0;
% z = 1.205;
% eps = 1e-6;
% j =1;
% for i=1:length(XCoor)
% 
%        if (YCoor(i) > y - eps && YCoor(i) < y + eps) % take all coordinates with YCoor=y
%        if (ZCoor(i) > z - eps && ZCoor(i) < z + eps) % take all coordinates with ZCoor=z
%         nodeNos(j) = i;
%         j = j + 1;
%        end
%        end
%     
% end
% 
% for i=1:length(nodeNos)
% 
%     phiC_simple(i) = ref_Porosity_Calcite(timeFilePara, nodeNos(i));
%     phiF_simple(i) = ref_Porosity_Biofilm(timeFilePara, nodeNos(i));
%     Ca_simple(i) = ref_X_lCa(timeFilePara, nodeNos(i));
%     %omega1(i) = ref_omega(timeFilePara, nodeNos(i));
%     pH_simple(i) = ref_pH(timeFilePara, nodeNos(i));
%     poro_simple(i) = ref_porosity(timeFilePara, nodeNos(i));
%     perm_simple(i) = ref_Permeability(timeFilePara, nodeNos(i));
%     %sg1(i) = ref_Sg(timeFilePara, nodeNos(i));
%     
%     x_simple(i) = XCoor(nodeNos(i));
% end
% % for i=1:length(nodeNor)
% % 
% %     phiC_simple(i) = ref_Porosity_Calcite(timeFilePara, nodeNor(i));
% %     phiF_simple(i) = ref_Porosity_Biofilm(timeFilePara, nodeNor(i));
% %     Ca_simple(i) = ref_X_lCa(timeFilePara, nodeNor(i));
% %     %omega1(i) = ref_omega(timeFilePara, nodeNor(i));
% %     pH_simple(i) = ref_pH(timeFilePara, nodeNor(i));
% %     poro_simple(i) = ref_porosity(timeFilePara, nodeNor(i));
% %     perm_simple(i) = ref_Permeability(timeFilePara, nodeNor(i));
% %     %sg1(i) = ref_Sg(timeFilePara, nodeNor(i));
% %     
% %     x_simple(i) = x_real(i);
% % end
% 
% 
% 
% 
% perm_simple
% perm_ideal


load('extendedShigorina.mat');

% get coordinate vectors
j = 1;
for i=1:3:length(ref_Coordinates(1,:))
    XCoor(j) = ref_Coordinates(1,i);
    YCoor(j) = ref_Coordinates(1,i+1);
    ZCoor(j) = ref_Coordinates(1,i+2);
    j = j+1;
end
j = 1;

%plot over line
timeFilePara = length(ref_Time);


y = 0;
z = 4.0150;
eps = 1e-6;
j =1;
for i=1:length(XCoor)

       if (YCoor(i) > y - eps && YCoor(i) < y + eps) % take all coordinates with YCoor=y
       if (ZCoor(i) > z - eps && ZCoor(i) < z + eps) % take all coordinates with ZCoor=z
        nodeNo(j) = i;
        j = j + 1;
       end
       end
    
end

for i=1:length(nodeNo)

    phiCShigorina(i) = ref_Porosity_Calcite(timeFilePara, nodeNo(i));
    phiFShigorina(i) = ref_Porosity_Biofilm(timeFilePara, nodeNo(i));
%     CaShigorina(i) = ref_X_lCa(timeFilePara, nodeNo(i));
%     %omega1(i) = ref_omega(timeFilePara, nodeNo(i));
%     pHShigorina(i) = ref_pH(timeFilePara, nodeNo(i));
    poroShigorina(i) = ref_porosity(timeFilePara, nodeNo(i));
    permShigorina(i) = ref_Permeability(timeFilePara, nodeNo(i));
    %sg1(i) = ref_Sg(timeFilePara, nodeNo(i));
    
    xShigorina(i) = XCoor(nodeNo(i));
    error1(i) = 0;
end


load('extendedHommel.mat');

% get coordinate vectors
j = 1;
for i=1:3:length(ref_Coordinates(1,:))
    XCoor(j) = ref_Coordinates(1,i);
    YCoor(j) = ref_Coordinates(1,i+1);
    ZCoor(j) = ref_Coordinates(1,i+2);
    j = j+1;
end
j = 1;

%plot over line
timeFilePara = length(ref_Time);


y = 0;
z = 4.0150;
eps = 1e-6;
j =1;
for i=1:length(XCoor)

       if (YCoor(i) > y - eps && YCoor(i) < y + eps) % take all coordinates with YCoor=y
       if (ZCoor(i) > z - eps && ZCoor(i) < z + eps) % take all coordinates with ZCoor=z
        nodeNo(j) = i;
        j = j + 1;
       end
       end
    
end

for i=1:length(nodeNo)

    phiCHommel(i) = ref_Porosity_Calcite(timeFilePara, nodeNo(i));
    phiFHommel(i) = ref_Porosity_Biofilm(timeFilePara, nodeNo(i));
%     CaHommel(i) = ref_X_lCa(timeFilePara, nodeNo(i));
%     %omega1(i) = ref_omega(timeFilePara, nodeNo(i));
%     pHHommel(i) = ref_pH(timeFilePara, nodeNo(i));
    poroHommel(i) = ref_porosity(timeFilePara, nodeNo(i));
    permHommel(i) = ref_Permeability(timeFilePara, nodeNo(i));
    %sg1(i) = ref_Sg(timeFilePara, nodeNo(i));
    
    xHommel(i) = XCoor(nodeNo(i));
    error1(i) = 0;
end




load('small_real.mat');

% get coordinate vectors
j = 1;
for i=1:3:length(ref_Coordinates(1,:))
    XCoor(j) = ref_Coordinates(1,i);
    YCoor(j) = ref_Coordinates(1,i+1);
    ZCoor(j) = ref_Coordinates(1,i+2);
    j = j+1;
end
j = 1;

%plot over line
timeFilePara = length(ref_Time);


y = 0;
z = 1.195;
eps = 1e-6;
j =1;
for i=1:length(XCoor)

       if (YCoor(i) > y - eps && YCoor(i) < y + eps) % take all coordinates with YCoor=y
       if (ZCoor(i) > z - eps && ZCoor(i) < z + eps) % take all coordinates with ZCoor=z
        nodeNo(j) = i;
        j = j + 1;
       end
       end
    
end

for i=1:length(nodeNo)

    phiCsmall_real(i) = ref_Porosity_Calcite(timeFilePara, nodeNo(i));
    phiFsmall_real(i) = ref_Porosity_Biofilm(timeFilePara, nodeNo(i));
    Casmall_real(i) = ref_X_lCa(timeFilePara, nodeNo(i));
    %omega1(i) = ref_omega(timeFilePara, nodeNo(i));
    pHsmall_real(i) = ref_pH(timeFilePara, nodeNo(i));
    porosmall_real(i) = ref_porosity(timeFilePara, nodeNo(i));
    permsmall_real(i) = ref_Permeability(timeFilePara, nodeNo(i));
    %sg1(i) = ref_Sg(timeFilePara, nodeNo(i));
    
    xsmall_real(i) = XCoor(nodeNo(i));
    error1(i) = 0;
end

load('small_simple.mat');

% get coordinate vectors
j = 1;
for i=1:3:length(ref_Coordinates(1,:))
    XCoor(j) = ref_Coordinates(1,i);
    YCoor(j) = ref_Coordinates(1,i+1);
    ZCoor(j) = ref_Coordinates(1,i+2);
    j = j+1;
end
j = 1;

%plot over line
timeFilePara = length(ref_Time);


y = 0;
z = 1.195;
eps = 1e-6;
j =1;
for i=1:length(XCoor)

       if (YCoor(i) > y - eps && YCoor(i) < y + eps) % take all coordinates with YCoor=y
       if (ZCoor(i) > z - eps && ZCoor(i) < z + eps) % take all coordinates with ZCoor=z
        nodeNo(j) = i;
        j = j + 1;
       end
       end
    
end

for i=1:length(nodeNo)

    phiCsmall_simple(i) = ref_Porosity_Calcite(timeFilePara, nodeNo(i));
    phiFsmall_simple(i) = ref_Porosity_Biofilm(timeFilePara, nodeNo(i));
    Casmall_simple(i) = ref_X_lCa(timeFilePara, nodeNo(i));
    %omega1(i) = ref_omega(timeFilePara, nodeNo(i));
    pHsmall_simple(i) = ref_pH(timeFilePara, nodeNo(i));
    porosmall_simple(i) = ref_porosity(timeFilePara, nodeNo(i));
    permsmall_simple(i) = ref_Permeability(timeFilePara, nodeNo(i));
    %sg1(i) = ref_Sg(timeFilePara, nodeNo(i));
    
    xsmall_simple(i) = XCoor(nodeNo(i));
    error1(i) = 0;
end






load('small_fancy.mat');

% get coordinate vectors
j = 1;
for i=1:3:length(ref_Coordinates(1,:))
    XCoor(j) = ref_Coordinates(1,i);
    YCoor(j) = ref_Coordinates(1,i+1);
    ZCoor(j) = ref_Coordinates(1,i+2);
    j = j+1;
end
j = 1;

%plot over line
timeFilePara = length(ref_Time);


y = 0;
z = 1.195;
eps = 1e-6;
j =1;
for i=1:length(XCoor)

       if (YCoor(i) > y - eps && YCoor(i) < y + eps) % take all coordinates with YCoor=y
       if (ZCoor(i) > z - eps && ZCoor(i) < z + eps) % take all coordinates with ZCoor=z
        nodeNo(j) = i;
        j = j + 1;
       end
       end
    
end

for i=1:length(nodeNo)

    phiCsmall_fancy(i) = ref_Porosity_Calcite(timeFilePara, nodeNo(i));
    phiFsmall_fancy(i) = ref_Porosity_Biofilm(timeFilePara, nodeNo(i));
    Casmall_fancy(i) = ref_X_lCa(timeFilePara, nodeNo(i));
    %omega1(i) = ref_omega(timeFilePara, nodeNo(i));
    pHsmall_fancy(i) = ref_pH(timeFilePara, nodeNo(i));
    porosmall_fancy(i) = ref_porosity(timeFilePara, nodeNo(i));
    permsmall_fancy(i) = ref_Permeability(timeFilePara, nodeNo(i));
    %sg1(i) = ref_Sg(timeFilePara, nodeNo(i));
    
    xsmall_fancy(i) = XCoor(nodeNo(i));
    error1(i) = 0;
end






load('big_simple.mat');

% get coordinate vectors
j = 1;
for i=1:3:length(ref_Coordinates(1,:))
    XCoor(j) = ref_Coordinates(1,i);
    YCoor(j) = ref_Coordinates(1,i+1);
    ZCoor(j) = ref_Coordinates(1,i+2);
    j = j+1;
end
j = 1;

%plot over line
timeFilePara = length(ref_Time);


y = 0;
z = 4.0150;
eps = 1e-6;
j =1;
for i=1:length(XCoor)

       if (YCoor(i) > y - eps && YCoor(i) < y + eps) % take all coordinates with YCoor=y
       if (ZCoor(i) > z - eps && ZCoor(i) < z + eps) % take all coordinates with ZCoor=z
        nodeNo(j) = i;
        j = j + 1;
       end
       end
    
end

for i=1:length(nodeNo)

    phiCbig_simple(i) = ref_Porosity_Calcite(timeFilePara, nodeNo(i));
    phiFbig_simple(i) = ref_Porosity_Biofilm(timeFilePara, nodeNo(i));
    Cabig_fancyimple(i) = ref_X_lCa(timeFilePara, nodeNo(i));
    %omega1(i) = ref_omega(timeFilePara, nodeNo(i));
    pHbig_fancy(i) = ref_pH(timeFilePara, nodeNo(i));
    porobig_simple(i) = ref_porosity(timeFilePara, nodeNo(i));
    permbig_simple(i) = ref_Permeability(timeFilePara, nodeNo(i));
    %sg1(i) = ref_Sg(timeFilePara, nodeNo(i));
    
    xbig_simple(i) = XCoor(nodeNo(i));
    error1(i) = 0;
end








load('big_fancy.mat');

% get coordinate vectors
j = 1;
for i=1:3:length(ref_Coordinates(1,:))
    XCoor(j) = ref_Coordinates(1,i);
    YCoor(j) = ref_Coordinates(1,i+1);
    ZCoor(j) = ref_Coordinates(1,i+2);
    j = j+1;
end
j = 1;

%plot over line
timeFilePara = length(ref_Time);


y = 0;
z = 4.0150;
eps = 1e-6;
j =1;
for i=1:length(XCoor)

       if (YCoor(i) > y - eps && YCoor(i) < y + eps) % take all coordinates with YCoor=y
       if (ZCoor(i) > z - eps && ZCoor(i) < z + eps) % take all coordinates with ZCoor=z
        nodeNo(j) = i;
        j = j + 1;
       end
       end
    
end

for i=1:length(nodeNo)

    phiCbig_fancy(i) = ref_Porosity_Calcite(timeFilePara, nodeNo(i));
    phiFbig_fancy(i) = ref_Porosity_Biofilm(timeFilePara, nodeNo(i));
    Cabig_fancy(i) = ref_X_lCa(timeFilePara, nodeNo(i));
    %omega1(i) = ref_omega(timeFilePara, nodeNo(i));
    pHbig_fancy(i) = ref_pH(timeFilePara, nodeNo(i));
    porobig_fancy(i) = ref_porosity(timeFilePara, nodeNo(i));
    permbig_fancy(i) = ref_Permeability(timeFilePara, nodeNo(i));
    %sg1(i) = ref_Sg(timeFilePara, nodeNo(i));
    
    xbig_fancy(i) = XCoor(nodeNo(i));
    error1(i) = 0;
end





load('big_real.mat');

% get coordinate vectors
j = 1;
for i=1:3:length(ref_Coordinates(1,:))
    XCoor(j) = ref_Coordinates(1,i);
    YCoor(j) = ref_Coordinates(1,i+1);
    ZCoor(j) = ref_Coordinates(1,i+2);
    j = j+1;
end
j = 1;

%plot over line
timeFilePara = length(ref_Time);


y = 0;
z = 4.0150;
eps = 1e-6;
j =1;
for i=1:length(XCoor)

       if (YCoor(i) > y - eps && YCoor(i) < y + eps) % take all coordinates with YCoor=y
       if (ZCoor(i) > z - eps && ZCoor(i) < z + eps) % take all coordinates with ZCoor=z
        nodeNo(j) = i;
        j = j + 1;
       end
       end
    
end

for i=1:length(nodeNo)

    phiCbig_real(i) = ref_Porosity_Calcite(timeFilePara, nodeNo(i));
    phiFbig_real(i) = ref_Porosity_Biofilm(timeFilePara, nodeNo(i));
    Cabig_real(i) = ref_X_lCa(timeFilePara, nodeNo(i));
    %omega1(i) = ref_omega(timeFilePara, nodeNo(i));
    pHbig_real(i) = ref_pH(timeFilePara, nodeNo(i));
    porobig_real(i) = ref_porosity(timeFilePara, nodeNo(i));
    permbig_real(i) = ref_Permeability(timeFilePara, nodeNo(i));
    %sg1(i) = ref_Sg(timeFilePara, nodeNo(i));
    
    xbig_real(i) = XCoor(nodeNo(i));
    error1(i) = 0;
end

xField = xbig_real;
Kinit = zeros(length(xbig_real),1);
for i=1:length(xbig_real);
    if(xField(i)<4.1)
        Kinit(i) = 1.645e-12;
    else
        Kinit(i) = 1.0856e-14;
    end
end

Fig = figure;
figure(Fig);

h1_plot = plot(xbig_simple, phiCbig_simple, 'r-', xbig_fancy, phiCbig_fancy, 'g-', xbig_real, phiCbig_real, 'b-', xsmall_simple, phiCsmall_simple, 'r--', xsmall_fancy, phiCsmall_fancy, 'g--', xsmall_real, phiCsmall_real, 'b--', xShigorina, phiCShigorina, 'k:o', xHommel, phiCHommel, 'k:+');
h1_leg = legend('2014: 8mx8m, simple inj.','2014: 8mx8m, ideal inj.', '2014: 8mx8m, real inj.', '2014: 2.4mx2.4m, simple inj.', '2014: 2.4mx2.4m, ideal inj.', '2014: 2.4mx2.4m, real inj.', '2018: 8mx50m, real inj. with 2014 parameters', '2018: 8mx50m, real inj., Hommel 2015 parameters');
h1_xlab = xlabel('Radius [m]');
h1_ylab = ylabel('Calcite volume fraction [-]');
% h1_title = title('Calcite, comparison of simulations according different injection schemes');
axis([0.23 8 0 0.02]);


set(gca,'FontSize',16)
set(h1_leg,'FontSize',16)
set(h1_xlab,'FontSize',16)
set(h1_ylab,'FontSize',16)
set(h1_plot,'LineWidth',2);
set(h1_plot,'MarkerSize',10);

% axis([0 900 0 4.8]);
set(Fig,'position',[0 0, 1000 500])
set(Fig,'PaperPositionMode','Auto')
saveas(Fig,'./new_graph','epsc')

Fig = figure;
figure(Fig);

h1_plot = plot(xbig_simple, phiFbig_simple, 'r-', xbig_fancy, phiFbig_fancy, 'g-', xbig_real, phiFbig_real, 'b-', xsmall_simple, phiFsmall_simple, 'r--', xsmall_fancy, phiFsmall_fancy, 'g--', xsmall_real, phiFsmall_real, 'b--', xShigorina, phiFShigorina, 'k:o', xHommel, phiFHommel, 'k:+');
h1_leg = legend('2014: 8mx8m, simple inj.','2014: 8mx8m, ideal inj.', '2014: 8mx8m, real inj.', '2014: 2.4mx2.4m, simple inj.', '2014: 2.4mx2.4m, ideal inj.', '2014: 2.4mx2.4m, real inj.', '2018: 8mx50m, real inj. with 2014 parameters', '2018: 8mx50m, real inj., Hommel 2015 parameters');
% h1_plot = plot(xbig_simple, phiFbig_simple, 'r-', xbig_fancy, phiFbig_fancy, 'g-', xbig_real, phiFbig_real, 'b-', xsmall_simple, phiFsmall_simple, 'r--', xsmall_fancy, phiFsmall_fancy, 'g--', xsmall_real, phiFsmall_real, 'b--', x_simple, phiF_simple, 'k:',x_ideal, phiF_ideal, 'k:o', x_real, phiF_real, 'k--+');
% h1_leg = legend('Shigorina 2014: large, simple inj.','Shigorina 2014: large, ideal inj.', 'Shigorina 2014: large, real inj.', 'Shigorina 2014: small, simple inj.','Shigorina 2014: small, ideal inj.', 'Shigorina 2014: small, real inj.', 'Hommel et al. 2015: small, simple inj.', 'Hommel et al. 2015: small, ideal inj.', 'Hommel et al. 2015: small, real inj.');
h1_xlab = xlabel('Radius [m]');
h1_ylab = ylabel('Biofilm volume fraction [-]');
% h1_title = title('Biofilm, comparison of simulations according different injection schemes');
axis([0.23 8 0 0.0009]);


set(gca,'FontSize',16)
set(h1_leg,'FontSize',16)
set(h1_xlab,'FontSize',16)
set(h1_ylab,'FontSize',16)
set(h1_plot,'LineWidth',2);
set(h1_plot,'MarkerSize',10);

% axis([0 900 0 4.8]);
set(Fig,'position',[0 0, 1000 500])
set(Fig,'PaperPositionMode','Auto')
saveas(Fig,'./new_graph','epsc')


Fig = figure;
figure(Fig);

h1_plot = plot(xField, Kinit, 'm-', xbig_simple, permbig_simple, 'r-', xbig_fancy, permbig_fancy, 'g-', xbig_real, permbig_real, 'b-', xsmall_simple, permsmall_simple, 'r--', xsmall_fancy, permsmall_fancy, 'g--', xsmall_real, permsmall_real, 'b--', xShigorina, permShigorina, 'k:o', xHommel, permHommel, 'k:+');
h1_leg = legend('Initial Permeability for 8mx8m case','2014: 8mx8m, simple inj.','2014: 8mx8m, ideal inj.', '2014: 8mx8m, real inj.', '2014: 2.4mx2.4m, simple inj.', '2014: 2.4mx2.4m, ideal inj.', '2014: 2.4mx2.4m, real inj.', '2018: 8mx50m, real inj. with 2014 parameters', '2018: 8mx50m, real inj., Hommel 2015 parameters');
% h1_plot = plot(xbig_simple, permbig_simple, 'r-', xbig_fancy, permbig_fancy, 'g-', xbig_real, permbig_real, 'b-', xsmall_simple, permsmall_simple, 'r--', xsmall_fancy, permsmall_fancy, 'g--', xsmall_real, permsmall_real, 'b--', x_simple, perm_simple, 'k:',x_ideal, perm_ideal, 'k:o', x_real, perm_real, 'k--+');
% h1_leg = legend('Shigorina 2014: large, simple inj.','Shigorina 2014: large, ideal inj.', 'Shigorina 2014: large, real inj.', 'Shigorina 2014: small, simple inj.','Shigorina 2014: small, ideal inj.', 'Shigorina 2014: small, real inj.', 'Hommel et al. 2015: small, simple inj.', 'Hommel et al. 2015: small, ideal inj.', 'Hommel et al. 2015: small, real inj.');
h1_xlab = xlabel('Radius [m]');
h1_ylab = ylabel('Permeability [m^2]');
% h1_title = title('Permeability, comparison of simulations according different injection schemes');
axis([0.23 8 0 1.8*10^-12]);


set(gca,'FontSize',16)
set(h1_leg,'FontSize',16)
set(h1_xlab,'FontSize',16)
set(h1_ylab,'FontSize',16)
set(h1_plot,'LineWidth',2);
set(h1_plot,'MarkerSize',10);

% axis([0 900 0 4.8]);
set(Fig,'position',[0 0, 1000 500])
set(Fig,'PaperPositionMode','Auto')
saveas(Fig,'./new_graph','epsc')

Fig = figure;
figure(Fig);

h1_plot = plot(xbig_simple, porobig_simple, 'r-', xbig_fancy, porobig_fancy, 'g-', xbig_real, porobig_real, 'b-', xsmall_simple, porosmall_simple, 'r--', xsmall_fancy, porosmall_fancy, 'g--', xsmall_real, porosmall_real, 'b--', xShigorina, poroShigorina, 'k:o', xHommel, poroHommel, 'k:+');
h1_leg = legend('2014: 8mx8m, simple inj.','2014: 8mx8m, ideal inj.', '2014: 8mx8m, real inj.', '2014: 2.4mx2.4m, simple inj.', '2014: 2.4mx2.4m, ideal inj.', '2014: 2.4mx2.4m, real inj.', '2018: 8mx50m, real inj. with 2014 parameters', '2018: 8mx50m, real inj., Hommel 2015 parameters');
% h1_plot = plot(xbig_simple, porobig_simple, 'r-', xbig_fancy, porobig_fancy, 'g-', xbig_real, porobig_real, 'b-', xsmall_simple, porosmall_simple, 'r--', xsmall_fancy, porosmall_fancy, 'g--', xsmall_real, porosmall_real, 'b--', x_simple, poro_simple, 'k:',x_ideal, poro_ideal, 'k:o', x_real, poro_real, 'k--+');
% h1_leg = legend('Shigorina 2014: large, simple inj.','Shigorina 2014: large, ideal inj.', 'Shigorina 2014: large, real inj.', 'Shigorina 2014: small, simple inj.','Shigorina 2014: small, ideal inj.', 'Shigorina 2014: small, real inj.', 'Hommel et al. 2015: small, simple inj.', 'Hommel et al. 2015: small, ideal inj.', 'Hommel et al. 2015: small, real inj.');
h1_xlab = xlabel('Radius [m]');
h1_ylab = ylabel('Porosity [-]');
% h1_title = title('Porosity, comparison of simulations according different injection schemes');
axis([0.23 8 0.1 0.122]);


set(gca,'FontSize',16)
set(h1_leg,'FontSize',16)
set(h1_xlab,'FontSize',16)
set(h1_ylab,'FontSize',16)
set(h1_plot,'LineWidth',2);
set(h1_plot,'MarkerSize',10);

% axis([0 900 0 4.8]);
set(Fig,'position',[0 0, 1000 500])
set(Fig,'PaperPositionMode','Auto')
saveas(Fig,'./new_graph','epsc')


% Fig = figure;
% figure(Fig);
% 
% h1_plot = plot(x_simple, perm_simple, 'k:',x_ideal, perm_ideal, 'k:o', x_real, perm_real, 'k--+', x_realnew, perm_realnew, 'm--p');
% h1_leg = legend('Hommel et al. 2015: small, simple inj.', 'Hommel et al. 2015: small, ideal inj.', 'Hommel et al. 2015: small, real inj.');
% h1_xlab = xlabel('Radius [m]');
% h1_ylab = ylabel('Permeability [m^2]');
% % h1_title = title('Permeability, comparison of simulations according different injection schemes');
% axis([0.23 2.4 0 1.8*10^-12]);
% 
% 
% set(gca,'FontSize',16)
% set(h1_leg,'FontSize',16)
% set(h1_xlab,'FontSize',16)
% set(h1_ylab,'FontSize',16)
% set(h1_plot,'LineWidth',2);
% 
% % axis([0 900 0 4.8]);
% set(Fig,'position',[0 0, 1000 500])
% set(Fig,'PaperPositionMode','Auto')
% saveas(Fig,'./new_graph','epsc')

