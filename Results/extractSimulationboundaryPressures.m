clear;

% load('big_real.mat');
load('Gorgas1pRealkg.mat');

% get coordinate vectors
j = 1;
for i=1:3:length(ref_Coordinates(1,:))
    XCoor(j) = ref_Coordinates(1,i);
    YCoor(j) = ref_Coordinates(1,i+1);
    ZCoor(j) = ref_Coordinates(1,i+2);
    j = j+1;
end
j = 1;

% %plot over line
% timeFilePara = length(ref_Time);

% r = 0.2286;
r = 63.3;
dr = 0.01;
rSquareBig = (r+dr)*(r+dr);
rSquareSmall = (r-dr)*(r-dr);
z = 4.0;
dz = 0.01;
eps = 1e-6;
j =1;

for i=1:length(XCoor)

       if ( XCoor(i)*XCoor(i) + YCoor(i)*YCoor(i) < rSquareBig && XCoor(i)*XCoor(i) + YCoor(i)*YCoor(i) > rSquareSmall) % take all coordinates with XCoor,YCoor= r      
       if (ZCoor(i) > z - dz - eps && ZCoor(i) < z + dz + eps) % take all coordinates ZCoor within z+-dz
          nodeNo(j) = i;
          j = j + 1;
       end
       end
    
end

time = zeros(length(ref_Time),1);
pBC = zeros(length(ref_Time),length(nodeNo));
pMeanBC = zeros(length(ref_Time),1);

for j=1:length(ref_Time)
    
    time(j) = ref_Time(j)/3600;
    sumP = 0;
    
    for i=1:length(nodeNo)
        pBC(j,i) = ref_pl(j, nodeNo(i));
        sumP = sumP + ref_pl(j, nodeNo(i));
    end
    
    pMeanBC(j) = sumP/length(nodeNo);
end

pfactor = pMeanBC/pMeanBC(1);

plot(pfactor);


