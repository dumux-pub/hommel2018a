clear;

% load('big_real.mat');
% load('Gorgas1pReal.mat');
% load('ExtendedGorgas/extendedGorgas.mat');
% load('GorgasNew/extendedShigorina.mat');
load('GorgasNew/extendedHommel.mat');

% get coordinate vectors
j = 1;
for i=1:3:length(ref_Coordinates(1,:))
    XCoor(j) = ref_Coordinates(1,i);
    YCoor(j) = ref_Coordinates(1,i+1);
    ZCoor(j) = ref_Coordinates(1,i+2);
    j = j+1;
end
j = 1;

% %plot over line
% timeFilePara = length(ref_Time);

% r = 0.2286;
r = 0.2286;
rSquare = r*r;
z = 4.0;
dz = 0.025;
eps = 1e-6;
j =1;

for i=1:length(XCoor)

       if ( XCoor(i)*XCoor(i) + YCoor(i)*YCoor(i) < rSquare + eps) % take all coordinates with XCoor,YCoor= r
       if (ZCoor(i) > z - dz - eps && ZCoor(i) < z + dz + eps) % take all coordinates ZCoor within z+-dz
        nodeNo(j) = i;
        j = j + 1;
       end
       end
    
end

time = zeros(length(ref_Time),1);
p = zeros(length(ref_Time),length(nodeNo));
pMean = zeros(length(ref_Time),1);

for j=1:length(ref_Time)
    
    time(j) = ref_Time(j)/3600;
    sumP = 0;
    
    for i=1:length(nodeNo)
        p(j,i) = ref_pw(j, nodeNo(i));
        sumP = sumP + ref_pw(j, nodeNo(i));
    end
    
    pMean(j) = sumP/length(nodeNo);
end

