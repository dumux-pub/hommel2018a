// -**- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 * \ingroup TwoPICPModel
 * \file
 *
 * \brief Defines default values for most properties required by the
 *        fully implicit two-phase induced calcium carbonate precipitation model.
 */
#ifndef DUMUX_2PICP_PROPERTY_DEFAULTS_HH
#define DUMUX_2PICP_PROPERTY_DEFAULTS_HH

#include "dumux/porousmediumflow/2pncmin/implicit/indices.hh"
#include "model.hh"
#include "fluxvariables.hh"
#include "volumevariables.hh"
#include "properties.hh"

#include <dumux/porousmediumflow/2pnc/implicit/newtoncontroller.hh>
#include <dumux/porousmediumflow/implicit/darcyfluxvariables.hh>
#include <dumux/material/spatialparams/implicit.hh>
#include <dumux/material/fluidmatrixinteractions/2p/thermalconductivitysomerton.hh>

namespace Dumux
{

namespace Properties {
//////////////////////////////////////////////////////////////////
// Property values
//////////////////////////////////////////////////////////////////

/*!
 * \brief Set the property for the number of secondary components.
 * Secondary components are components calculated from
 * primary components by equilibrium relations and
 * do not have mass balance equation on their own.
 * These components are important in the context of bio-mineralization applications.
 * We just forward the number from the fluid system
 *
 */
SET_PROP(TwoPICP, NumSecComponents)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;

public:
    static const int value = FluidSystem::numSecComponents;

};
/*!
 * \brief Set the property for the number of solid phases, excluding the non-reactive matrix.
 *
 * We just forward the number from the fluid system
 *
 */
SET_PROP(TwoPICP, NumSPhases)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;

public:
    static const int value = FluidSystem::numSPhases;
};

/*!
 * \brief Set the property for the number of equations.
 * For each component and each precipitated mineral/solid phase one equation has to
 * be solved.
 */
SET_PROP(TwoPICP, NumEq)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;

public:
    static const int value = FluidSystem::numComponents + FluidSystem::numSPhases;
};

/*!
 * \brief The fluid state which is used by the volume variables to
 *        store the thermodynamic state. This should be chosen
 *        appropriately for the model ((non-)isothermal, equilibrium, ...).
 *        This can be done in the problem.
 */
SET_PROP(TwoPICP, FluidState){
    private:
        typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
        typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    public:
        typedef CompositionalSecCompFluidState<Scalar, FluidSystem> type;
};

//! Use the 2picp local residual operator
SET_TYPE_PROP(TwoPICP,
              LocalResidual,
              TwoPICPLocalResidual<TypeTag>);
////! Use the 2pncmin local residual operator
//SET_TYPE_PROP(TwoPICP,
//              LocalResidual,
//              TwoPNCMinLocalResidual<TypeTag>);

//! the Model property
SET_TYPE_PROP(TwoPICP, Model, TwoPICPModel<TypeTag>);

//! the VolumeVariables property
SET_TYPE_PROP(TwoPICP, VolumeVariables, TwoPICPVolumeVariables<TypeTag>);

//! the FluxVariables property
SET_TYPE_PROP(TwoPICP, FluxVariables, TwoPICPFluxVariables<TypeTag>);

//! The indices required by the isothermal 2pNcMin model
SET_TYPE_PROP(TwoPICP, Indices, TwoPNCMinIndices <TypeTag, /*PVOffset=*/0>);

//! disable useSalinity for the calculation of osmotic pressure by default
//SET_BOOL_PROP(TwoPICP, useSalinity, false);

//! do not account for dispersion if not desired
SET_BOOL_PROP(TwoPICP, UseDispersion, true);
//SET_BOOL_PROP(TwoPICP, UseDispersion, false);

//! Somerton is used as default model to compute the effective thermal heat conductivity
SET_PROP(TwoPICPNI, ThermalConductivityModel)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
public:
    typedef ThermalConductivitySomerton<Scalar, Indices> type;
//    typedef ThermalConductivityAverage<Scalar> type;
};

//////////////////////////////////////////////////////////////////
// Property values for isothermal model required for the general non-isothermal model
//////////////////////////////////////////////////////////////////

// set isothermal Model
SET_TYPE_PROP(TwoPICPNI, IsothermalModel, TwoPICPModel<TypeTag>);

// set isothermal FluxVariables
SET_TYPE_PROP(TwoPICPNI, IsothermalFluxVariables, TwoPICPFluxVariables<TypeTag>);

//set isothermal VolumeVariables
SET_TYPE_PROP(TwoPICPNI, IsothermalVolumeVariables, TwoPICPVolumeVariables<TypeTag>);

//set isothermal LocalResidual
SET_TYPE_PROP(TwoPICPNI, IsothermalLocalResidual, TwoPICPLocalResidual<TypeTag>);
////set isothermal LocalResidual
//SET_TYPE_PROP(TwoPICPNI, IsothermalLocalResidual, TwoPNCMinLocalResidual<TypeTag>);

//set isothermal Indices
SET_TYPE_PROP(TwoPICPNI, IsothermalIndices, TwoPNCMinIndices<TypeTag, /*PVOffset=*/0>);

////! the FluidState property
//SET_TYPE_PROP(TwoPICPNI, IsoFluidState, CompositionalSecCompFluidState<TypeTag>);

//set isothermal NumEq
SET_PROP(TwoPICPNI, IsothermalNumEq)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;

public:
    static const int value = FluidSystem::numComponents + FluidSystem::numSPhases; // +1 is added by default
};

}
}

#endif
