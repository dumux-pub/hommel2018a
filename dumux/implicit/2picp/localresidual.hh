// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Element-wise calculation of the Jacobian matrix for problems
 *        using the fully implicit two-phase induced calcium carbonate
 *        precipitation model.
 */

#ifndef DUMUX_2PICP_LOCAL_RESIDUAL_HH
#define DUMUX_2PICP_LOCAL_RESIDUAL_HH

#include "properties.hh"
#include <dumux/porousmediumflow/2pncmin/implicit/localresidual.hh>

namespace Dumux
{
/*!
 * \ingroup TwoPICPModel
 * \ingroup ImplicitLocalResidual
 * \brief Element-wise calculation of the Jacobian matrix for problems
 *        using the two-phase n-component mineralization fully implicit model.
 *
 * This class is used to fill the gaps in ImplicitLocalResidual as well as
 * the TwoPNCLocalResidual for the fully implicit two-phase
 * induced calcium carbonate precipitation model..
 */
template<class TypeTag>
class TwoPICPLocalResidual: public TwoPNCMinLocalResidual<TypeTag>
{
protected:
    typedef TwoPNCMinLocalResidual<TypeTag> ParentType;
    typedef TwoPICPLocalResidual<TypeTag> ThisType;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;
    typedef typename GET_PROP_TYPE(TypeTag, ElementSolutionVector) ElementSolutionVector;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    typedef typename GET_PROP_TYPE(TypeTag, LocalResidual) Implementation;


    enum
    {
        numEq = GET_PROP_VALUE(TypeTag, NumEq),
        numPhases = GET_PROP_VALUE(TypeTag, NumPhases),
        numSPhases = GET_PROP_VALUE(TypeTag, NumSPhases),
        numComponents = GET_PROP_VALUE(TypeTag, NumComponents),

        replaceCompEqIdx = GET_PROP_VALUE(TypeTag, ReplaceCompEqIdx),

        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,

        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx,

        wCompIdx = FluidSystem::wCompIdx,
        nCompIdx = FluidSystem::nCompIdx,

        conti0EqIdx = Indices::conti0EqIdx,

        wPhaseOnly = Indices::wPhaseOnly,
        nPhaseOnly = Indices::nPhaseOnly,
        bothPhases = Indices::bothPhases,

    };

    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementBoundaryTypes) ElementBoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) SpatialParams;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

    bool useDispersion_ = GET_PROP_VALUE(TypeTag, UseDispersion);

public:

    /*!
     * \brief Evaluates the dispersive mass flux of all components over
     *        a face of a sub-control volume. A special function is necessary here,
     *        as the induced calcium carbonate precipitation model consideres
     *        also dispersion.
     *
     * \param flux The flux over the sub-control-volume face for each component
     * \param fluxVars The flux variables at the current sub-control-volume face
     */
    void computeDiffusiveFlux(PrimaryVariables &flux, const FluxVariables &fluxVars) const
    {
        //Loop calculates the diffusive flux for every component in a phase. The amount of moles of a component
        //(eg Air in liquid) in a phase
        //which is not the main component (eg. H2O in the liquid phase) moved from i to j equals the amount of moles moved
        //from the main component in a phase (eg. H2O in the liquid phase) from j to i. So two fluxes in each component loop
        // are calculated in the same phase.

        if(useDispersion_) //account for dispersion
            for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
            {
                for (int compIdx = 0; compIdx < numComponents; ++compIdx)
                {
                    //only consider dispersion of all components in the wetting (water) phase or of water in the gas phase!
                    if((compIdx != phaseIdx && phaseIdx == wPhaseIdx) || compIdx == wCompIdx)
                    {
                        const auto diffCont = -
                                        fluxVars.molarDensity(wPhaseIdx)*
                                        (fluxVars.normalDisp(wPhaseIdx, compIdx)
                                        *fluxVars.moleFractionGrad(phaseIdx, compIdx));

                        //add diffusive fluxes only to the component balances
                        if (replaceCompEqIdx != (conti0EqIdx + compIdx))
                            flux[conti0EqIdx + compIdx] += diffCont;
                        if (replaceCompEqIdx != (conti0EqIdx + phaseIdx))
                            flux[conti0EqIdx + phaseIdx] -= diffCont;
                    }
                }
            }
        else
            ParentType::computeDiffusiveFlux(flux, fluxVars);
    }
};

} // end namespace

#endif
