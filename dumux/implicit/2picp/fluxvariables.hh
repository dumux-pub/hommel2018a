// -**- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Contains the data which is required to calculate
 *        all fluxes of components over a face of a finite volume for
 *        the fully implicit two-phase induced calcium carbonate precipitation model.
 */
#ifndef DUMUX_2PICP_FLUX_VARIABLES_HH
#define DUMUX_2PICP_FLUX_VARIABLES_HH

#include <dumux/common/math.hh>
#include <dumux/common/spline.hh>
#include <dumux/porousmediumflow/2pncmin/implicit/fluxvariables.hh>
#include "properties.hh"

namespace Dumux
{

/*!
 * \ingroup TwoPICPModel
 * \ingroup ImplicitFluxVariables
 * \brief Contains the data which is required to calculate
 *        all fluxes of components over a face of a finite volume for
 *        the fully implicit two-phase induced calcium carbonate precipitation model.
 * This means pressure and concentration gradients, phase densities at
 * the integration point, etc.
 */

template <class TypeTag>
class TwoPICPFluxVariables : public TwoPNCMinFluxVariables<TypeTag>
{
    friend typename GET_PROP_TYPE(TypeTag, BaseFluxVariables); // be friends with base class
    friend class TwoPNCMinFluxVariables<TypeTag>; // be friends with parent class
    friend class TwoPNCFluxVariables<TypeTag>; // be friends with parent's parent class

    typedef TwoPNCMinFluxVariables<TypeTag> ParentType;
    typedef TwoPICPFluxVariables<TypeTag> ThisType;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GridView::ctype CoordScalar;
    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

    enum {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,
        numPhases = GET_PROP_VALUE(TypeTag, NumPhases),
        numComponents = GET_PROP_VALUE(TypeTag, NumComponents),
    };

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef Dune::FieldVector<CoordScalar, dimWorld> DimWorldVector;
    typedef Dune::FieldMatrix<CoordScalar, dimWorld, dimWorld> DimWorldMatrix;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {
        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx,
        wCompIdx  = FluidSystem::wCompIdx,
    };

public:
    /*!
     * \brief Compute / update the flux variables
     *
     * \param problem The problem
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param fIdx The local index of the SCV (sub-control-volume) face
     * \param elemVolVars The volume variables of the current element
     * \param onBoundary A boolean variable to specify whether the flux variables
     * are calculated for interior SCV faces or boundary faces, default=false
     */
    void update(const Problem &problem,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                const int fIdx,
                const ElementVolumeVariables &elemVolVars,
                const bool onBoundary = false)
    {
        ParentType::update(problem, element, fvGeometry, fIdx, elemVolVars, onBoundary);

        useDispersion_ = GET_PROP_VALUE(TypeTag, UseDispersion);
        if(useDispersion_) //account for dispersion
            calculatePorousDispersionTensor_(problem, element, elemVolVars);
    }

     /*!
     * \brief Return the absolute pressure gradient for each fluid phase.
     */
    Scalar absgradp(int phaseIdx) const
    {
         Scalar absgradp_ = 0;
         for (int i=0;i<dim;i++)
         {
             absgradp_ += this->potentialGrad_[phaseIdx][i] * this->potentialGrad_[phaseIdx][i];
         }
         absgradp_ = sqrt(absgradp_);
         return absgradp_;
    }

    /*!
    * \brief Return the Dispersion coefficient normal to the scv face for each component in each fluid phase.
    */
    const DimWorldVector &normalDisp(int phaseIdx, int compIdx) const
    {
        return normalDisp_[phaseIdx][compIdx];
    }

protected:
    /*!
    * \brief Calculation of the dispersion
    *
    *        \param problem The considered problem file
    *        \param element The considered element of the grid
    *        \param elemDat The parameters stored in the considered element
    */
    void calculatePorousDispersionTensor_(const Problem &problem,
                                          const Element &element,
                                          const ElementVolumeVariables &elemVolVars)
    {

        // calculate the mean intrinsic permeability
        const auto& spatialParams = problem.spatialParams();
        DimWorldMatrix K(0.0);
        const auto& volVarsI = elemVolVars[this->face().i];
        const auto& volVarsJ = elemVolVars[this->face().j];

        if (GET_PROP_VALUE(TypeTag, ImplicitIsBox))
        {
            auto Ki = spatialParams.intrinsicPermeability(element, this->fvGeometry_(), this->face().i);
            Ki *= volVarsI.permeabilityFactor();

            auto Kj = spatialParams.intrinsicPermeability(element, this->fvGeometry_(), this->face().j);
            Kj *= volVarsJ.permeabilityFactor();

            spatialParams.meanK(K, Ki, Kj);
        }
        else
        {
            const Element& elementI = this->fvGeometry_().neighbors[this->face().i];
            FVElementGeometry fvGeometryI;
            fvGeometryI.subContVol[0].global = elementI.geometry().center();

            const Element& elementJ = this->fvGeometry_().neighbors[this->face().j];
            FVElementGeometry fvGeometryJ;
            fvGeometryJ.subContVol[0].global = elementJ.geometry().center();

            auto Ki = spatialParams.intrinsicPermeability(elementI, fvGeometryI, 0);
            Ki *= volVarsI.permeabilityFactor();

            auto Kj = spatialParams.intrinsicPermeability(elementJ, fvGeometryJ, 0);
            Kj *= volVarsJ.permeabilityFactor();

            spatialParams.meanK(K, Ki, Kj);
        }

        dispersionTensor_ = 0.0;
//        std::cout<<"test1"<<std::endl;
//        normalDisp_ = 0.0; //TODO:this produces a segmentation fault!!! Why??  Now set to zero further below in the loop itself, which works.
//        std::cout<<"test2"<<std::endl;

        //calculate dispersivity at the interface: [0]: alphaL = longitudinal disp. [m], [1] alphaT = transverse disp. [m]
        Scalar dispersivity[2];
        dispersivity[0] = alphaL_;//0.025; //alphaL [m]
        dispersivity[1] = alphaL_/10; //alphaT [m]

        //calculate velocity  v = 1/phi * vDarcy = -1/phi *mobility* K * grad(p)
        DimWorldVector velocity[numPhases];
        Scalar vNorm[numPhases];
        Scalar S[numPhases];
        Scalar porosity = 0.5 * (volVarsI.porosity()  + volVarsJ.porosity());


        int numPhasesPresent = numPhases;
        if(volVarsI.saturation(wPhaseIdx)==1.0 && volVarsJ.saturation(wPhaseIdx)==1.0)
            numPhasesPresent=1;

        for (int phaseIdx=0; phaseIdx < numPhasesPresent; phaseIdx++)
        {
//            K.mv(ParentType::potentialGrad_[phaseIdx], velocity[phaseIdx]);
            K.mv(this->potentialGrad_[phaseIdx], velocity[phaseIdx]);
            velocity[phaseIdx] *= 0.5 * (volVarsI.mobility(phaseIdx)  + volVarsJ.mobility(phaseIdx));

            S[phaseIdx] = 0.5 * (volVarsI.saturation(phaseIdx)  + volVarsJ.saturation(phaseIdx));

            velocity[phaseIdx] /= - porosity;

            //normalized velocity
            vNorm[phaseIdx] = 0;
//              vNorm[phaseIdx] = velocity[phaseIdx].two_norm();
            for (int i=0; i<dim; i++)
            {
                vNorm[phaseIdx] +=  velocity[phaseIdx][i]*velocity[phaseIdx][i];
            }
            vNorm[phaseIdx] = sqrt(vNorm[phaseIdx]);

            for (int compIdx=0; compIdx < numComponents; compIdx++)
            {
                //matrix multiplication of the velocity at the interface: vv^T
                dispersionTensor_[phaseIdx][compIdx] = 0;
                for (int i=0; i<dim; i++)
                for (int j = 0; j<dim; j++)
                    dispersionTensor_[phaseIdx][compIdx][i][j] = velocity[phaseIdx][i]*velocity[phaseIdx][j];

                //normalize velocity product --> vv^T/||v||, [m/s]
                dispersionTensor_[phaseIdx][compIdx] /= vNorm[phaseIdx];
                if (vNorm[phaseIdx] < 1e-20)
                    dispersionTensor_[phaseIdx][compIdx] = 0;

                //multiply with dispersivity difference: vv^T/||v||*(alphaL - alphaT), [m^2/s] --> alphaL = longitudinal disp., alphaT = transv. disp.
                dispersionTensor_[phaseIdx][compIdx] *= (dispersivity[0] - dispersivity[1]);

                //add ||v||*alphaT to the main diagonal:vv^T/||v||*(alphaL - alphaT) + ||v||*alphaT, [m^2/s]
                for (int i = 0; i<dim; i++)
                {
                    normalDisp_[phaseIdx][compIdx][i] = 0.0;
                    dispersionTensor_[phaseIdx][compIdx][i][i] += vNorm[phaseIdx]*dispersivity[1];

                    //multiply all entries of the Tensor by Saturation and porosity
                    for (int j = 0; j<dim; j++)
                    {
                        dispersionTensor_[phaseIdx][compIdx][i][j] *= S[phaseIdx]*porosity;
                    }
                    //add the porous diffusion coefficient
                    dispersionTensor_[phaseIdx][compIdx][i][i] += ParentType::porousDiffCoeff_[phaseIdx][compIdx];
                }

                dispersionTensor_[phaseIdx][compIdx].mv(this->face().normal,normalDisp_[phaseIdx][compIdx]);
            }
        }
    }

    //! the dispersion tensor in the porous medium
    Dune::FieldMatrix<DimWorldMatrix, numPhases, numComponents> dispersionTensor_;
    Dune::FieldMatrix<DimWorldVector, numPhases, numComponents> normalDisp_;

    //the longitudinal dispersion length
    Scalar alphaL_ = 0.0;//0.025; //alphaL [m]
    bool useDispersion_ = false;
};

} // end namespace

#endif
