// -**- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Contains the quantities which are constant within a
 *        finite volume in the fully implicit two-phase induced calcium
 *        carbonate precipitation model.
 */
#ifndef DUMUX_2PICP_VOLUME_VARIABLES_HH
#define DUMUX_2PICP_VOLUME_VARIABLES_HH

#include <vector>
#include <iostream>

#include <dumux/common/math.hh>
#include <dumux/implicit/model.hh>
#include <dumux/material/fluidstates/compositionalseccompfluidstate.hh>
#include <dumux/material/constraintsolvers/computefromreferencephase.hh>
#include <dumux/material/constraintsolvers/miscible2pnccomposition.hh>
#include <dumux/porousmediumflow/2pncmin/implicit/volumevariables.hh>

#include "properties.hh"
#include <dumux/porousmediumflow/2pncmin/implicit/indices.hh>

#include <dumux/material/binarycoefficients/brine_co2.hh>

namespace Dumux
{

/*!
 * \ingroup TwoPICPModel
 * \ingroup ImplicitVolumeVariables
 * \brief Contains the quantities which are are constant within a
 *        finite volume in the two-phase, n-component model.
 */
template <class TypeTag>
class TwoPICPVolumeVariables : public TwoPNCMinVolumeVariables<TypeTag>
{
    typedef TwoPNCMinVolumeVariables<TypeTag> ParentType;
//    typedef TwoPNCVolumeVariables<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) Implementation;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLawParams) MaterialLawParams;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    typedef typename GET_PROP_TYPE(TypeTag, CO2Tables) CO2Tables;
    typedef typename GET_PROP_TYPE(TypeTag, Chemistry) Chemistry;
    typedef Dumux::BinaryCoeff::Brine_CO2<Scalar, CO2Tables, true> Brine_CO2;

    enum
    {
        dim = GridView::dimension,
        dimWorld=GridView::dimensionworld,

        numPhases = GET_PROP_VALUE(TypeTag, NumPhases),
        numSPhases =  GET_PROP_VALUE(TypeTag, NumSPhases),
        numComponents = GET_PROP_VALUE(TypeTag, NumComponents),
        numMajorComponents = GET_PROP_VALUE(TypeTag, NumMajorComponents),
        numSecComponents = GET_PROP_VALUE(TypeTag, NumSecComponents),

        // formulations
        formulation = GET_PROP_VALUE(TypeTag, Formulation),
        pwsn = TwoPNCFormulation::pwsn,
        pnsw = TwoPNCFormulation::pnsw,

        // phase indices
        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx,

        // component indices
        wCompIdx = FluidSystem::wCompIdx,
        nCompIdx = FluidSystem::nCompIdx,
        NaIdx    = FluidSystem::NaIdx,
        ClIdx = FluidSystem::ClIdx,
        CaIdx    = FluidSystem::CaIdx,
        CO2Idx    = FluidSystem::CO2Idx,

//        HIdx = FluidSystem::HIdx,
//        UreaIdx = FluidSystem::UreaIdx,
        CO3Idx = FluidSystem::CO3Idx,
        HCO3Idx = FluidSystem::HCO3Idx,
        NH4Idx = FluidSystem::NH4Idx,

        // phase presence enums
        nPhaseOnly = Indices::nPhaseOnly,
        wPhaseOnly = Indices::wPhaseOnly,
        bothPhases = Indices::bothPhases,

        // primary variable indices
        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,
    };

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef typename Grid::ctype CoordScalar;
    typedef Dumux::Miscible2pNCComposition<Scalar, FluidSystem> Miscible2pNCComposition;
    typedef Dumux::ComputeFromReferencePhase<Scalar, FluidSystem> ComputeFromReferencePhase;

    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dim : 0 };
public:

    typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;

    /*!
     * \copydoc ImplicitVolumeVariables::update
     */
    void update(const PrimaryVariables &priVars,
                const Problem &problem,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                int scvIdx,
                bool isOldSol)
    {

        completeFluidState(priVars, problem, element, fvGeometry, scvIdx, this->fluidState_, isOldSol);

        ParentType::update(priVars, problem, element, fvGeometry, scvIdx, isOldSol);

        //change the salinity calculation of the parent type as only Na, Cl, and Ca, affect the salinity
        ParentType::salinity_= 0.0;
        ParentType::moleFractionSalinity_ = 0.0;
        for (int compIdx = NaIdx; compIdx<= CaIdx ; compIdx++)  //salinity = XlNa + XlCl + XlCa
        {
            if(fluidState_.moleFraction(wPhaseIdx, compIdx)>0)
            {
                ParentType::salinity_ += fluidState_.massFraction(wPhaseIdx, compIdx);
                ParentType::moleFractionSalinity_ += fluidState_.moleFraction(wPhaseIdx, compIdx);
            }
        }
//        ParentType::moleFractionSalinity_ = Chemistry::salinityToMolFrac_(ParentType::salinity_);

        completeFluidState(priVars, problem, element, fvGeometry, scvIdx, this->fluidState_, isOldSol);

        /////////////
        // calculate the remaining quantities
        /////////////


        // porosity and permeability evaluation
        ParentType::initialPorosity_ = problem.spatialParams().porosity(element,
                                                            fvGeometry,
                                                            scvIdx); //1 - InitialSolidity;
        ParentType::porosity_= ParentType::initialPorosity_;
        Scalar critPoro = problem.spatialParams().critPorosity(element,
                    fvGeometry,
                    scvIdx);
        for(int sPhaseIdx = 0; sPhaseIdx < numSPhases; ++sPhaseIdx)
        {
            ParentType::precipitateVolumeFraction_[sPhaseIdx] = priVars[numComponents + sPhaseIdx];
            ParentType::porosity_-= ParentType::precipitateVolumeFraction_[sPhaseIdx];
        }

         Scalar porosity = ParentType::porosity_;
         Scalar initialPorosity = ParentType::initialPorosity_;

        // Kozeny-Carman relation
         Scalar KozenyCarmanExponent = 3;
         if(porosity>critPoro)
             ParentType::permeabilityFactor_ = std::pow(((porosity - critPoro)/(initialPorosity - critPoro)), KozenyCarmanExponent);
         else
             ParentType::permeabilityFactor_ = 0.0;


         Scalar mNa = Chemistry::moleFracToMolality(fluidState_.moleFraction(wPhaseIdx,NaIdx), ParentType::moleFractionSalinity_, fluidState_.moleFraction(wPhaseIdx,nCompIdx));  //[mol_sodium/kg_H2O]
         if (mNa < 0)
              mNa = 0;
         Scalar mCl = Chemistry::moleFracToMolality(fluidState_.moleFraction(wPhaseIdx,ClIdx), ParentType::moleFractionSalinity_, fluidState_.moleFraction(wPhaseIdx,nCompIdx));  //[mol_chloride/kg_H2O]
         if (mCl < 0)
              mCl = 0;
         Scalar mNH4 = Chemistry::moleFracToMolality(fluidState_.moleFraction(wPhaseIdx,NH4Idx), ParentType::moleFractionSalinity_, fluidState_.moleFraction(wPhaseIdx,nCompIdx));  //[mol_NH4/kg_H2O]
         if (mNH4 < 0)
              mNH4 = 0;
         Scalar mCa = Chemistry::moleFracToMolality(fluidState_.moleFraction(wPhaseIdx,CaIdx), ParentType::moleFractionSalinity_, fluidState_.moleFraction(wPhaseIdx,nCompIdx));  //[mol_calcium/kg_H2O]
         if (mCa < 0)
              mCa = 0;
         Scalar mCO3 = Chemistry::moleFracToMolality(fluidState_.moleFraction(wPhaseIdx,CO3Idx), ParentType::moleFractionSalinity_, fluidState_.moleFraction(wPhaseIdx,nCompIdx));  //[mol_CO3/kg_H2O]
         if (mCO3 < 0)
              mCO3 = 0;
         Scalar mHCO3 = Chemistry::moleFracToMolality(fluidState_.moleFraction(wPhaseIdx,HCO3Idx), ParentType::moleFractionSalinity_, fluidState_.moleFraction(wPhaseIdx,nCompIdx));  //[mol_HCO3/kg_H2O]
         if (mHCO3 < 0)
              mHCO3 = 0;

          Chemistry chemistry;

         Omega_ = chemistry.Omega(mNa, mCa, mNH4, mHCO3, mCO3, mCl, fluidState_.temperature());
         OmegaApprox_ = chemistry.OmegaApprox(mCa, mCO3);


        // energy related quantities not contained in the fluid state
        asImp_().updateEnergy_(priVars, problem,element, fvGeometry, scvIdx, isOldSol);


    }

   /*!
    * \copydoc ImplicitModel::completeFluidState
    * \param isOldSol Specifies whether this is the previous solution or the current one
    */
    static void completeFluidState(const PrimaryVariables& priVars,
                                   const Problem& problem,
                                   const Element& element,
                                   const FVElementGeometry& fvGeometry,
                                   int scvIdx,
                                   FluidState& fluidState,
                                   bool isOldSol = false)

    {
        Scalar t = Implementation::temperature_(priVars, problem, element,
                                                fvGeometry, scvIdx);
        fluidState.setTemperature(t);

      int globalVertIdx = problem.model().dofMapper().subIndex(element, scvIdx, dim);
        int phasePresence = problem.model().phasePresence(globalVertIdx, isOldSol);

    /////////////
        // set the saturations
        /////////////

    Scalar Sn;
        if (phasePresence == nPhaseOnly)
            Sn = 1.0;
        else if (phasePresence == wPhaseOnly) {
            Sn = 0.0;
        }
        else if (phasePresence == bothPhases) {
            if (formulation == pwsn)
                Sn = priVars[switchIdx];
            else if (formulation == pnsw)
                Sn = 1.0 - priVars[switchIdx];
            else DUNE_THROW(Dune::InvalidStateException, "Formulation: " << formulation << " is invalid.");
        }
    else DUNE_THROW(Dune::InvalidStateException, "phasePresence: " << phasePresence << " is invalid.");
        fluidState.setSaturation(nPhaseIdx, Sn);
        fluidState.setSaturation(wPhaseIdx, 1.0 - Sn);

            /////////////
        // set the pressures of the fluid phases
        /////////////

        // calculate capillary pressure
        const MaterialLawParams &materialParams
        = problem.spatialParams().materialLawParams(element, fvGeometry, scvIdx);
        Scalar pC = MaterialLaw::pc(materialParams, 1 - Sn);

        // extract the pressures
        if (formulation == pwsn) {
            fluidState.setPressure(wPhaseIdx, priVars[pressureIdx]);
            if (priVars[pressureIdx] + pC < 0.0)
                            DUNE_THROW(Dumux::NumericalProblem,"Capillary pressure is too low! \n wPhasePressure = "<<priVars[pressureIdx]
                              <<"\n pC = " <<pC <<", Sn = " <<Sn <<", Sw = " << 1-Sn
                              <<"\n priVars = " <<priVars);
            fluidState.setPressure(nPhaseIdx, priVars[pressureIdx] + pC);
        }
        else if (formulation == pnsw) {
            fluidState.setPressure(nPhaseIdx, priVars[pressureIdx]);
// Here we check for (p_n - pc) in order to ensure that (p_l > 0)
            if (priVars[pressureIdx] - pC < 0.0)
            {
                std::cout<< "p_n: "<< priVars[pressureIdx]<<" Cap_press: "<< pC << std::endl;
                DUNE_THROW(Dumux::NumericalProblem,"Capillary pressure is too high");
            }
//          std::cout<< "p_n: "<< priVars[pressureIdx]<<" Cap_press: "<< pC << std::endl;
            fluidState.setPressure(wPhaseIdx, priVars[pressureIdx] - pC);
        }
        else DUNE_THROW(Dune::InvalidStateException, "Formulation: " << formulation << " is invalid.");

        /////////////
        // calculate the phase compositions
        /////////////

        // set the known mole fractions in the fluidState so that they
        // can be used by the Miscible2pNCComposition constraint solver
        // and can be used to compute the fugacity coefficients
        for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
        {
            fluidState.setMoleFraction(wPhaseIdx, compIdx, priVars[compIdx]);
        }

    typename FluidSystem::ParameterCache paramCache;

        // now comes the tricky part: calculate phase composition
        if (phasePresence == bothPhases) {
            // both phases are present, phase composition results from
            // the gas <-> liquid equilibrium. This is
            // the job of the "MiscibleMultiPhaseComposition"
            // constraint solver

            Miscible2pNCComposition::solve(fluidState,
                                           paramCache,
                                           wPhaseIdx,   //known phaseIdx
                                           /*setViscosity=*/true,
                                           /*setEnthalpy=**/false);
        }
        else if (phasePresence == nPhaseOnly){

            Dune::FieldVector<Scalar, numComponents + numSecComponents> moleFrac;
            Dune::FieldVector<Scalar, numComponents> fugCoeffW;
            Dune::FieldVector<Scalar, numComponents> fugCoeffN;

            for (int compIdx=0; compIdx<numComponents; ++compIdx)
            {
                fugCoeffW[compIdx] = FluidSystem::fugacityCoefficient(fluidState,
                                                                paramCache,
                                                                wPhaseIdx,
                                                                compIdx);
                fugCoeffN[compIdx] = FluidSystem::fugacityCoefficient(fluidState,
                                                                paramCache,
                                                                nPhaseIdx,
                                                                compIdx);
            }
            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            {
                moleFrac[compIdx] = priVars[compIdx]*fugCoeffW[compIdx]*fluidState.pressure(wPhaseIdx)
                                        /(fugCoeffN[compIdx]*fluidState.pressure(nPhaseIdx));
            }
            moleFrac[wCompIdx] =  priVars[switchIdx];
            Scalar sumMoleFracOtherComponent = 0;
            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            {
                    sumMoleFracOtherComponent+=moleFrac[compIdx];
            }
            sumMoleFracOtherComponent += moleFrac[wCompIdx];
            moleFrac[nCompIdx] = 1 - sumMoleFracOtherComponent;

//            for (int compIdx=numComponents; compIdx<numSecComponents; ++compIdx)
//            {
//              moleFrac[compIdx] = 0;          //no secondary component in the gas phase!!
//            }

            // convert mass to mole fractions and set the fluid state
            for (int compIdx=0; compIdx<numComponents; ++compIdx)
            {
                fluidState.setMoleFraction(nPhaseIdx, compIdx, moleFrac[compIdx]);
            }
            for (int compIdx=numComponents; compIdx<numComponents + numSecComponents; ++compIdx)
            {
                fluidState.setMoleFractionSecComp(nPhaseIdx, compIdx, 0);
                fluidState.setMoleFractionSecComp(wPhaseIdx, compIdx, 0);
                if (compIdx == CO2Idx)
                    fluidState.setMoleFractionSecComp(nPhaseIdx, compIdx, fluidState.moleFraction(nPhaseIdx, nCompIdx));
            }
            // calculate the composition of the remaining phases (as
            // well as the densities of all phases). this is the job
            // of the "ComputeFromReferencePhase" constraint solver
             ComputeFromReferencePhase::solve(fluidState,
                                             paramCache,
                                             wPhaseIdx,
                                             /*setViscosity=*/true,
                                             /*setEnthalpy=**/false);

         }
        else if (phasePresence == wPhaseOnly){
        // only the liquid phase is present, i.e. liquid phase
        // composition is stored explicitly.
        // extract _mass_ fractions in the gas phase
            Dune::FieldVector<Scalar, numComponents + numSecComponents> moleFrac;

            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            {
                moleFrac[compIdx] = priVars[compIdx];
            }
            moleFrac[nCompIdx] = priVars[switchIdx];
            Scalar sumMoleFracOtherComponent = 0;
            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            {
                    sumMoleFracOtherComponent+=moleFrac[compIdx];
            }
            sumMoleFracOtherComponent += moleFrac[nCompIdx];
            moleFrac[wCompIdx] = 1 - sumMoleFracOtherComponent;

            for (int compIdx=numComponents; compIdx<numComponents + numSecComponents; ++compIdx)
            {
                moleFrac[compIdx] = 0;
            }
            Scalar XwSalinity= 0.0;
            for (int compIdx = NaIdx; compIdx<= CaIdx ; compIdx++)  //salinity = XlNa + XlCl + XlCa
            {
                if(fluidState.massFraction(wPhaseIdx, compIdx)>0)
                {
                    XwSalinity+= fluidState.massFraction(wPhaseIdx, compIdx);
                }
            }
                        Scalar xnH2O;
                        Scalar xwCO2;

             Brine_CO2::calculateMoleFractions(fluidState.temperature(),
                                        fluidState.pressure(nPhaseIdx),
                                        XwSalinity,
                                        /*knownPhaseIdx=*/-1,
                                        xwCO2,
                                        xnH2O);
                // normalize the phase compositions
                xwCO2 = std::max(0.0, std::min(1.0, xwCO2));
                xnH2O = std::max(0.0, std::min(1.0, xnH2O));

                for (int compIdx=0; compIdx<numComponents+numSecComponents; ++compIdx)
                {
                    fluidState.setMoleFraction(wPhaseIdx, compIdx, moleFrac[compIdx]);
                    fluidState.setMoleFraction(nPhaseIdx, compIdx, 0);
                }
                //only CO2 and water are present in the non-wetting Phase
                fluidState.setMoleFraction(nPhaseIdx, wCompIdx, xnH2O);
                fluidState.setMoleFraction(nPhaseIdx, nCompIdx, 1-xnH2O);

        }
        paramCache.updateAll(fluidState);
        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
        {
            // compute and set the viscosity
            Scalar mu = FluidSystem::viscosity(fluidState, paramCache, phaseIdx);
            fluidState.setViscosity(phaseIdx, mu);

            // compute and set the density
            Scalar rho = FluidSystem::density(fluidState, paramCache, phaseIdx);
            fluidState.setDensity(phaseIdx, rho);

            // compute and set the enthalpy
            Scalar h = Implementation::enthalpy_(fluidState, paramCache, phaseIdx);
            fluidState.setEnthalpy(phaseIdx, h);
        }

        if (phasePresence == bothPhases){
            Scalar XwSalinity= 0.0;
            Scalar xwSalinity= 0.0;
        for (int compIdx = NaIdx; compIdx<= CaIdx ; compIdx++)  //salinity = XlNa + XlCl + XlCa
        {
            if(fluidState.massFraction(wPhaseIdx, compIdx)>0)
            {
                XwSalinity+= fluidState.massFraction(wPhaseIdx, compIdx);
                xwSalinity+= fluidState.moleFraction(wPhaseIdx, compIdx);
            }
        }
//        Scalar xwSalinity = Chemistry::salinityToMolFrac_(XwSalinity);

                        Scalar xnH2O;
                        Scalar xwCO2;

             Brine_CO2::calculateMoleFractions(fluidState.temperature(),
                                        fluidState.pressure(nPhaseIdx),
                                        XwSalinity,
                                        /*knownPhaseIdx=*/-1,
                                        xwCO2,
                                        xnH2O);
                // normalize the phase compositions
                xwCO2 = std::max(0.0, std::min(1.0, xwCO2));
                xnH2O = std::max(0.0, std::min(1.0, xnH2O));
             Scalar constant_C  = xwCO2;

             Dune::FieldVector<Scalar, numComponents + numSecComponents> moleFrac;

             for(int compIdx=0; compIdx<numComponents; ++compIdx)
                {
                    moleFrac [compIdx] = fluidState.moleFraction(wPhaseIdx, compIdx);
                    if (moleFrac [compIdx] < 0)
                        moleFrac [compIdx] = 0;
                }

             Chemistry chemistry;
             chemistry.calculateEquilibriumChemistry(fluidState, phasePresence, xwSalinity, constant_C, moleFrac);

//           fluidState.setMoleFraction(wPhaseIdx, nCompIdx, moleFrac[nCompIdx]); // xlCTOT = xlco2+xlco3+xlhco3, like computed in the chemistry
             for (int compIdx=numComponents; compIdx<numComponents + numSecComponents; ++compIdx)
                {
                    fluidState.setMoleFractionSecComp(wPhaseIdx, compIdx, moleFrac[compIdx]);
                    fluidState.setMoleFractionSecComp(nPhaseIdx, compIdx, 0);
                    if (compIdx== CO2Idx)
                        fluidState.setMoleFractionSecComp(nPhaseIdx, CO2Idx, fluidState.moleFraction(nPhaseIdx, nCompIdx));
                }

        }
        else if (phasePresence == wPhaseOnly){
            Dune::FieldVector<Scalar, numComponents + numSecComponents> moleFrac;
            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            {
                moleFrac[compIdx] = priVars[compIdx];
                if (moleFrac [compIdx] < 0)
                    moleFrac [compIdx] = 0;
            }
            moleFrac[nCompIdx] = priVars[switchIdx];
            if (moleFrac[nCompIdx] < 0)
                moleFrac[nCompIdx] = 1e-40;

            Scalar sumMoleFracOtherComponent = 0;
            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            {
                sumMoleFracOtherComponent+=moleFrac[compIdx];
            }
            sumMoleFracOtherComponent += moleFrac[nCompIdx];
            moleFrac[wCompIdx] = 1 - sumMoleFracOtherComponent;
            for (int compIdx=numComponents; compIdx<numComponents + numSecComponents; ++compIdx)
            {
                moleFrac[compIdx] = 0;
            }

            Scalar XwSalinity= 0.0;
            Scalar xwSalinity= 0.0;
        for (int compIdx = NaIdx; compIdx<= CaIdx ; compIdx++)  //salinity = XlNa + XlCl + XlCa
        {
            if(fluidState.massFraction(wPhaseIdx, compIdx)>0)
            {
                XwSalinity+= fluidState.massFraction(wPhaseIdx, compIdx);
                xwSalinity+= fluidState.moleFraction(wPhaseIdx, compIdx);
            }
        }
//        Scalar xwSalinity = Chemistry::salinityToMolFrac_(XwSalinity);
            Scalar constant_C = moleFrac[nCompIdx];
            if (constant_C < 0 || constant_C>1)
                 std::cout<< "constant_C = moleFrac[nCompIdx] (wPhaseonly) =   "<<  constant_C <<std::endl;

            Chemistry chemistry;
            chemistry.calculateEquilibriumChemistry(fluidState, phasePresence, xwSalinity, constant_C, moleFrac);

            for (int compIdx=numComponents; compIdx<numComponents + numSecComponents; ++compIdx)
            {
//                std::cout<< "moleFrac["<<FluidSystem::componentName(compIdx)<<"] (wPhase) =  "<< moleFrac[compIdx]  <<std::endl;
                fluidState.setMoleFractionSecComp(wPhaseIdx, compIdx, moleFrac[compIdx]);
                fluidState.setMoleFractionSecComp(nPhaseIdx, compIdx, 0);
                if (compIdx == CO2Idx)
                    fluidState.setMoleFractionSecComp(nPhaseIdx, CO2Idx, fluidState.moleFraction(nPhaseIdx, nCompIdx));
            }

        }
        paramCache.updateAll(fluidState);
        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
        {
            // compute and set the viscosity
            Scalar mu = FluidSystem::viscosity(fluidState, paramCache, phaseIdx);
            fluidState.setViscosity(phaseIdx, mu);

            // compute and set the density
            Scalar rho = FluidSystem::density(fluidState, paramCache, phaseIdx);
            fluidState.setDensity(phaseIdx, rho);

            // compute and set the enthalpy
            Scalar h = Implementation::enthalpy_(fluidState, paramCache, phaseIdx);
            fluidState.setEnthalpy(phaseIdx, h);
        }
    }
//    /*!
//     * \brief Returns the volume fraction of the precipitate (solid phase)
//     * for the given phaseIdx
//     *
//     * \param phaseIdx the index of the solid phase
//     */
//    Scalar precipitateVolumeFraction(int phaseIdx) const
//    { return precipitateVolumeFraction_[phaseIdx - numPhases]; }
//
//    /*!
//     * \brief Returns the inital porosity of the
//     * pure, precipitate-free porous medium
//     */
//    Scalar initialPorosity() const
//    { return initialPorosity_;}
//
//    /*!
//     * \brief Returns the inital permeability of the
//     * pure, precipitate-free porous medium
//     */
//    Scalar initialPermeability() const
//    { return initialPermeability_;}
//
//    /*!
//     * \brief Returns the factor for the reduction of the initial permeability
//     * due precipitates in the porous medium
//     */
//    Scalar permeabilityFactor() const
//    { return permeabilityFactor_; }
//
//    /*!
//     * \brief Returns the mole fraction of the salinity in the liquid phase
//     */
//    Scalar moleFracSalinity() const
//    {
//        return ParentType::moleFractionSalinity_;
//    }
//
//    /*!
//     * \brief Returns the salinity (mass fraction) in the liquid phase
//     */
//    Scalar salinity() const
//    {
//        return ParentType::salinity_ ;
//    }
//
//    /*!
//     * \brief Returns the density of the phase for all fluid and solid phases
//     *
//     * \param phaseIdx the index of the fluid phase
//     */
//    Scalar density(int phaseIdx) const
//    {
//        if (phaseIdx < numPhases)
//            return this->fluidState_.density(phaseIdx);
//        else if (phaseIdx >= numPhases)
//            return FluidSystem::precipitateDensity(phaseIdx);
//        else
//            DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
//    }
//    /*!
//     * \brief Returns the mass density of a given phase within the
//     *        control volume.
//     *
//     * \param phaseIdx The phase index
//     */
//    Scalar molarDensity(int phaseIdx) const
//    {
//        if (phaseIdx < numPhases)
//            return this->fluidState_.molarDensity(phaseIdx);
//        else if (phaseIdx >= numPhases)
//            return FluidSystem::precipitateMolarDensity(phaseIdx);
//        else
//            DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
//    }
//
//    /*!
//     * \brief Returns the molality of a component in the phase
//     *
//     * \param phaseIdx the index of the fluid phase
//     * \param compIdx the index of the component
//     * \f$\mathrm{molality}=\frac{n_\mathrm{component}}{m_\mathrm{solvent}}
//     * =\frac{n_\mathrm{component}}{n_\mathrm{solvent}*M_\mathrm{solvent}}\f$
//     * compIdx of the main component (solvent) in the
//     * phase is equal to the phaseIdx
//     */
//    Scalar molality(int phaseIdx, int compIdx) const // [moles/Kg]
//    { return this->fluidState_.moleFraction(phaseIdx, compIdx)
//                  /(fluidState_.moleFraction(phaseIdx, phaseIdx)
//                  * FluidSystem::molarMass(phaseIdx));}
    /*!
     *  copydoc 2pncMin::moleFraction
     */
   Scalar moleFraction(int phaseIdx, int compIdx) const
   { return fluidState_.moleFraction(phaseIdx, compIdx); }

    /*!
     *  copydoc 2pncMin::massFraction
     */
   Scalar massFraction(int phaseIdx, int compIdx) const
   { return fluidState_.massFraction(phaseIdx, compIdx); }

   /*!
    * \brief Returns the phase state for the control-volume.
    */
   const FluidState &fluidState() const
   { return fluidState_; }

   /*!
    * \brief Returns the calcite saturation state Omega for the control-volume.
    */
   Scalar Omega() const
   { return Omega_; }
   /*!
    * \brief Returns the approximated (no activity coefficients considered)
    *  calcite saturation state Omega for the control-volume.
    */
   Scalar OmegaApprox() const
   { return OmegaApprox_; }

//protected:
//    friend class TwoPNCMinVolumeVariables<TypeTag>;
//    friend class TwoPNCVolumeVariables<TypeTag>;
//    static Scalar temperature_(const PrimaryVariables &priVars,
//                                const Problem& problem,
//                                const Element &element,
//                                const FVElementGeometry &fvGeometry,
//                                int scvIdx)
//    {
//        return problem.temperatureAtPos(fvGeometry.subContVol[scvIdx].global);
//    }
//
//    template<class ParameterCache>
//    static Scalar enthalpy_(const FluidState& fluidState,
//                            const ParameterCache& paramCache,
//                            int phaseIdx)
//    {
//        return 0;
//    }
//
//   /*!
//    * \brief Update all quantities for a given control volume.
//    *
//    * \param priVars The solution primary variables
//    * \param problem The problem
//    * \param element The element
//    * \param fvGeometry Evaluate function with solution of current or previous time step
//    * \param scvIdx The local index of the SCV (sub-control volume)
//    * \param isOldSol Evaluate function with solution of current or previous time step
//    */
//    void updateEnergy_(const PrimaryVariables &priVars,
//                       const Problem &problem,
//                       const Element &element,
//                       const FVElementGeometry &fvGeometry,
//                       const int scvIdx,
//                       bool isOldSol)
//    {};

//    Scalar precipitateVolumeFraction_[numSPhases];
//    Scalar permeabilityFactor_;
//    Scalar initialPorosity_;
//    Scalar initialPermeability_;
//    Scalar minimumPorosity_;
//    Scalar salinity_;
//    Scalar moleFractionSalinity_;
    FluidState fluidState_;
    Scalar Omega_=0.;
    Scalar OmegaApprox_=0.;

private:
    Implementation &asImp_()
    { return *static_cast<Implementation*>(this); }

    const Implementation &asImp_() const
    { return *static_cast<const Implementation*>(this); }
};

} // end namespace

#endif
