// $Id: 2p2cmodel.hh 5093 2011-01-23 18:05:49Z claude $
/*****************************************************************************
 *   Copyright (C) 2008 by Klaus Mosthaf, Andreas Lauser, Bernd Flemisch     *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

/*!
* \file
*
* \brief daption of the fully implicit scheme to the two-phase biomineralisation flow model.
*/

#ifndef DUMUX_2PBIOMIN_MODEL_HH
#define DUMUX_2PBIOMIN_MODEL_HH
#define VELOCITY_OUTPUT

#include "properties.hh"
#include "2pbiominlocalresidual.hh"
#include "dumux/porousmediumflow/2pncmin/implicit/localresidual.hh"
#include "propertydefaults.hh"
#include "dumux/porousmediumflow/2pncmin/implicit/model.hh"

#include <boost/format.hpp>
#include <cmath>

namespace Dumux
{
/*!
 * \ingroup TwoPNCMinModelModel
 * \defgroup TwoPBioMinModel Two-phase biomineralisation model
 */

/*!
 * \ingroup TwoPBioMinModel
 * \brief daption of the fully implicit scheme to the two-phase biomineralisation flow model.
 *
 * This model implements two-phase biomineralisation flow of two compressible and
 * partially miscible fluids \f$\alpha \in \{ w, n \}\f$ composed of the n components
 * \f$\kappa \in \{ w, C_{total}, ... \}\f$. The standard multiphase Darcy
 * approach is used as the equation for the conservation of momentum:
 * \f[
 v_\alpha = - \frac{k_{r\alpha}}{\mu_\alpha} \mbox{\bf K}
 \left(\text{grad}\, p_\alpha - \varrho_{\alpha} \mbox{\bf g} \right)
 * \f]
 *
 * By inserting this into the equations for the conservation of the
 * components, one gets one transport equation for each component
 * \f{eqnarray}
 && \phi \frac{\partial (\sum_\alpha \varrho_\alpha X_\alpha^\kappa S_\alpha )}
 {\partial t}
 - \sum_\alpha  \text{div} \left\{ \varrho_\alpha X_\alpha^\kappa
 \frac{k_{r\alpha}}{\mu_\alpha} \mbox{\bf K}
 (\text{grad}\, p_\alpha - \varrho_{\alpha}  \mbox{\bf g}) \right\}
 \nonumber \\ \nonumber \\
    &-& \sum_\alpha \text{div} \left\{{\bf D_{\alpha, pm}^\kappa} \varrho_{\alpha} \text{grad}\, X^\kappa_{\alpha} \right\}
 - \sum_\alpha q_\alpha^\kappa = 0 \qquad \kappa \in \{ w, C_{total}, ... \} \, ,
 \alpha \in \{w, g\}
 \f}
 *
 * This is discretized using a fully-coupled vertex
 * centered finite volume (box) scheme as spatial and
 * the implicit Euler method as temporal discretization.
 *
 * By using constitutive relations for the capillary pressure \f$p_c =
 * p_n - p_w\f$ and relative permeability \f$k_{r\alpha}\f$ and taking
 * advantage of the fact that \f$S_w + S_n = 1\f$ and \f$X^\kappa_w + X^\kappa_n = 1\f$, the number of
 * unknowns can be reduced to two.
 * The used primary variables are, like in the two-phase model, either \f$p_w\f$ and \f$S_n\f$
 * or \f$p_n\f$ and \f$S_w\f$. The formulation which ought to be used can be
 * specified by setting the <tt>Formulation</tt> property to either
 * TwoPTwoCIndices::pWsN or TwoPTwoCIndices::pNsW. By
 * default, the model uses \f$p_w\f$ and \f$S_n\f$.
 * Moreover, the second primary variable depends on the phase state, since a
 * primary variable switch is included. The phase state is stored for all nodes
 * of the system. Following cases can be distinguished:
 * <ul>
 *  <li> Both phases are present: The saturation is used (either \f$S_n\f$ or \f$S_w\f$, dependent on the chosen <tt>Formulation</tt>),
 *      as long as \f$ 0 < S_\alpha < 1\f$</li>.
 *  <li> Only wetting phase is present: The mass fraction of, e.g., air in the wetting phase \f$X^a_w\f$ is used,
 *      as long as the maximum mass fraction is not exceeded (\f$X^a_w<X^a_{w,max}\f$)</li>
 *  <li> Only non-wetting phase is present: The mass fraction of, e.g., water in the non-wetting phase, \f$X^w_n\f$, is used,
 *      as long as the maximum mass fraction is not exceeded (\f$X^w_n<X^w_{n,max}\f$)</li>
 * </ul>
 */

template<class TypeTag>
class TwoPBioMinModel: public TwoPNCMinModel<TypeTag>
{
    typedef TwoPBioMinModel<TypeTag> ThisType;
    typedef TwoPNCMinModel<TypeTag> ParentType;
    typedef ImplicitModel<TypeTag> BaseClassType;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementBoundaryTypes) ElementBoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, VertexMapper) VertexMapper;
    typedef typename GET_PROP_TYPE(TypeTag, ElementMapper) ElementMapper;
    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    enum {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,

        numEq = GET_PROP_VALUE(TypeTag, NumEq),
        numPhases = GET_PROP_VALUE(TypeTag, NumPhases),
        numSPhases = GET_PROP_VALUE(TypeTag, NumSPhases),
        numComponents = GET_PROP_VALUE(TypeTag, NumComponents),
        numSecComponents = GET_PROP_VALUE(TypeTag, NumSecComponents),

        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,

        wPhaseIdx = Indices::wPhaseIdx,
        nPhaseIdx = Indices::nPhaseIdx,

        wCompIdx = FluidSystem::wCompIdx,
        nCompIdx = FluidSystem::nCompIdx,
        UreaIdx = FluidSystem::UreaIdx,
        CaIdx = FluidSystem::CaIdx,

        wPhaseOnly = Indices::wPhaseOnly,
        nPhaseOnly = Indices::nPhaseOnly,
        bothPhases = Indices::bothPhases,

        pwsn = TwoPNCFormulation::pwsn,
        pnsw = TwoPNCFormulation::pnsw,
        formulation = GET_PROP_VALUE(TypeTag, Formulation)
    };

    typedef CompositionalSecCompFluidState<Scalar, FluidSystem> FluidState;

    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::template Codim<dim>::Iterator VertexIterator;

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef typename GridView::ctype CoordScalar;
    typedef Dune::FieldMatrix<CoordScalar, dimWorld, dimWorld> Tensor;
    typedef Dune::FieldVector<Scalar, numPhases> PhasesVector;
    typedef Dune::FieldVector<Scalar, dim> Vector;

//    static constexpr Scalar ImplicitMobilityUpwindWeight =
//            GET_PROP_VALUE(TypeTag, ImplicitMobilityUpwindWeight);

    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
        enum { dofCodim = isBox ? dim : 0 };

public:
    /*!
     * \copydoc 2pncMin::init
     */
    void init(Problem &problem)
    {
        ParentType::init(problem);
        try
    {
            plausibilityTolerance_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Model, PlausibilityTolerance);
            pHMax_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Model, pHMax);
            pHMin_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Model, pHMin);
            // retrieve the upwind weight for the mass conservation equations. Use the value
            // specified via the property system as default, and overwrite
            // it by the run-time parameter from the Dune::ParameterTree
            massUpwindWeight_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Implicit, MassUpwindWeight);
            useUCurExtrapolation_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Model, uCurExtrapolation);
    }

    catch (Dumux::ParameterException &e) {
        std::cerr << e << ". Abort!\n";
        exit(1) ;
    }
        unsigned numDofs = this->numDofs();

        staticDat_.resize(numDofs);
        velocity_.resize(numDofs);
        absgradpw_.resize(numDofs);
        uPrevPrev_.resize(numDofs);
        uPrevPrev_ = this->uPrev_;

        for(int i=0;i<numDofs;i++)
        {
            absgradpw_[i] = 0.0;
        }

        setSwitched_(false);

        if (isBox) // i.e. vertex-centered discretization
        {
            for (const auto& element : elements(this->gridView_()))
            {
                FVElementGeometry fvGeometry;
                fvGeometry.update(this->gridView_(), element);
                for (unsigned int scvIdx = 0; scvIdx < fvGeometry.numScv; ++scvIdx)
                {
                    auto dofIdxGlobal = this->dofMapper().subIndex(element, scvIdx, dofCodim);
                    staticDat_[dofIdxGlobal].phasePresence = this->problem_().initialPhasePresence(element, fvGeometry, scvIdx);
                    staticDat_[dofIdxGlobal].wasSwitched = false;
                    staticDat_[dofIdxGlobal].oldPhasePresence = staticDat_[dofIdxGlobal].phasePresence;

                    velocity_[dofIdxGlobal] = 0.0;
                    absgradpw_[dofIdxGlobal] = 0.0;
                }
            }
        }
    }

    /*!
     * \copydoc 2pncMin::globalPhaseStorage
     */
    void globalPhaseStorage(PrimaryVariables &dest, int phaseIdx)
    {
        dest = 0;

        ElementIterator element = this->gridView_().template begin<0>();
        const ElementIterator elemEndIt = this->gridView_().template end<0>();
        for (; element != elemEndIt; ++element)
        {
            this->localResidual().evalPhaseStorage(*element, phaseIdx);
            for (int i = 0; i < element->template count<dim>(); ++i)
                dest += this->localResidual().residual(i);
        };

        this->gridView_().comm().sum(dest);
    }

//    /*!
//     * \copydoc Implicit::checkPlausibility
//     */
//    void checkPlausibility() const
//    {
//       // Looping over all elements of the domain
//              ElementIterator eEndIt = this->problem_().gridView().template end<0>();
//              for (ElementIterator eIt = this->problem_().gridView().template begin<0>() ; eIt not_eq eEndIt; ++eIt)
//              {
//                  ElementVolumeVariables elemVolVars;
//                  FVElementGeometry fvGeometry;
//
//                  // updating the volume variables
//                  fvGeometry.update(this->problem_().gridView(), *eIt);
//                  elemVolVars.update(this->problem_(), *eIt, fvGeometry, false);
//
//                  std::stringstream  message ;
//                  // number of scv
//                  const unsigned int numScv = fvGeometry.numScv; // box: numSCV, cc:1
//
//                  for (unsigned int scvIdx = 0; scvIdx < numScv; ++scvIdx) {
//
//                      const FluidState & fluidState = elemVolVars[scvIdx].fluidState();
//
//                      // mass Check
//                      const Scalar eps = plausibilityTolerance_ ;
//                      for (int compIdx=0; compIdx< numComponents+numSecComponents; ++ compIdx){
//                          const Scalar xTest = elemVolVars[scvIdx].moleFraction(wPhaseIdx, compIdx);
//
//                          if (not std::isfinite(xTest) or xTest < 0.-eps or xTest > 1.+eps ){
//                              message <<"\nUnphysical Value in Mass: \n";
//
//                              message << "\tx" <<"_w"
//                                      <<"^"<<FluidSystem::componentName(compIdx)<<"="
//                                      << elemVolVars[scvIdx].moleFraction(wPhaseIdx, compIdx) <<"\n";
//                          }
//                      }
//
//                      // check chemistry by pH
//                      Scalar pH = elemVolVars[scvIdx].pH();
//                      if (not std::isfinite(pH) or pH < pHMin_ or pH > pHMax_ ){
//                                                      message <<"\nUnphysical Value in pH: \n";
//
//                                                      message << "pH = " << elemVolVars[scvIdx].pH() <<"\n";
//                      }
//
//                      // Some check wrote into the error-message, add some additional information and throw
//                      if (not message.str().empty()){
//                          // Getting the spatial coordinate
//                          const GlobalPosition & globalPosCurrent = fvGeometry.subContVol[scvIdx].global;
//                          std::stringstream positionString ;
//
//                          // Add physical location
//                          positionString << "Here:";
//                          for(int i=0; i<dim; i++)
//                              positionString << " x"<< (i+1) << "="  << globalPosCurrent[i] << " "   ;
//                          message << "Unphysical value found! \n" ;
//                          message << positionString.str() ;
//                          message << "\n";
//
//                          message << " Here come the primary Variables:" << "\n" ;
//                          for(unsigned int priVarIdx =0 ; priVarIdx<numEq; ++priVarIdx){
//                              message << "priVar[" << priVarIdx << "]=" << elemVolVars[scvIdx].priVar(priVarIdx) << "\n";
//                          }
//                          DUNE_THROW(NumericalProblem, message.str());
//                      }
//                  } // end scv-loop
//              } // end element loop
//
//
//    }

    /*!
     * \copydoc 2pncMin::switched
     */
    bool switched() const
    {
        return switchFlag_;
    }

    /*!
     * \copydoc 2pncMin::phasePresence
     */
    int phasePresence(int dofIdxGlobal, bool oldSol) const
    {
        return oldSol ? staticDat_[dofIdxGlobal].oldPhasePresence
                : staticDat_[dofIdxGlobal].phasePresence;
    }

//    /*!
//     * \returns the velocity to be used in the VolumeVariables to calculate the detachment rate for the output
//     */
//    Scalar velocity(int dofIdxGlobal) const
//    {
//        Scalar v = 0;
//        for (int i=0;i<dim;i++)
//        {
//            v += velocity_[dofIdxGlobal][i] * velocity_[dofIdxGlobal][i];
//        }
//        v = sqrt(v);
//        return v;
//    }

    Scalar absVector(Dune::FieldVector<Scalar, dim> vec)
    {
        Scalar abs = 0;
        for (int i=0;i<dim;i++)
        {
            abs += vec[i] * vec[i];
        }
        return sqrt(abs);
    }

    /*!
     * \returns the absolute value of the water phase pressure gradient
     * to be used in the VolumeVariables to calculate the detachment rate for the output
     */
    Scalar absgradpw(int dofIdxGlobal) const
    {
//        return 1e4;
        return absgradpw_[dofIdxGlobal];
    }

    /*!
     * \copydoc 2pncMin::addOutputVtkFields
     */
    template<class MultiWriter>
    //additional output of the permeability and the precipitate volume fractions
    void addOutputVtkFields(const SolutionVector &sol, MultiWriter &writer)
    {
        typedef Dune::BlockVector<Dune::FieldVector<Scalar, 1> > ScalarField;
        typedef Dune::BlockVector<Dune::FieldVector<double, dim> > VectorField;

        // get the number of degrees of freedom
        auto numDofs = this->numDofs();

        // create the required scalar fields
        ScalarField *Sw           = writer.allocateManagedBuffer(numDofs);
        ScalarField *Sn           = writer.allocateManagedBuffer(numDofs);
        ScalarField *pn           = writer.allocateManagedBuffer (numDofs);
        ScalarField *pw           = writer.allocateManagedBuffer (numDofs);
        ScalarField *pc           = writer.allocateManagedBuffer (numDofs);
        ScalarField *rhoW         = writer.allocateManagedBuffer (numDofs);
        ScalarField *rhoN         = writer.allocateManagedBuffer (numDofs);
        ScalarField *mobW         = writer.allocateManagedBuffer (numDofs);
        ScalarField *mobN         = writer.allocateManagedBuffer (numDofs);
        ScalarField *phasePresence = writer.allocateManagedBuffer (numDofs);
//        ScalarField *temperature  = writer.allocateManagedBuffer (numDofs);
        ScalarField *poro         = writer.allocateManagedBuffer (numDofs);
        ScalarField *permeability = writer.allocateManagedBuffer (numDofs);
//        ScalarField *Omega        = writer.allocateManagedBuffer (numDofs);
//        ScalarField *OmegaApprox  = writer.allocateManagedBuffer (numDofs);
        ScalarField *precipitateVolumeFraction[numSPhases];

        for (int i = 0; i < numSPhases; ++i)
            precipitateVolumeFraction[i] = writer.allocateManagedBuffer(numDofs);

        ScalarField *moleFraction[numPhases][numComponents];
//        ScalarField *moleFraction[numPhases][numComponents+numSecComponents];
        for (int i = 0; i < numPhases; ++i)
            for (int j = 0; j < numComponents; ++j)
                moleFraction[i][j] = writer.allocateManagedBuffer(numDofs);

        ScalarField *moleFractionSecComp[numSecComponents];
        for (int j = 0; j < numSecComponents; ++j)
            moleFractionSecComp[j] = writer.allocateManagedBuffer(numDofs);

        ScalarField *molarity[numComponents];
        for (int j = 0; j < numComponents ; ++j)
            molarity[j] = writer.allocateManagedBuffer(numDofs);


        VectorField *velocityN = writer.template allocateManagedBuffer<double, dim>(numDofs);
        VectorField *velocityW = writer.template allocateManagedBuffer<double, dim>(numDofs);
        ImplicitVelocityOutput<TypeTag> velocityOutput(this->problem_());

        if (velocityOutput.enableOutput()) // check if velocity output is demanded
        {
            // initialize velocity fields
            for (unsigned int i = 0; i < numDofs; ++i)
            {
                (*velocityN)[i] = Scalar(0);
                (*velocityW)[i] = Scalar(0);
            }
        }

        auto numElements = this->gridView_().size(0);
        ScalarField *rank = writer.allocateManagedBuffer(numElements);

        for (const auto& element : elements(this->gridView_()))
        {
            auto eIdxGlobal = this->problem_().elementMapper().index(element);
            (*rank)[eIdxGlobal] = this->gridView_().comm().rank();
            FVElementGeometry fvGeometry;
            fvGeometry.update(this->gridView_(), element);

            ElementVolumeVariables elemVolVars;
            elemVolVars.update(this->problem_(),
                               element,
                               fvGeometry,
                               false /* oldSol? */);

            for (int scvIdx = 0; scvIdx < fvGeometry.numScv; ++scvIdx)
            {
                auto dofIdxGlobal = this->dofMapper().subIndex(element, scvIdx, dofCodim);

                (*Sw)[dofIdxGlobal] = elemVolVars[scvIdx].saturation(wPhaseIdx);
                (*Sn)[dofIdxGlobal] = elemVolVars[scvIdx].saturation(nPhaseIdx);
                (*pn)[dofIdxGlobal] = elemVolVars[scvIdx].pressure(nPhaseIdx);
                (*pw)[dofIdxGlobal] = elemVolVars[scvIdx].pressure(wPhaseIdx);
                (*pc)[dofIdxGlobal] = elemVolVars[scvIdx].capillaryPressure();
                (*rhoW)[dofIdxGlobal] = elemVolVars[scvIdx].density(wPhaseIdx);
                (*rhoN)[dofIdxGlobal] = elemVolVars[scvIdx].density(nPhaseIdx);
                (*mobW)[dofIdxGlobal] = elemVolVars[scvIdx].mobility(wPhaseIdx);
                (*mobN)[dofIdxGlobal] = elemVolVars[scvIdx].mobility(nPhaseIdx);
                (*poro)[dofIdxGlobal] = elemVolVars[scvIdx].porosity();
//                Tensor K = this->perm_(this->problem_().spatialParams().intrinsicPermeability(element, fvGeometry, scvIdx));
                Tensor K = this->problem_().spatialParams().intrinsicPermeability(element, fvGeometry, scvIdx);
                (*permeability)[dofIdxGlobal] = K[1][1] * elemVolVars[scvIdx].permeabilityFactor();

                for (int sPhaseIdx = 0; sPhaseIdx < numSPhases; ++sPhaseIdx)
                    (*precipitateVolumeFraction[sPhaseIdx])[dofIdxGlobal] = elemVolVars[scvIdx].precipitateVolumeFraction(sPhaseIdx + numPhases);

//                (*temperature)[dofIdxGlobal] = elemVolVars[scvIdx].temperature();
                (*phasePresence)[dofIdxGlobal] = this->staticDat_[dofIdxGlobal].phasePresence;
//                (*Omega)[dofIdxGlobal] = elemVolVars[scvIdx].Omega();
//                (*OmegaApprox)[dofIdxGlobal] = elemVolVars[scvIdx].OmegaApprox();

                for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
                    for (int compIdx = 0; compIdx < numComponents; ++compIdx)
//                    for (int compIdx = 0; compIdx < numComponents+numSecComponents; ++compIdx)
                        (*moleFraction[phaseIdx][compIdx])[dofIdxGlobal]= elemVolVars[scvIdx].moleFraction(phaseIdx,compIdx);

                for (int compIdx = 0; compIdx < numSecComponents; ++compIdx)
                    (*moleFractionSecComp[compIdx])[dofIdxGlobal]= elemVolVars[scvIdx].moleFraction(wPhaseIdx,compIdx+numComponents);

                for (int compIdx = 0; compIdx < numComponents; ++compIdx)
                    (*molarity[compIdx])[dofIdxGlobal] = (elemVolVars[scvIdx].molarity(wPhaseIdx, compIdx));
            };

            // velocity output
            if(velocityOutput.enableOutput()){
                velocityOutput.calculateVelocity(*velocityW, elemVolVars, fvGeometry, element, wPhaseIdx);
                velocityOutput.calculateVelocity(*velocityN, elemVolVars, fvGeometry, element, nPhaseIdx);
            }
        }  // loop over element

        writer.attachDofData(*Sw, "Sw", isBox);
        writer.attachDofData(*Sn, "Sn", isBox);
        writer.attachDofData(*pn, "pn", isBox);
        writer.attachDofData(*pw, "pw", isBox);
        writer.attachDofData(*pc, "pc", isBox);
        writer.attachDofData(*rhoW, "rhoW", isBox);
        writer.attachDofData(*rhoN, "rhoN", isBox);
        writer.attachDofData(*mobW, "mobW", isBox);
        writer.attachDofData(*mobN, "mobN", isBox);
        writer.attachDofData(*poro, "porosity", isBox);
        writer.attachDofData(*permeability, "Kyy", isBox);
//        writer.attachDofData(*temperature, "temperature", isBox);
        writer.attachDofData(*phasePresence, "phase presence", isBox);
//        writer.attachDofData(*Omega, "Omega", isBox);
//        writer.attachDofData(*OmegaApprox, "OmegaApprox", isBox);

        for (int i = 0; i < numSPhases; ++i)
        {
            std::ostringstream oss;
            oss << "precipitateVolumeFraction_" << FluidSystem::phaseName(numPhases + i);
            writer.attachDofData(*precipitateVolumeFraction[i], oss.str(), isBox);
        }

        for (int i = 0; i < numPhases; ++i)
        {
            for (int j = 0; j < numComponents; ++j)
//                for (int j = 0; j < numComponents+numSecComponents; ++j)
            {
                std::ostringstream oss;
                oss << "x^" << FluidSystem::phaseName(i) << "_" << FluidSystem::componentName(j);
                writer.attachDofData(*moleFraction[i][j], oss.str(), isBox);
            }
        }
        for (int j = 0; j < numSecComponents; ++j)
        {
            std::ostringstream oss;
            oss << "x^" << "w" << "_" << FluidSystem::componentName(j+numComponents);
            writer.attachDofData(*moleFractionSecComp[j], oss.str(), isBox);
        }

        for (int j = 0; j < numComponents; ++j)
        {
            std::ostringstream oss;
            oss << "m^w_" << FluidSystem::componentName(j);
            writer.attachDofData(*molarity[j], oss.str(), isBox);
        }

        if (velocityOutput.enableOutput()) // check if velocity output is demanded
        {
            writer.attachDofData(*velocityW,  "velocityW", isBox, dim);
            writer.attachDofData(*velocityN,  "velocityN", isBox, dim);
        }

        writer.attachCellData(*rank, "process rank");
    }

    /*!
     * \brief Write the current solution to a restart file.
     *
     * \param outStream The output stream of one vertex for the restart file
     * \param entity The Entity
     */
    template<class Entity>
    void serializeEntity(std::ostream &outStream, const Entity &entity)
    {
        // write primary variables
        ParentType::serializeEntity(outStream, entity);

//        int vertIdx = this->dofMapper().map(entity);
        int vertIdx = this->dofMapper().index(entity);
        if (!outStream.good())
            DUNE_THROW(Dune::IOError, "Could not serialize vertex " << vertIdx);

        outStream << staticDat_[vertIdx].phasePresence << " ";
    }

    /*!
     * \brief Reads the current solution for a vertex from a restart
     *        file.
     *
     * \param inStream The input stream of one vertex from the restart file
     * \param entity The Entity
     */
    template<class Entity>
    void deserializeEntity(std::istream &inStream, const Entity &entity)
    {
        // read primary variables
        ParentType::deserializeEntity(inStream, entity);

        // read phase presence
//        int vertIdx = this->dofMapper().map(entity);
        int vertIdx = this->dofMapper().index(entity);
        if (!inStream.good())
            DUNE_THROW(Dune::IOError,
                       "Could not deserialize vertex " << vertIdx);

        inStream >> staticDat_[vertIdx].phasePresence;
        staticDat_[vertIdx].oldPhasePresence
                = staticDat_[vertIdx].phasePresence;

    }

    /*!
     * \copydoc 2pncMin::updateStaticData
     */
    void updateStaticData(SolutionVector &curGlobalSol,
                          const SolutionVector &oldGlobalSol)
    {
        bool wasSwitched = false;

        for (unsigned i = 0; i < staticDat_.size(); ++i)
            staticDat_[i].visited = false;

        FVElementGeometry fvGeometry;
        static VolumeVariables volVars;
        ElementIterator it = this->gridView_().template begin<0> ();
        const ElementIterator &endit = this->gridView_().template end<0> ();
        for (; it != endit; ++it)
        {
            fvGeometry.update(this->gridView_(), *it);
            for (int i = 0; i < fvGeometry.numScv; ++i)
            {
//                int dofIdxGlobal = this->vertexMapper().map(*it, i, dim);
                int dofIdxGlobal = this->vertexMapper().subIndex(*it, i, dim);

                if (staticDat_[dofIdxGlobal].visited)
                    continue;

                staticDat_[dofIdxGlobal].visited = true;
                volVars.update(curGlobalSol[dofIdxGlobal],
                               this->problem_(),
                               *it,
                               fvGeometry,
                               i,
                               false);
                const GlobalPosition &global = it->geometry().corner(i);
                if (primaryVarSwitch_(curGlobalSol,
                                      volVars,
                                      dofIdxGlobal,
                                      global))
                    wasSwitched = true;
            }
        }

        // make sure that if there was a variable switch in an
        // other partition we will also set the switch flag
        // for our partition.
        wasSwitched = this->gridView_().comm().max(wasSwitched);

        setSwitched_(wasSwitched);
    }
    /*!
     * \copydoc 2pncMin::StaticVars
     */
    void updateFailed()
    {
        if (useUCurExtrapolation_ == 1)
        {
            // Reset the current solution to the one of the
            // previous time step so that we can start the next
            // update at a physically meaningful solution.
            this->uCur_ = this->uPrev_;
            Scalar ratio = this->problem_().nextTimeStepSize(this->problem_().timeManager().timeStepSize())
                           / this->problem_().timeManager().timeStepSize();
            this->uCur_ *= 1 + ratio;
            uPrevPrev_ *= ratio;
            this->uCur_ -= uPrevPrev_;
            uPrevPrev_ /= ratio;

            if (isBox)
                this->curHints_ = this->prevHints_;

            this->jacAsm_->reassembleAll();
        }
        else
            ParentType::updateFailed();
    }
    /*!
     * \copydoc 2pncMin::StaticVars
     */
    void advanceTimeLevel()
    {
        if (useUCurExtrapolation_ == 1)
        {
            // make the current solution the previous one.
            uPrevPrev_ = this->uPrev_;
            ParentType::advanceTimeLevel();
            Scalar ratio = this->problem_().nextTimeStepSize(this->problem_().timeManager().timeStepSize())
                           / this->problem_().timeManager().timeStepSize();
            this->uCur_ *= 1 + ratio;
            uPrevPrev_ *= ratio;
            this->uCur_ -= uPrevPrev_;
            uPrevPrev_ /= ratio;
        }
        else
            ParentType::advanceTimeLevel();
    }

protected:
    /*!
     * \copydoc 2pncMin::StaticVars
     */
    struct StaticVars
    {
        int phasePresence;
        bool wasSwitched;

        int oldPhasePresence;
        bool visited;
    };

    /*!
     * \copydoc 2pncMin::resetPhasePresence_
     */
    void resetPhasePresence_()
    {
        int numDofs = this->gridView_().size(dim);
        for (int i = 0; i < numDofs; ++i)
        {
            staticDat_[i].phasePresence
                    = staticDat_[i].oldPhasePresence;
            staticDat_[i].wasSwitched = false;
        }
    }

    /*!
     * \copydoc 2pncMin::updateOldPhasePresence_
     */
    void updateOldPhasePresence_()
    {
        int numDofs = this->gridView_().size(dim);
        for (int i = 0; i < numDofs; ++i)
        {
            staticDat_[i].oldPhasePresence
                    = staticDat_[i].phasePresence;
            staticDat_[i].wasSwitched = false;
        }
    }

    /*!
     * \copydoc 2pncMin::setSwitched_
     */
    void setSwitched_(bool yesno)
    {
        switchFlag_ = yesno;
    }

    //  perform variable switch at a vertex; Returns true if a
    //  variable switch was performed.
    bool primaryVarSwitch_(SolutionVector &globalSol,
                           const VolumeVariables &volVars, int dofIdxGlobal,
                           const GlobalPosition &globalPos)
    {
        // evaluate primary variable switch
        bool wouldSwitch = false;
        int phasePresence = staticDat_[dofIdxGlobal].phasePresence;
        int newPhasePresence = phasePresence;

        // check if a primary var switch is necessary
        if (phasePresence == nPhaseOnly)
        {
            // calculate mole fraction in the hypothetic liquid phase
//            Scalar xll = volVars.moleFraction(wPhaseIdx, wCompIdx);
//            Scalar xlg = volVars.moleFraction(wPhaseIdx, nCompIdx);
//            Scalar xlNaCl = volVars.moleFraction(wPhaseIdx, NaClIdx);

            Scalar xlMax = 1.0;
            Scalar sumxl  = 0;
            for(int compIdx = 0; compIdx < numComponents; ++compIdx)
            {
                sumxl += volVars.moleFraction(wPhaseIdx, compIdx);
            }

            if (sumxl > xlMax)
                wouldSwitch = true;
            if (staticDat_[dofIdxGlobal].wasSwitched)
                xlMax *= 1.02;

            // if the sum of the mole fractions would be larger than
            // 100%, liquid phase appears
            if (sumxl > xlMax)
            {
                // liquid phase appears
                std::cout << "liquid phase appears at vertex " << dofIdxGlobal
                        << ", coordinates: " << globalPos << ", sum of xl: "
                        << sumxl << std::endl;
                newPhasePresence = bothPhases;
                if (formulation == pnsw)
                    globalSol[dofIdxGlobal][switchIdx] = 0.0;
                else if (formulation == pwsn)
                    globalSol[dofIdxGlobal][switchIdx] = 1.0;
            };
        }
        else if (phasePresence == wPhaseOnly)
        {
            // calculate fractions of the partial pressures in the
            // hypothetic gas phase
            Scalar xgMax = 1.0;
            Scalar sumxg  = 0;
            for(int compIdx = 0; compIdx < numComponents; ++compIdx)
            {
                sumxg += volVars.moleFraction(nPhaseIdx, compIdx);
            }
            if (sumxg > xgMax)
                wouldSwitch = true;
            if (staticDat_[dofIdxGlobal].wasSwitched)
                xgMax *= 1.02;

            // if the sum of the mole fractions would be larger than
            // 100%, gas phase appears
            if (sumxg > xgMax)
            {
                // gas phase appears
                std::cout << "gas phase appears at vertex " << dofIdxGlobal
                        << ", coordinates: " << globalPos << ", sum of xg: "
                        << sumxg << std::endl;
                newPhasePresence = bothPhases;
                if (formulation == pnsw)
                    globalSol[dofIdxGlobal][switchIdx] = 0.999;
                else if (formulation == pwsn)
                    globalSol[dofIdxGlobal][switchIdx] = 0.001;
            }
        }
        else if (phasePresence == bothPhases)
        {
            Scalar Smin = 0.0;
            if (staticDat_[dofIdxGlobal].wasSwitched)
                Smin = -0.01;

            if (volVars.saturation(nPhaseIdx) <= Smin)
            {
                wouldSwitch = true;
                // gas phase disappears
                std::cout << "Gas phase disappears at vertex " << dofIdxGlobal
                        << ", coordinates: " << globalPos << ", Sg: "
                        << volVars.saturation(nPhaseIdx) << std::endl;
                newPhasePresence = wPhaseOnly;

                globalSol[dofIdxGlobal][switchIdx]
                        = volVars.moleFraction(wPhaseIdx, nCompIdx);
            }

            else if (volVars.saturation(wPhaseIdx) <= Smin)
            {
                wouldSwitch = true;
                // liquid phase disappears
                std::cout << "Liquid phase disappears at vertex " << dofIdxGlobal
                        << ", coordinates: " << globalPos << ", Sl: "
                        << volVars.saturation(wPhaseIdx) << std::endl;
                newPhasePresence = nPhaseOnly;

                globalSol[dofIdxGlobal][switchIdx]
                        = volVars.moleFraction(nPhaseIdx, wCompIdx);
            }
        }

        staticDat_[dofIdxGlobal].phasePresence = newPhasePresence;
        staticDat_[dofIdxGlobal].wasSwitched = wouldSwitch;
        return phasePresence != newPhasePresence;
    }

    // parameters given in constructor
    std::vector<StaticVars> staticDat_;
    bool switchFlag_;
    bool velocityOutput_;

private:
    Scalar plausibilityTolerance_;
    Scalar pHMax_;
    Scalar pHMin_;
    std::vector<Dune::FieldVector<Scalar, dim> > velocity_;
    std::vector<Scalar> absgradpw_;

    Scalar massUpwindWeight_;
    SolutionVector uPrevPrev_;
    int useUCurExtrapolation_;
};

}

#include "dumux/porousmediumflow/2pncmin/implicit/propertydefaults.hh"

#endif
