// -**- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 * \ingroup TwoPBioMinModel
 *
 * \file
 *
 * \brief Defines the properties required for the two-phase biomineralization
 *        fully implicit model.
 */
#ifndef DUMUX_2PBIOMIN_PROPERTIES_HH
#define DUMUX_2PBIOMIN_PROPERTIES_HH

#include <dumux/porousmediumflow/2pncmin/implicit/properties.hh>
#include <dumux/implicit/properties.hh>

namespace Dumux
{

namespace Properties
{
//////////////////////////////////////////////////////////////////
// Type tags
//////////////////////////////////////////////////////////////////

//! The type tag for the isothermal two phase biomineralisation problems
NEW_TYPE_TAG(TwoPBioMin, INHERITS_FROM(TwoPNCMin));
//NEW_TYPE_TAG(BoxTwoPBioMin, INHERITS_FROM(BoxModel, TwoPBioMin));
//NEW_TYPE_TAG(CCTwoPBioMin, INHERITS_FROM(CCModel, TwoPBioMin));

//! The type tags for the corresponding non-isothermal problems
NEW_TYPE_TAG(TwoPBioMinNI, INHERITS_FROM(TwoPBioMin, NonIsothermal));
//NEW_TYPE_TAG(BoxTwoPBioMinNI, INHERITS_FROM(BoxModel, TwoPBioMinNI));
//NEW_TYPE_TAG(CCTwoPBioMinNI, INHERITS_FROM(CCModel, TwoPBioMinNI));

//////////////////////////////////////////////////////////////////
// Property tags
//////////////////////////////////////////////////////////////////

NEW_PROP_TAG(CO2Tables); //!< The CO2 Tables that are used

}
}

#endif
