// -**- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 * \ingroup TwoPBioMinModel
 * \file
 *
 * \brief Defines default values for most properties required by the
 *        two-phase biomineralization fully implicit model.
 */
#ifndef DUMUX_2PBIOMIN_PROPERTY_DEFAULTS_HH
#define DUMUX_2PBIOMIN_PROPERTY_DEFAULTS_HH

#include "dumux/porousmediumflow/2pncmin/implicit/indices.hh"
#include "2pbiominmodel.hh"
#include "2pbiominfluxvariables.hh"
#include "2pbiominvolumevariables.hh"
#include "properties.hh"

#include <dumux/porousmediumflow/2pnc/implicit/newtoncontroller.hh>
#include <dumux/porousmediumflow/implicit/darcyfluxvariables.hh>
#include <dumux/material/spatialparams/implicit.hh>
#include <dumux/material/fluidmatrixinteractions/2p/thermalconductivitysomerton.hh>

namespace Dumux
{

template <class TypeTag> class TwoPBioMinModel;

namespace Properties {
//////////////////////////////////////////////////////////////////
// Property values
//////////////////////////////////////////////////////////////////

/*!
 * \brief Set the property for the number of secondary components.
 * Secondary components are components calculated from
 * primary components by equilibrium relations and
 * do not have mass balance equation on their own.
 * These components are important in the context of bio-mineralization applications.
 * We just forward the number from the fluid system
 *
 */
SET_PROP(TwoPBioMin, NumSecComponents)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;

public:
    static const int value = FluidSystem::numSecComponents;

};
/*!
 * \brief Set the property for the number of solid phases, excluding the non-reactive matrix.
 *
 * We just forward the number from the fluid system
 *
 */
SET_PROP(TwoPBioMin, NumSPhases)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;

public:
    static const int value = FluidSystem::numSPhases;
};

/*!
 * \brief Set the property for the number of equations.
 * For each component and each precipitated mineral/solid phase one equation has to
 * be solved.
 */
SET_PROP(TwoPBioMin, NumEq)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;

public:
    static const int value = FluidSystem::numComponents + FluidSystem::numSPhases;
};

/*!
 * \brief The fluid state which is used by the volume variables to
 *        store the thermodynamic state. This should be chosen
 *        appropriately for the model ((non-)isothermal, equilibrium, ...).
 *        This can be done in the problem.
 */
SET_PROP(TwoPBioMin, FluidState){
    private:
        typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
        typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    public:
        typedef Dumux::CompositionalFluidState<Scalar, FluidSystem> type;
};

//! Use the 2pbiomin local residual operator
SET_TYPE_PROP(TwoPBioMin,
              LocalResidual,
              TwoPBioMinLocalResidual<TypeTag>);

//! the Model property
SET_TYPE_PROP(TwoPBioMin, Model, TwoPBioMinModel<TypeTag>);

//! the VolumeVariables property
SET_TYPE_PROP(TwoPBioMin, VolumeVariables, TwoPBioMinVolumeVariables<TypeTag>);

//! the FluxVariables property
SET_TYPE_PROP(TwoPBioMin, FluxVariables, TwoPBioMinFluxVariables<TypeTag>);

//! The indices required by the isothermal 2pBioMin model
SET_TYPE_PROP(TwoPBioMin, Indices, TwoPNCMinIndices <TypeTag, /*PVOffset=*/0>);

//! disable useSalinity for the calculation of osmotic pressure by default
SET_BOOL_PROP(TwoPBioMin, useSalinity, false);
/*
//! the FluidState property
SET_TYPE_PROP(TwoPBioMin, FluidState, CompositionalSecCompFluidState<TypeTag>);*/

//! Somerton is used as default model to compute the effective thermal heat conductivity
SET_PROP(TwoPBioMinNI, ThermalConductivityModel)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
public:
    typedef ThermalConductivitySomerton<Scalar, Indices> type;
//    typedef ThermalConductivityAverage<Scalar> type;
};

//////////////////////////////////////////////////////////////////
// Property values for isothermal model required for the general non-isothermal model
//////////////////////////////////////////////////////////////////

// set isothermal Model
SET_TYPE_PROP(TwoPBioMinNI, IsothermalModel, TwoPBioMinModel<TypeTag>);

// set isothermal FluxVariables
SET_TYPE_PROP(TwoPBioMinNI, IsothermalFluxVariables, TwoPBioMinFluxVariables<TypeTag>);

//set isothermal VolumeVariables
SET_TYPE_PROP(TwoPBioMinNI, IsothermalVolumeVariables, TwoPBioMinVolumeVariables<TypeTag>);

//set isothermal LocalResidual
SET_TYPE_PROP(TwoPBioMinNI, IsothermalLocalResidual, TwoPBioMinLocalResidual<TypeTag>);

//set isothermal Indices
SET_TYPE_PROP(TwoPBioMinNI, IsothermalIndices, TwoPNCMinIndices<TypeTag, /*PVOffset=*/0>);
/*
//! the FluidState property
SET_TYPE_PROP(TwoPBioMinNI, IsoFluidState, CompositionalSecCompFluidState<TypeTag>);*/

//set isothermal NumEq
SET_PROP(TwoPBioMinNI, IsothermalNumEq)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;

public:
    static const int value = FluidSystem::numComponents + FluidSystem::numSPhases; // +1 is added by default
};

}
}

#endif
