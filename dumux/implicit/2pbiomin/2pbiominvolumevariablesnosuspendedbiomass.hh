// $Id: 2p2cvolumevariables.hh 5151 2011-02-01 14:22:03Z lauser $
/*****************************************************************************
 *   Copyright (C) 2008,2009 by Vishal Jambhekar,
 *                              Alexzander Kissinger,
 *                              Klaus Mosthaf,                               *
 *                              Andreas Lauser,                              *
 *                              Bernd Flemisch                               *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Contains the quantities which are constant within a
 *        finite volume in the two-phase, n-component mineralisation model.
 */
#ifndef DUMUX_2PBIOMIN_VOLUME_VARIABLES_NO_SUSPENDED_BIOMASS_HH
#define DUMUX_2PBIOMIN_VOLUME_VARIABLES_NO_SUSPENDED_BIOMASS_HH

#include <dumux/implicit/model.hh>
#include <dumux/material/fluidstates/compositionalseccompfluidstate.hh>
#include <dumux/common/math.hh>
#include <dune/common/parallel/collectivecommunication.hh>
#include <vector>
#include <iostream>

#include "dumux/porousmediumflow/2pncmin/implicit/volumevariables.hh"
#include "2pbiominfluxvariables.hh"
#include "2pbiominmodel.hh"

#include <dumux/material/constraintsolvers/computefromreferencephase2pncmin.hh>
#include <dumux/material/constraintsolvers/miscible2pnccomposition.hh>

#include <dumux/material/binarycoefficients/brine_co2.hh>
//#include <appl/co2/biomin/bioco2tables.hh>

namespace Dumux
{
/*!
 * \ingroup TwoPBioMinModel
 * \brief Contains the quantities which are are constant within a
 *        finite volume in the two-phase, biomineralisation model.
 */
template <class TypeTag>
class TwoPBioMinVolumeVariablesNoSuspendedBiomass : public TwoPNCMinVolumeVariables<TypeTag>
{
    typedef TwoPNCMinVolumeVariables<TypeTag> ParentType;
    typedef ImplicitVolumeVariables<TypeTag> BaseClassType;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) Implementation;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLawParams) MaterialLawParams;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    typedef typename GET_PROP_TYPE(TypeTag, CO2Tables) CO2Tables;
//    typedef Dumux::BioMin::CO2Tables CO2Tables;
    typedef typename GET_PROP_TYPE(TypeTag, Chemistry) Chemistry;

    typedef Dumux::BinaryCoeff::Brine_CO2<Scalar, CO2Tables, true> Brine_CO2;

    enum {
        dim = GridView::dimension,
        dimWorld=GridView::dimensionworld,

        numPhases = GET_PROP_VALUE(TypeTag, NumPhases),
        numSPhases =  GET_PROP_VALUE(TypeTag, NumSPhases),
        numComponents = GET_PROP_VALUE(TypeTag, NumComponents),
        numMajorComponents = GET_PROP_VALUE(TypeTag, NumMajorComponents),
        numSecComponents = GET_PROP_VALUE(TypeTag, NumSecComponents),

        // formulations
        formulation = GET_PROP_VALUE(TypeTag, Formulation),
        pwsn = TwoPNCFormulation::pwsn,
        pnsw = TwoPNCFormulation::pnsw,

        // phase indices
        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx,

        bPhaseIdx = FluidSystem::bPhaseIdx,
        cPhaseIdx = FluidSystem::cPhaseIdx,

        // component indices
        wCompIdx = FluidSystem::wCompIdx,
        nCompIdx = FluidSystem::nCompIdx,
        NaIdx = FluidSystem::NaIdx,
        ClIdx = FluidSystem::ClIdx,
        CaIdx = FluidSystem::CaIdx,
        CO2Idx = FluidSystem::CO2Idx,
        HIdx = FluidSystem::HIdx,
        O2Idx = FluidSystem::O2Idx,
        UreaIdx = FluidSystem::UreaIdx,
        CO3Idx = FluidSystem::CO3Idx,
        HCO3Idx = FluidSystem::HCO3Idx,
        NH4Idx = FluidSystem::NH4Idx,
//      BiosuspIdx = FluidSystem::BiosuspIdx,
        BiosubIdx = FluidSystem::BiosubIdx,
        CalciteIdx = FluidSystem::CalciteIdx,

        // phase presence enums
        nPhaseOnly = Indices::nPhaseOnly,
        wPhaseOnly = Indices::wPhaseOnly,
        bothPhases = Indices::bothPhases,

        // primary variable indices
        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,

    };

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename Grid::ctype CoordScalar;
    typedef Dumux::Miscible2pNCComposition<Scalar, FluidSystem> miscible2pNCComposition;
    typedef Dumux::ComputeFromReferencePhase2pNCMin<Scalar, FluidSystem> computeFromReferencePhase2pNCMin;

    typedef Dune::FieldVector<Scalar, dim> Vector;

public:

      //! The type of the object returned by the fluidState() method
//    typedef CompositionalFluidState<Scalar, FluidSystem> FluidState;
    typedef CompositionalSecCompFluidState<Scalar, FluidSystem> FluidState;
    /*!
     * \copydoc 2pncMin::update
     *
     */
    void update(const PrimaryVariables &primaryVariables,
                const Problem &problem,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                int scvIdx,
                bool isOldSol)
    {
        ParentType::update(primaryVariables,
                           problem,
                           element,
                           fvGeometry,
                           scvIdx,
                           isOldSol);

        completeFluidState(primaryVariables, problem, element, fvGeometry, scvIdx, fluidState_, isOldSol);

        /////////////
        // calculate the remaining quantities
        /////////////

    const MaterialLawParams &materialParams =
      problem.spatialParams().materialLawParams(element, fvGeometry, scvIdx);

    // Second instance of a parameter cache.
        // Could be avoided if diffusion coefficients also
        // became part of the fluid state.
        typename FluidSystem::ParameterCache paramCache;
        paramCache.updateAll(fluidState_);
        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
            {
                // relative permeabilities
                Scalar kr;
                if (phaseIdx == wPhaseIdx)
                    kr = MaterialLaw::krw(materialParams, fluidState_.saturation(wPhaseIdx));
                else // ATTENTION: krn requires the liquid saturation
                    // as parameter!
                    kr = MaterialLaw::krn(materialParams, fluidState_.saturation(wPhaseIdx));
                ParentType::mobility_[phaseIdx] = kr / fluidState_.viscosity(phaseIdx);
                Valgrind::CheckDefined(ParentType::mobility_[phaseIdx]);
               int compIIdx = phaseIdx;
               for(int compIdx = 0; compIdx < numComponents; ++compIdx)
                {
                    int compJIdx = compIdx;
                    // binary diffusion coefficents
                    ParentType::diffCoeff_[phaseIdx][compIdx] = 0.0;
                    if(compIIdx!= compJIdx)
                      ParentType::diffCoeff_[phaseIdx][compIdx] = FluidSystem::binaryDiffusionCoefficient(fluidState_,
                                                                                    paramCache,
                                                                                    phaseIdx,
                                                                                    compIIdx,
                                                                                    compJIdx);
                    Valgrind::CheckDefined(ParentType::diffCoeff_[phaseIdx][compIdx]);
                }
            }
    // porosity and permeability evaluation

    ParentType::initialPorosity_ = problem.spatialParams().porosity(element,
                                                        fvGeometry,
                                                        scvIdx); //1 - InitialSolidity;
    ParentType::porosity_= ParentType::initialPorosity_;
    Scalar critPoro = problem.spatialParams().critPorosity(element,
                fvGeometry,
                scvIdx);
    for(int sPhaseIdx = 0; sPhaseIdx < numSPhases; ++sPhaseIdx)
    {
        ParentType::precipitateVolumeFraction_[sPhaseIdx] = primaryVariables[numComponents + sPhaseIdx];
        ParentType::porosity_-= ParentType::precipitateVolumeFraction_[sPhaseIdx];
    }

     Scalar porosity = ParentType::porosity_;
     Scalar initialPorosity = ParentType::initialPorosity_;

    // Kozeny-Carman relation
     Scalar KozenyCarmanExponent = 3;
     if(porosity>critPoro)
         ParentType::permeabilityFactor_ = std::pow(((porosity - critPoro)/(initialPorosity - critPoro)), KozenyCarmanExponent);
     else
         ParentType::permeabilityFactor_ = 0.0;

    salinity_= 0.0;
    for (int compIdx = NaIdx; compIdx<= CaIdx ; compIdx++)  //salinity = XlNa + XlCl + XlCa
    {
        if(fluidState_.moleFraction(wPhaseIdx, compIdx)>0)
        {
            salinity_+= fluidState_.massFraction(wPhaseIdx, compIdx);
        }
    }

    moleFractionSalinity_ = Chemistry::salinityToMolFrac_(salinity_);

    Scalar mNa = Chemistry::moleFracToMolality(fluidState_.moleFraction(wPhaseIdx,NaIdx), moleFractionSalinity_, fluidState_.moleFraction(wPhaseIdx,nCompIdx));  //[mol_sodium/kg_H2O]
    if (mNa < 0)
         mNa = 0;
    Scalar mCl = Chemistry::moleFracToMolality(fluidState_.moleFraction(wPhaseIdx,ClIdx), moleFractionSalinity_, fluidState_.moleFraction(wPhaseIdx,nCompIdx));  //[mol_chloride/kg_H2O]
    if (mCl < 0)
         mCl = 0;
    Scalar mNH4 = Chemistry::moleFracToMolality(fluidState_.moleFraction(wPhaseIdx,NH4Idx), moleFractionSalinity_, fluidState_.moleFraction(wPhaseIdx,nCompIdx));  //[mol_NH4/kg_H2O]
    if (mNH4 < 0)
         mNH4 = 0;
    Scalar mCa = Chemistry::moleFracToMolality(fluidState_.moleFraction(wPhaseIdx,CaIdx), moleFractionSalinity_, fluidState_.moleFraction(wPhaseIdx,nCompIdx));  //[mol_calcium/kg_H2O]
    if (mCa < 0)
         mCa = 0;
    Scalar mCO3 = Chemistry::moleFracToMolality(fluidState_.moleFraction(wPhaseIdx,CO3Idx), moleFractionSalinity_, fluidState_.moleFraction(wPhaseIdx,nCompIdx));  //[mol_CO3/kg_H2O]
    if (mCO3 < 0)
         mCO3 = 0;
    Scalar mHCO3 = Chemistry::moleFracToMolality(fluidState_.moleFraction(wPhaseIdx,HCO3Idx), moleFractionSalinity_, fluidState_.moleFraction(wPhaseIdx,nCompIdx));  //[mol_HCO3/kg_H2O]
    if (mHCO3 < 0)
         mHCO3 = 0;
    Scalar mH = Chemistry::moleFracToMolality(fluidState_.moleFraction(wPhaseIdx,HIdx), moleFractionSalinity_, fluidState_.moleFraction(wPhaseIdx,nCompIdx));  //[mol_H/kg_H2O]
    Scalar mUrea = Chemistry::moleFracToMolality(fluidState_.moleFraction(wPhaseIdx,UreaIdx), moleFractionSalinity_, fluidState_.moleFraction(wPhaseIdx,nCompIdx));  //[mol_urea/kg_H2O]
    if (mUrea < 0)
         mUrea = 0;

    pH_ =-log10(mH);
     if (mH < 0 || isnan(mH))
         pH_ = 99;
     Chemistry chemistry;

     //get the absolute value of the wPhase pressure gradient from the velocity stored in the model. (quick and dirty hack)
     int dofIdxGlobal = problem.model().vertexMapper().subIndex(element, scvIdx, dim);
     Scalar absgradpw = problem.model().absgradpw(dofIdxGlobal);
//     Scalar absgradpw_old = problem.model().velocity(dofIdxGlobal);
//     absgradpw_old *= fluidState_.viscosity(wPhaseIdx);
//     absgradpw_old /= (ParentType::permeabilityFactor_ * problem.spatialParams().intrinsicPermeabilityScalar(element, fvGeometry, scvIdx));

//     std::cout<<"absgradpw = "<< absgradpw <<"\n old absgradpw = "<< absgradpw_old <<std::endl;

     Scalar Sw = fluidState_.saturation(wPhaseIdx);
     Scalar volFracBiofilm = ParentType::precipitateVolumeFraction_[bPhaseIdx-numPhases];
     Scalar volFracCalcite = ParentType::precipitateVolumeFraction_[cPhaseIdx-numPhases];
//   Scalar cBio = fluidState_.moleFraction(wPhaseIdx, BiosuspIdx) * fluidState_.molarDensity(wPhaseIdx) * FluidSystem::molarMass(BiosuspIdx);
     Scalar cSubstrate = fluidState_.moleFraction(wPhaseIdx, BiosubIdx) * fluidState_.molarDensity(wPhaseIdx) * FluidSystem::molarMass(BiosubIdx);
     Scalar cO2 = fluidState_.moleFraction(wPhaseIdx, O2Idx) * fluidState_.molarDensity(wPhaseIdx) * FluidSystem::molarMass(O2Idx);
     Scalar mue = chemistry.kmue() * chemistry.Yield()* cSubstrate / (chemistry.Ks() + cSubstrate)* cO2 / (chemistry.Ke() + cO2);

    saturationIdx_ = chemistry.Omega(mNa, mCa, mNH4, mHCO3, mCO3, mCl, fluidState_.temperature(wPhaseIdx));
    rprecChem_ = chemistry.rprec(initialPorosity, volFracCalcite, mNa, mCa, mNH4, mHCO3, mCO3, mCl, fluidState_.temperature(wPhaseIdx));
    rureaChem_ = chemistry.rurea(mUrea, density(bPhaseIdx), volFracBiofilm, mNH4);
    rattachChem_ = 0; //chemistry.ra(cBio, volFracBiofilm, porosity, Sw);
    rdetachChem_ = chemistry.rd(density(bPhaseIdx), volFracBiofilm, initialPorosity, porosity,Sw, absgradpw, volFracCalcite, cO2, cSubstrate);
    rgrowthChem_ = chemistry.rgf(density(bPhaseIdx), volFracBiofilm, cO2, cSubstrate);
    rdecayChem_ = chemistry.rdcf(density(bPhaseIdx), volFracBiofilm, rprecChem_, density(cPhaseIdx), volFracCalcite, initialPorosity);

        // energy related quantities not belonging to the fluid state
        asImp_().updateEnergy_(primaryVariables, problem,element, fvGeometry, scvIdx, isOldSol);
    }

      /*!
    * \copydoc TwoPNCMinModel::completeFluidState
    * \the secondary components are calculated here by calculateEquilibriumChemistry
    */
  static void completeFluidState(const PrimaryVariables& primaryVariables,
                  const Problem& problem,
                  const Element& element,
                  const FVElementGeometry& fvGeometry,
                  int scvIdx,
                  FluidState& fluidState,
                  bool isOldSol = false)

    {
        Scalar t = Implementation::temperature_(primaryVariables, problem, element,
                                                fvGeometry, scvIdx);
        fluidState.setTemperature(t);

      int globalVertIdx = problem.model().dofMapper().subIndex(element, scvIdx, dim);
        int phasePresence = problem.model().phasePresence(globalVertIdx, isOldSol);

    /////////////
        // set the saturations
        /////////////

    Scalar Sg;
        if (phasePresence == nPhaseOnly)
            Sg = 1.0;
        else if (phasePresence == wPhaseOnly) {
            Sg = 0.0;
        }
        else if (phasePresence == bothPhases) {
            if (formulation == pwsn)
                Sg = primaryVariables[switchIdx];
            else if (formulation == pnsw)
                Sg = 1.0 - primaryVariables[switchIdx];
            else DUNE_THROW(Dune::InvalidStateException, "Formulation: " << formulation << " is invalid.");
        }
    else DUNE_THROW(Dune::InvalidStateException, "phasePresence: " << phasePresence << " is invalid.");
        fluidState.setSaturation(nPhaseIdx, Sg);
        fluidState.setSaturation(wPhaseIdx, 1.0 - Sg);

            /////////////
        // set the pressures of the fluid phases
        /////////////

        // calculate capillary pressure
        const MaterialLawParams &materialParams
        = problem.spatialParams().materialLawParams(element, fvGeometry, scvIdx);
        Scalar pC = MaterialLaw::pc(materialParams, 1 - Sg);

        // extract the pressures
        if (formulation == pwsn) {
            fluidState.setPressure(wPhaseIdx, primaryVariables[pressureIdx]);
            if (primaryVariables[pressureIdx] + pC < 0.0)
                            DUNE_THROW(Dumux::NumericalProblem,"Capillary pressure is too low");
            fluidState.setPressure(nPhaseIdx, primaryVariables[pressureIdx] + pC);
        }
        else if (formulation == pnsw) {
            fluidState.setPressure(nPhaseIdx, primaryVariables[pressureIdx]);
// Here we check for (p_g - pc) in order to ensure that (p_l > 0)
            if (primaryVariables[pressureIdx] - pC < 0.0)
            {
                std::cout<< "p_g: "<< primaryVariables[pressureIdx]<<" Cap_press: "<< pC << std::endl;
                DUNE_THROW(Dumux::NumericalProblem,"Capillary pressure is too high");
            }
//          std::cout<< "p_g: "<< primaryVariables[pressureIdx]<<" Cap_press: "<< pC << std::endl;
            fluidState.setPressure(wPhaseIdx, primaryVariables[pressureIdx] - pC);
        }
        else DUNE_THROW(Dune::InvalidStateException, "Formulation: " << formulation << " is invalid.");

        /////////////
        // calculate the phase compositions
        /////////////

        // set the known mole fractions in the fluidState so that they
        // can be used by the miscible2pNCComposition constraint solver
        // and can be used to compute the fugacity coefficients
        for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
        {
            fluidState.setMoleFraction(wPhaseIdx, compIdx, primaryVariables[compIdx]);
        }

    typename FluidSystem::ParameterCache paramCache;

        // now comes the tricky part: calculate phase composition
        if (phasePresence == bothPhases) {
            // both phases are present, phase composition results from
            // the gas <-> liquid equilibrium. This is
            // the job of the "MiscibleMultiPhaseComposition"
            // constraint solver

            miscible2pNCComposition::solve(fluidState,
                                           paramCache,
                                           wPhaseIdx,   //known phaseIdx
                                           /*setViscosity=*/true,
                                           /*setEnthalpy=**/false);
        }
        else if (phasePresence == nPhaseOnly){

            Dune::FieldVector<Scalar, numComponents + numSecComponents> moleFrac;
            Dune::FieldVector<Scalar, numComponents> fugCoeffL;
            Dune::FieldVector<Scalar, numComponents> fugCoeffG;

            for (int compIdx=0; compIdx<numComponents; ++compIdx)
            {
                fugCoeffL[compIdx] = FluidSystem::fugacityCoefficient(fluidState,
                                                                paramCache,
                                                                wPhaseIdx,
                                                                compIdx);
                fugCoeffG[compIdx] = FluidSystem::fugacityCoefficient(fluidState,
                                                                paramCache,
                                                                nPhaseIdx,
                                                                compIdx);
            }
            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            {
                moleFrac[compIdx] = primaryVariables[compIdx]*fugCoeffL[compIdx]*fluidState.pressure(wPhaseIdx)
                                        /(fugCoeffG[compIdx]*fluidState.pressure(nPhaseIdx));
            }
            moleFrac[wCompIdx] =  primaryVariables[switchIdx];
            Scalar sumMoleFracNotGas = 0;
            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            {
                    sumMoleFracNotGas+=moleFrac[compIdx];
            }
            sumMoleFracNotGas += moleFrac[wCompIdx];
            moleFrac[nCompIdx] = 1 - sumMoleFracNotGas;

//            for (int compIdx=numComponents; compIdx<numSecComponents; ++compIdx)
//            {
//              moleFrac[compIdx] = 0;          //no secondary component in the gas phase!!
//            }

            // convert mass to mole fractions and set the fluid state
            for (int compIdx=0; compIdx<numComponents; ++compIdx)
            {
                fluidState.setMoleFraction(nPhaseIdx, compIdx, moleFrac[compIdx]);
            }
            for (int compIdx=numComponents; compIdx<numComponents + numSecComponents; ++compIdx)
            {
                fluidState.setMoleFractionSecComp(nPhaseIdx, compIdx, 0);
                fluidState.setMoleFractionSecComp(wPhaseIdx, compIdx, 0);
                if (compIdx == CO2Idx)
                    fluidState.setMoleFractionSecComp(nPhaseIdx, compIdx, fluidState.moleFraction(nPhaseIdx, nCompIdx));
            }
            // calculate the composition of the remaining phases (as
            // well as the densities of all phases). this is the job
            // of the "computeFromReferencePhase2pnc" constraint solver
             computeFromReferencePhase2pNCMin::solve(fluidState,
                                             paramCache,
                                             wPhaseIdx,
                                             nPhaseOnly,
                                             /*setViscosity=*/true,
                                             /*setEnthalpy=**/false);

         }
        else if (phasePresence == wPhaseOnly){
        // only the liquid phase is present, i.e. liquid phase
        // composition is stored explicitly.
        // extract _mass_ fractions in the gas phase
            Dune::FieldVector<Scalar, numComponents + numSecComponents> moleFrac;

            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            {
                moleFrac[compIdx] = primaryVariables[compIdx];
            }
            moleFrac[nCompIdx] = primaryVariables[switchIdx];
            Scalar sumMoleFracNotWater = 0;
            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            {
                    sumMoleFracNotWater+=moleFrac[compIdx];
            }
            sumMoleFracNotWater += moleFrac[nCompIdx];
            moleFrac[wCompIdx] = 1 - sumMoleFracNotWater;

            for (int compIdx=numComponents; compIdx<numComponents + numSecComponents; ++compIdx)
            {
                moleFrac[compIdx] = 0;
            }
            Scalar XlSalinity= 0.0;
            for (int compIdx = NaIdx; compIdx<= CaIdx ; compIdx++)  //salinity = XlNa + XlCl + XlCa
            {
                if(fluidState.massFraction(wPhaseIdx, compIdx)>0)
                {
                    XlSalinity+= fluidState.massFraction(wPhaseIdx, compIdx);
                }
            }
                        Scalar xgH2O;
                        Scalar xlCO2;
                        Scalar x_NaCl;

             Brine_CO2::calculateMoleFractions(fluidState.temperature(),
                                        fluidState.pressure(nPhaseIdx),
                                        XlSalinity,
                                        /*knownPhaseIdx=*/-1,
                                        xlCO2,
                                        xgH2O);
                // normalize the phase compositions
                xlCO2 = std::max(0.0, std::min(1.0, xlCO2));
                xgH2O = std::max(0.0, std::min(1.0, xgH2O));

//              Scalar xgO2 = 0;

    //            for (int compIdx=0; compIdx<numComponents + numSecComponents; ++compIdx)
                for (int compIdx=0; compIdx<numComponents; ++compIdx)
                {
                    fluidState.setMoleFraction(wPhaseIdx, compIdx, moleFrac[compIdx]);
                    fluidState.setMoleFraction(nPhaseIdx, compIdx, 0);
                }
                fluidState.setMoleFraction(nPhaseIdx, wCompIdx, xgH2O);
//              fluidState.setMoleFraction(nPhaseIdx, O2Idx, xgO2);
//              fluidState.setMoleFraction(nPhaseIdx, nCompIdx, 1-xgH2O-xgO2);
                fluidState.setMoleFraction(nPhaseIdx, nCompIdx, 1-xgH2O);


//            // convert mass to mole fractions and set the fluid state
//            for (int compIdx=0; compIdx<numComponents; ++compIdx)
//            {
//              fluidState.setMoleFraction(wPhaseIdx, compIdx, moleFrac[compIdx]);
//            }
//            // calculate the composition of the remaining phases (as
//            // well as the densities of all phases). this is the job
//            // of the "computeFromReferencePhase2pnc" constraint solver
//             computeFromReferencePhase2pnc::solve(fluidState,
//                                             paramCache,
//                                             wPhaseIdx,
//                                             /*setViscosity=*/true,
//                                             /*setEnthalpy=**/false);
//

//
//            for (int compIdx=numComponents; compIdx<numComponents + numSecComponents; ++compIdx)
//            {
//              fluidState.setMoleFractionSecComp(wPhaseIdx, compIdx, moleFrac[compIdx]);
//              fluidState.setMoleFractionSecComp(nPhaseIdx, compIdx, 0);
//              if (compIdx == CO2Idx)
//                  fluidState.setMoleFractionSecComp(nPhaseIdx, CO2Idx, fluidState.moleFraction(nPhaseIdx, nCompIdx));
//            }

        }
        paramCache.updateAll(fluidState);
        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
        {
            // compute and set the viscosity
            Scalar mu = FluidSystem::viscosity(fluidState, paramCache, phaseIdx);
            fluidState.setViscosity(phaseIdx, mu);

            // compute and set the density
            Scalar rho = FluidSystem::density(fluidState, paramCache, phaseIdx);
            fluidState.setDensity(phaseIdx, rho);

            // compute and set the enthalpy
            Scalar h = Implementation::enthalpy_(fluidState, paramCache, phaseIdx);
            fluidState.setEnthalpy(phaseIdx, h);
        }

        if (phasePresence == bothPhases){
            Scalar XlSalinity= 0.0;
        for (int compIdx = NaIdx; compIdx<= CaIdx ; compIdx++)  //salinity = XlNa + XlCl + XlCa
        {
            if(fluidState.massFraction(wPhaseIdx, compIdx)>0)
            {
                XlSalinity+= fluidState.massFraction(wPhaseIdx, compIdx);
            }
        }
        Scalar xlSalinity = Chemistry::salinityToMolFrac_(XlSalinity);

                        Scalar xgH2O;
                        Scalar xlCO2;
                        Scalar x_Sal;

             Brine_CO2::calculateMoleFractions(fluidState.temperature(),
                                        fluidState.pressure(nPhaseIdx),
                                        XlSalinity,
                                        /*knownPhaseIdx=*/-1,
                                        xlCO2,
                                        xgH2O);
                // normalize the phase compositions
                xlCO2 = std::max(0.0, std::min(1.0, xlCO2));
                xgH2O = std::max(0.0, std::min(1.0, xgH2O));
             Scalar constant_C  = xlCO2;

             Dune::FieldVector<Scalar, numComponents + numSecComponents> moleFrac;

             for(int compIdx=0; compIdx<numComponents; ++compIdx)
                {
                    moleFrac [compIdx] = fluidState.moleFraction(wPhaseIdx, compIdx);
                    if (moleFrac [compIdx] < 0)
                        moleFrac [compIdx] = 0;
                }

             Chemistry chemistry;
             chemistry.calculateEquilibriumChemistry(fluidState, phasePresence, xlSalinity, constant_C, moleFrac);

//           fluidState.setMoleFraction(wPhaseIdx, nCompIdx, moleFrac[nCompIdx]); // xlCTOT = xlco2+xlco3+xlhco3, like computed in the chemistry
             for (int compIdx=numComponents; compIdx<numComponents + numSecComponents; ++compIdx)
                {
                    fluidState.setMoleFractionSecComp(wPhaseIdx, compIdx, moleFrac[compIdx]);
                    fluidState.setMoleFractionSecComp(nPhaseIdx, compIdx, 0);
                    if (compIdx== CO2Idx)
                        fluidState.setMoleFractionSecComp(nPhaseIdx, CO2Idx, fluidState.moleFraction(nPhaseIdx, nCompIdx));
                }

        }
        else if (phasePresence == wPhaseOnly){
            Dune::FieldVector<Scalar, numComponents + numSecComponents> moleFrac;
            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            {
                moleFrac[compIdx] = primaryVariables[compIdx];
                if (moleFrac [compIdx] < 0)
                    moleFrac [compIdx] = 0;
            }
            moleFrac[nCompIdx] = primaryVariables[switchIdx];
            if (moleFrac[nCompIdx] < 0)
                moleFrac[nCompIdx] = 1e-40;

            Scalar sumMoleFracNotWater = 0;
            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            {
                sumMoleFracNotWater+=moleFrac[compIdx];
            }
            sumMoleFracNotWater += moleFrac[nCompIdx];
            moleFrac[wCompIdx] = 1 - sumMoleFracNotWater;
            for (int compIdx=numComponents; compIdx<numComponents + numSecComponents; ++compIdx)
            {
                moleFrac[compIdx] = 0;
            }

            Scalar XlSalinity= 0.0;
        for (int compIdx = NaIdx; compIdx<= CaIdx ; compIdx++)  //salinity = XlNa + XlCl + XlCa
        {
            if(fluidState.massFraction(wPhaseIdx, compIdx)>0)
            {
                XlSalinity+= fluidState.massFraction(wPhaseIdx, compIdx);
            }
        }
        Scalar xlSalinity = Chemistry::salinityToMolFrac_(XlSalinity);
            Scalar constant_C = moleFrac[nCompIdx];
            if (constant_C < 0 || constant_C>1)
                 std::cout<< "constant_C = moleFrac[nCompIdx] (wPhaseonly) =   "<<  constant_C <<std::endl;

            Chemistry chemistry;
            chemistry.calculateEquilibriumChemistry(fluidState, phasePresence, xlSalinity, constant_C, moleFrac);

            for (int compIdx=numComponents; compIdx<numComponents + numSecComponents; ++compIdx)
            {
                fluidState.setMoleFractionSecComp(wPhaseIdx, compIdx, moleFrac[compIdx]);
                fluidState.setMoleFractionSecComp(nPhaseIdx, compIdx, 0);
                if (compIdx == CO2Idx)
                    fluidState.setMoleFractionSecComp(nPhaseIdx, CO2Idx, fluidState.moleFraction(nPhaseIdx, nCompIdx));
            }

        }
        paramCache.updateAll(fluidState);
        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
        {
            // compute and set the viscosity
            Scalar mu = FluidSystem::viscosity(fluidState, paramCache, phaseIdx);
            fluidState.setViscosity(phaseIdx, mu);

            // compute and set the density
            Scalar rho = FluidSystem::density(fluidState, paramCache, phaseIdx);
            fluidState.setDensity(phaseIdx, rho);

            // compute and set the enthalpy
            Scalar h = Implementation::enthalpy_(fluidState, paramCache, phaseIdx);
            fluidState.setEnthalpy(phaseIdx, h);
        }
    }

    Scalar salinity() const
    {
        std::cout <<"test: function Scalar salinity() used"<<std::endl;
        return salinity_; }

    Scalar moleFracSalinity() const
    { return moleFractionSalinity_; }

    Scalar pH() const
    { return pH_; }

    Scalar precipitateVolumeFraction(int phaseIdx) const
    { return ParentType::precipitateVolumeFraction_[phaseIdx - numPhases]; }

    Scalar initialPorosity() const
    { return ParentType::initialPorosity_;}
//
//    Scalar permeabilityFactor() const
//    { return ParentType::permeabilityFactor_; }

    Scalar moleFraction(int phaseIdx, int compIdx) const
    { return fluidState_.moleFraction(phaseIdx, compIdx); }

    Scalar massFraction(int phaseIdx, int compIdx) const
    { return fluidState_.massFraction(phaseIdx, compIdx); }

    /*!
     * \brief Returns the phase state for the control-volume.
     */
    const FluidState &fluidState() const
    { return fluidState_; }

    /*!
     * \copydoc 2pncMin::saturation
     */
    Scalar saturation(int phaseIdx) const
    {
        if (phaseIdx<numPhases)
            return fluidState_.saturation(phaseIdx);
        else
            DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
            return 1;
    }

    /*!
     * \copydoc 2pncMin::density
     */
    Scalar density(int phaseIdx) const
    {
        if (phaseIdx<numPhases)
            return fluidState_.density(phaseIdx);

        else if (phaseIdx<numPhases+numSPhases)
//                return FluidSystem::solidPhaseDensity(phaseIdx);
                return FluidSystem::precipitateDensity(phaseIdx);
        else
            DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
            return 1;
    }

    /*!
     * \copydoc 2pncMin::molarDensity
     */
    Scalar molarDensity(int phaseIdx) const
    {
        if (phaseIdx<numPhases)
            return fluidState_.molarDensity(phaseIdx);

        else if (phaseIdx<numPhases+numSPhases)
//            return FluidSystem::solidPhaseDensity(phaseIdx)/FluidSystem::molarMassMineral(phaseIdx);
              return FluidSystem::precipitateMolarDensityDensity(phaseIdx);
        else
            DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
            return 1;
    }

    /*!
     * \copydoc 2pncMin::pressure
     */
    Scalar pressure(int phaseIdx) const
    { return fluidState_.pressure(phaseIdx); }

    /*!
     * \copydoc 2pncMin::temperature
     */
    Scalar temperature() const
    { return fluidState_.temperature(/*phaseIdx=*/0); }

//    /*!
//     * \copydoc 2pncMin::mobility
//     */
//    Scalar mobility(int phaseIdx) const
//    {
//        return ParentType::mobility_[phaseIdx];
//    }

    /*!
     * \copydoc 2pncMin::capillaryPressure
     */
    Scalar capillaryPressure() const
    { return fluidState_.pressure(nPhaseIdx) - fluidState_.pressure(wPhaseIdx); }

//    /*!
//     * copydoc 2pncMin::porosity
//     */
//    Scalar porosity() const
//    { return ParentType::porosity_; }
//
//    /*!
//     * copydoc 2pncMin::diffCoeff
//     */
//    Scalar diffCoeff(int phaseIdx, int compIdx) const
//    { return ParentType::diffCoeff_[phaseIdx][compIdx]; }

    Scalar saturationIdx() const
    {   return saturationIdx_;}

    Scalar Appa_KSP() const
    {   return Appa_Ksp_;}

    Scalar rprec() const
    {   return rprecChem_;}
    Scalar rurea() const
    {   return rureaChem_;}

    Scalar rattachChem() const
    {   return rattachChem_;}
    Scalar rdetachChem() const
    {   return rdetachChem_;}
    Scalar rgrowthChem() const
    {   return rgrowthChem_;}
    Scalar rdecayChem() const
    {   return rdecayChem_;}


//protected:

  static Scalar temperature_(const PrimaryVariables &priVars,
                            const Problem& problem,
                            const Element &element,
                            const FVElementGeometry &elemGeom,
                            int scvIdx)
    {
        return problem.temperature(element, elemGeom, scvIdx);
    }

    template<class ParameterCache>
    static Scalar enthalpy_(const FluidState& fluidState,
                            const ParameterCache& paramCache,
                            int phaseIdx)
    {
        return 0;
    }
protected:

    //Scalar temperature_;     //!< Temperature within the control volume
//    Scalar porosity_;        //!< Effective porosity within the control volume
//    Scalar mobility_[numPhases];  //!< Effective mobility within the control volume
    Scalar precipitateVolumeFraction_[numSPhases];
//    Scalar density_;
//    Scalar permeabilityFactor_;
    FluidState fluidState_;
//    Scalar initialPorosity_;
    Scalar salinity_;
    Scalar moleFractionSalinity_;
    Scalar pH_;
//    Scalar moleFractionSec_[numPhases][numSecComponents];
//    Dune::FieldMatrix<Scalar, numPhases, numComponents> diffCoeff_;
    Scalar rprec_;
    Scalar rurea_;

    Scalar rattach_;
    Scalar rdetach_;
    Scalar rgrowth_;
    Scalar rdecay_;

    Scalar rprecChem_;
    Scalar rureaChem_;

    Scalar rattachChem_;
    Scalar rdetachChem_;
    Scalar rgrowthChem_;
    Scalar rdecayChem_;

//    Scalar mue_;
    Scalar Appa_Ksp_;
    Scalar saturationIdx_;

private:
    Implementation &asImp_()
    { return *static_cast<Implementation*>(this); }

    const Implementation &asImp_() const
    { return *static_cast<const Implementation*>(this); }

};

} // end namespace

#endif
