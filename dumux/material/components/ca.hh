/*
 * Ca.hh
 *
 *  Created on: 14.03.2011
 *      Author: kissinger
 */

/*!
 * \file
 *
 * \brief A class for the Ca2+ fluid properties
 */
#ifndef DUMUX_CA_HH
#define DUMUX_CA_HH

#include <dumux/common/exceptions.hh>
#include <dumux/material/components/component.hh>

#include <cmath>
#include <iostream>


namespace Dumux
{
/*!
 * \brief A class for the Ca (Calcium ion) fluid properties
 */
template <class Scalar>
class Ca : public Component<Scalar, Ca<Scalar> >
{
public:
    /*!
     * \brief A human readable name for the Ca.
     */
    static std::string name()
    { return "Ca"; }

    /*!
     * \brief The mass in [kg] of one mole of Ca.
     */
    static Scalar molarMass()
    { return 40.078e-3; } // kg/mol

    /*!
     * \brief The diffusion Coefficient of Ca in water.
     */
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    { return 2e-9; }

    static Scalar charge()
    {
        return 2.0;
    }

    /*!
     * \brief Return the constant ai Parkhurst (1990) for the modified Debye-Hückel equation
     */

    static Scalar ai()
    {
        return 5.0e-10;
    }

    /*!
     * \brief Return the constant bi Parkhurst (1990) for the modified Debye-Hückel equation
     */
    static Scalar bi()
    {
        return 0.165;
    }

};

} // end namespace

#endif