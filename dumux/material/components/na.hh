/*
 * Na.hh
 *
 *  Created on: 28.06.2011
 *      Author: kissinger
 */

/*!
 * \file
 *
 * \brief A class for the Na2+ fluid properties
 */
#ifndef DUMUX_NA_HH
#define DUMUX_NA_HH

#include <dumux/common/exceptions.hh>
#include <dumux/material/components/component.hh>

#include <cmath>
#include <iostream>


namespace Dumux
{
/*!
 * \brief A class for the Na (Natrium ion) fluid properties
 */
template <class Scalar>
class Na : public Component<Scalar, Na<Scalar> >
{
public:
    /*!
     * \brief A human readable name for the Na.
     */
    static std::string name()
    { return "Na"; }

    /*!
     * \brief The mass in [kg] of one mole of Na.
     */
    static Scalar molarMass()
    { return 22.9898e-3; } // kgNa/molNa

    /*!
     * \brief The diffusion Coefficient of Na in water.
     */
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    { return 2e-9; }

    static Scalar charge()
    {
        return 1.0;
    }

    /*!
     * \brief Return the constant ai Parkhurst (1990) for the modified Debye-Hückel equation
     */

    static Scalar ai()
    {
        return (4.25e-8);   /* Source: http://web.eps.utk.edu/~faculty/misra/460_lectures/CH_7_Text.pdf*/
    }

   /*!
    * \brief Return the constant bi Parkhurst (1990) for the modified Debye-Hückel equation
    */
    static Scalar bi()
    {
        return (0.06); /*Source: http://web.eps.utk.edu/~faculty/misra/460_lectures/CH_7_Text.pdf*/
    }

};

} // end namespace

#endif