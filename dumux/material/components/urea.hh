/*
 * urea.hh
 *
 *  Created on: 20.04.2011
 *      Author: hommel
 */

/*!
 * \file
 *
 * \brief A class for the urea fluid properties
 */
#ifndef DUMUX_UREA_HH
#define DUMUX_UREA_HH

#include <dumux/common/exceptions.hh>
#include <dumux/material/components/component.hh>

#include <cmath>
#include <iostream>


namespace Dumux
{
/*!
 * \brief A class for the urea fluid properties
 */
template <class Scalar>
class Urea : public Component<Scalar, Urea<Scalar> >
{
public:
    /*!
     * \brief A human readable name for the Urea.
     */
    static std::string name()
    { return "Urea"; }

    /*!
     * \brief The mass in [kg] of one mole of Urea.
     */
    static Scalar molarMass()
    { return 0.0606; } // kg/mol

    /*!
     * \brief The diffusion Coefficient of Urea in water.
     */
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    { return 2e-9; }

        // TODO Check the right value

};

} // end namespace

#endif