/*
 * hPlus.h
 *
 *  Created on: 14.03.2011
 *      Author: kissinger
 */

/*!
 * \file
 *
 * \brief A class for the H+ fluid properties
 */
#ifndef DUMUX_HPLUS_HH
#define DUMUX_HPLUS_HH

#include <dumux/common/exceptions.hh>
#include <dumux/material/components/component.hh>

#include <cmath>
#include <iostream>

namespace Dumux
{
/*!
 * \brief A class for the H+ fluid properties
 */
template <class Scalar>
class HPlus : public Component<Scalar, HPlus<Scalar> >
{
public:
    /*!
     * \brief A human readable name for the H+.
     */
    static const char *name()
    { return "H"; }

    /*!
     * \brief The mass in [kg] of one mole of H+.
     */
    static Scalar molarMass()
    { return 1.00794e-3; }

    static Scalar charge()
    {
        return 1.0;
    }

    /*!
     * \brief Return the constant ai Parkhurst (1990) for the modified Debye-Hückel equation
     */

    static Scalar ai()
    {
        return 4.78e-10;
    }

    /*!
     * \brief Return the constant bi Parkhurst (1990) for the modified Debye-Hückel equation
     */
    static Scalar bi()
    {
        return 0.24;
    }

};

} // end namespace

#endif