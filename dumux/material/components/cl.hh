/*
 * Na.hh
 *
 *  Created on: 28.06.2011
 *      Author: kissinger
 */

/*!
 * \file
 *
 * \brief A class for the Cl- fluid properties
 */
#ifndef DUMUX_CL_HH
#define DUMUX_CL_HH

#include <dumux/common/exceptions.hh>
#include <dumux/material/components/component.hh>

#include <cmath>
#include <iostream>


namespace Dumux
{
/*!
 * \brief A class for the Na (Natrium ion) fluid properties
 */
template <class Scalar>
class Cl : public Component<Scalar, Cl<Scalar> >
{
public:
    /*!
     * \brief A human readable name for the Cl.
     */
    static std::string name()
    { return "Cl"; }

    /*!
     * \brief The mass in [kg] of one mole of Cl.
     */
    static Scalar molarMass()
    { return 35.453e-3; } // kgNa/molNa

    /*!
     * \brief The diffusion Coefficient of Cl in water.
     */
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    { return 2e-9; }

    static Scalar charge()
    {
        return -1.0;
    }

    /*!
     * \brief Return the constant ai Parkhurst (1990) for the modified Debye-Hückel equation
     */

    static Scalar ai()
    {
        return 3e-8; /*Source: http://web.eps.utk.edu/~faculty/misra/460_lectures/CH_7_Text.pdf*/
    }

   /*!
    * \brief Return the constant bi Parkhurst (1990) for the modified Debye-Hückel equation
    */
    static Scalar bi()
    {
        return 0.01; /* Source : http://web.eps.utk.edu/~faculty/misra/460_lectures/CH_7_Text.pdf*/
    }

};

} // end namespace

#endif