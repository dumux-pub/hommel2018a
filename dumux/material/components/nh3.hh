/*
 * NH3.hh
 *
 *  Created on: 18.5.2011
 *      Author: hommel
 */

/*!
 * \file
 *
 * \brief A class for the NH4 fluid properties
 */
#ifndef DUMUX_NH3_HH
#define DUMUX_NH3_HH

#include <dumux/common/exceptions.hh>
#include <dumux/material/components/component.hh>

#include <cmath>
#include <iostream>


namespace Dumux
{
/*!
 * \brief A class for the NH3 fluid properties
 */
template <class Scalar>
class NH3 : public Component<Scalar, NH3<Scalar> >
{
public:
    /*!
     * \brief A human readable name for the NH3.
     */
    static std::string name()
    { return "NH3"; }

    /*!
     * \brief The mass in [kg] of one mole of NH3.
     */
    static Scalar molarMass()
    { return 0.017031; } // kg/mol


    /*!
     * \brief The diffusion Coefficient of NH3 in water.
     */
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    { return 2e-9; }

        // TODO Check the right value

};

} // end namespace

#endif