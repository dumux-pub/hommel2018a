/*
 * HCO3.hh
 *
 *  Created on: 14.03.2011
 *      Author: kissinger
 */

/*!
 * \file
 *
 * \brief A class for the HCO3 fluid properties
 */
#ifndef DUMUX_HCO3_HH
#define DUMUX_HCO3_HH

#include <dumux/common/exceptions.hh>
#include <dumux/material/components/component.hh>

#include <cmath>
#include <iostream>


namespace Dumux
{
/*!
 * \brief A class for the HCO3 fluid properties
 */
template <class Scalar>
class HCO3 : public Component<Scalar, HCO3<Scalar> >
{
public:
    /*!
     * \brief A human readable name for the HCO3.
     */
    static std::string name()
    { return "HCO3"; }

    /*!
     * \brief The mass in [kg] of one mole of HCO3.
     */
    static Scalar molarMass()
    { return 61.01714e-3; } // kg/mol

    /*!
     * \brief The diffusion Coefficient of HCO3 in water.
     */
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    { return 2e-9; }

    static Scalar charge()
    {
        return -1.0;
    }

    /*!
     * \brief Return the constant ai Parkhurst (1990) for the modified Debye-Hückel equation
     */

    static Scalar ai()
    {
        return 5.4e-10;
    }

    /*!
     * \brief Return the constant bi Parkhurst (1990) for the modified Debye-Hückel equation
     */
    static Scalar bi()
    {
        return 0.0;
    }

};

} // end namespace

#endif