/*
 * OH.hh
 *
 *  Created on: 14.03.2011
 *      Author: kissinger
 */

/*!
 * \file
 *
 * \brief A class for the OH- fluid properties
 */
#ifndef DUMUX_OH_HH
#define DUMUX_OH_HH

#include <dumux/common/exceptions.hh>
#include <dumux/material/components/component.hh>

#include <cmath>
#include <iostream>


namespace Dumux
{
/*!
 * \brief A class for the Ca (Calcium ion) fluid properties
 */
template <class Scalar>
class OH : public Component<Scalar, OH<Scalar> >
{
public:
    /*!
     * \brief A human readable name for the OH.
     */
    static std::string name()
    { return "OH"; }

    /*!
     * \brief The mass in [kg] of one mole of OH.
     */
    static Scalar molarMass()
    { return 16.9994; } // kg/mol

    static Scalar charge()
    {
        return -1.0;
    }

    /*!
     * \brief Return the constant ai Parkhurst (1990) for the modified Debye-Hückel equation
     */

    static Scalar ai()
    {
        return 10.65e-10;
    }

    /*!
     * \brief Return the constant bi Parkhurst (1990) for the modified Debye-Hückel equation
     */
    static Scalar bi()
    {
        return 0.21;
    }

};

} // end namespace

#endif