/*
 * calcite.hh
 *
 *  Created on: 22.03.2011
 *      Author: kissinger
 */

#ifndef CALCITE_HH_
#define CALCITE_HH_

#include <dumux/common/exceptions.hh>
#include <dumux/material/components/component.hh>
#include <dumux/material/components/ca.hh>
#include <dumux/material/components/co3.hh>

#include <cmath>
#include <iostream>
#include <dumux/material/components/ca.hh>
#include <dumux/material/components/co2.hh>

namespace Dumux
{
/*!
 * \brief A class for the CaCO3 mineral phase properties
 */
template <class Scalar>
class Calcite : public Component<Scalar, Calcite<Scalar> >
{


public:
    typedef Dumux::Ca<Scalar> Ca;
    typedef Dumux::CO3<Scalar> CO3;
    /*!
     * \brief A human readable name for the Ca.
     */
    static std::string name()
    { return "Calcite"; }

    /*!
     * \brief The mass in [kg] of one mole of Ca.
     */
    static Scalar molarMass()
    //{ return 0.1000873; } // kg/mol
    { return Ca::molarMass()+CO3::molarMass(); } // kg/mol

    /*!
     * \brief The Density of Calcite.
     */
    static Scalar density()
    {
        return 2.71e3; // Density in kg/m3
    }

    static Scalar solubilityProduct()
    {
        return 4.8e-9;  // TODO check for more accurate Data, find formula for pressure and temperature dependency
    }

    /*!
     * \brief The Mass Fraction of Ca in Calcit: M_Ca / M_CaCO3 (1 mol of Ca is "stored" in 1 mol of CaCO3)
     */

//    static Scalar massFractionCalcium()
//    {
//        return Ca::molarMass()/molarMass(); //
//    }
//
//    /*!
//     * \brief The Mass Fraction of CO2 in Calcit: M_CO2 / M_CaCO3 (1 mol of CO2 is "stored" in 1 mol of CaCO3)
//     */
//    static Scalar massFractionCO2()
//    {
//        return CO2::molarMass()/molarMass();
//    }

};

} // end namespace

#endif