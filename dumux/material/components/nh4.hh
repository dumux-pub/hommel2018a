/*
 * NH4.hh
 *
 *  Created on: 20.04.2011
 *      Author: hommel
 */

/*!
 * \file
 *
 * \brief A class for the NH4 fluid properties
 */
#ifndef DUMUX_NH4_HH
#define DUMUX_NH4_HH

#include <dumux/common/exceptions.hh>
#include <dumux/material/components/component.hh>

#include <cmath>
#include <iostream>


namespace Dumux
{
/*!
 * \brief A class for the NH4 fluid properties
 */
template <class Scalar>
class NH4 : public Component<Scalar, NH4<Scalar> >
{
public:
    /*!
     * \brief A human readable name for the NH4.
     */
    static std::string name()
    { return "NH4"; }

    /*!
     * \brief The mass in [kg] of one mole of NH4.
     */
    static Scalar molarMass()
    { return 0.018039; } // kg/mol

    static Scalar charge()
    {
        return 1.0;
    }


    /*!
     * \brief Return the constant ai Parkhurst (1990) for the modified Debye-Hückel equation
     */

    static Scalar ai()
    {
//        return 4.6492e-10;     //obtained from own correlation to Anozie's Ka, no literature value found!
        return 0.000000000964657793008481;//obtained from own correlation to Anozie's Ka, when including gamma NH3
    }

    /*!
     * \brief Return the constant bi Parkhurst (1990) for the modified Debye-Hückel equation
     */
    static Scalar bi()
    {
//        return 0.093030348;   //obtained from own correlation to Anozie's Ka, no literature value found!
        return 0.115120901799345;//obtained from own correlation to Anozie's Ka, when including gamma NH3
    }
    /*!
     * \brief The diffusion Coefficient of NH4 in water.
     */
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    { return 2e-9; }

        // TODO Check the right value

};

} // end namespace

#endif