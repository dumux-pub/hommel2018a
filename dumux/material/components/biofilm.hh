/*
 * biof.hh
 *
 *  Created on: 20.04.2011
 *      Author: hommel
 */

#ifndef BIOFILM_HH_
#define BIOFILM_HH_

#include <dumux/common/exceptions.hh>
#include <dumux/material/components/component.hh>
//#include <dumux/porousmediumflow/2pncmin/implicit/properties.hh>
#include <dumux/implicit/2pbiomin/properties.hh>

#include <cmath>
#include <iostream>


namespace Dumux
{
/*!
 * \brief A class for the Biofilm phase properties
 */
//template <class Scalar>
//class Biofilm : public Component<Scalar, Biofilm<Scalar> >
//{
//template <class TypeTag>
//class Biofilm : public Component<TypeTag, Biofilm<TypeTag> >

template <class TypeTag, class Scalar>
class Biofilm : public Component<Scalar, Biofilm<TypeTag, Scalar> >
{
//  typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    /*!
     * \brief A human readable name for the Biofilm.
     */
    static std::string name()
    { return "Biofilm"; }

    /*!
     * \brief The mass in [kg] of one mole of Biofilm
     */
    static Scalar molarMass()  // TODO what is the molar Mass of biofilm
   { return 1; } // kg/mol
        //based on a cell mass of 2.5e-16, the molar mass of cells would be 1.5e8 kg/mol.


    /*!
     * \brief The (dry) density of Biofilm.
     */
    static Scalar density()
    {
//      return 10 ; // fitted by Ebigbo 2012 WRR// Density in kg/m3 fitted value,  Hommel 2015 WRR, see now input file!
        return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BioCoefficients, rhoBiofilm);

    }
    //brief the Biomass Coefficients for the source therms.

};

} // end namespace

#endif